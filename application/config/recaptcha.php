<?php

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LedZCQTAAAAAG3ojVmCo1WBz4fp-cZ5WYlmOEzR';
$config['recaptcha_secret_key'] = '6LedZCQTAAAAAMpV-yvbWUcqHxTaoBS5ruWjkIUT';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
