<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['posts/category/(:any)']='posts/getbycategory/$1';
$route['info']='posts/getInfo';
$route['about-us']='posts/getAbout';
$route['question-and-answer']='posts/getQa';
$route['bed-and-breakfast']='bnb/getBnb';
$route['bnb/view/(:any)']='bnb/getOne/$1';
$route['help/(:any)']='posts/getHelp/$1';
$route['other/(:any)']='posts/getOther/$1';
$route['booking_print']='booking/bookingPrint';
$route['booking_print_bnb']='booking/bookingPrintBnb';
$route['confirm']='confirm';
$route['login_customer']='login/loginCustomer';
$route['customer-login']='login/viewLoginCustomer';

$route['booking_now/(:any)']='booking/postBooking/$1';

$route['booking_bnb_now/(:any)']='booking/postBookingBnb/$1';

$route['booking_form']='booking/getBookingForm';
$route['detail_booking']='booking/getDetailBooking';

$route['programs_booking_finish/(:any)']='booking/postProgramsBooked/$1';
$route['bnb_booking_finish/(:any)']='booking/postBnbBooked/$1';
//Booking Form
$route['booking_next/(:any)']='booking/postBookingNext/$1';

$route['booking_bnb_next/(:any)']='booking/postBookingBnbNext/$1';


$route['booking_include_all/(:any)']='booking/getBookingAll/$1';
$route['booking_include_none']='booking/getBookingWithNone';
$route['booking_bnb/(:any)']='booking/getBookingBnb/$1';
$route['booking_rac']='booking/getBookingWithRac';

$route['booking_one']='booking/getBookingOne/$1';
$route['booking_two']='booking/getBookingTwo';