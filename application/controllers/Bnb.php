<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Bnb
           *
           * @author Habib Koe Tkj
           */
          class Bnb extends CI_Controller
          {
                    
                    public function __construct()
                    {
                              parent::__construct();
                              
                              $this->load->model('bnb_model');
                              $this->load->model('programactivities_model');
                              $this->load->model('home_model');
                              $this->load->model('post_model');
                    }

                    public function getBnb()
                    {
                              $data = array(
                                             'app_name'=>'Bed and Breakfast | Royal Tour Operator and Mount Rinjani Trekking Packages',
                                             'page_base'=>'BnB',
                                             'page_name'=>'Bed and Breakfast',
                                             
                                             'bnb'=>  $this->bnb_model->get(),
                                             'posts_side'=>  $this->post_model->getByType('info',4),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/bb_rent_a_car', $data);
                    }
                    
                    public function getOne($url)
                    {
                              $data = array(
                                             'app_name'=>'Bed and Breakfast | Royal Tour Operator and Mount Rinjani Trekking Packages',
                                             'page_base'=>'BnB',
                                             'page_name'=>'Bed and Breakfast',
                                             
                                             'bnb_one'=>  $this->bnb_model->findByUrl($url),
                                             'posts_side'=>  $this->post_model->getByType('info',4),
                                             'bnb_side'=> $this->bnb_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/bb_rent_a_car_one', $data);
                    }
          }
          