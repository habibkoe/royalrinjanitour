<?php
          defined('BASEPATH') OR exit('No direct script access allowed');

          /**
           * Description of Booking
           *
           * @author Habib Koe Tkj
           */
          class Booking extends CI_Controller
          {

                    private $web_title = 'Booking Area | Royal Tour Operator and Mount Rinjani Trekking Packages';

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('home_model');
                              $this->load->model('booking_model');
                              $this->load->model('programactivities_model');
                              $this->load->model('bnb_model');
                              $this->load->model('post_model');
                              $this->load->library('recaptcha');
                    }
                    
                    //START PROGRAM BOOKING---------------------------------------------------------------------------------------------------------------------------
                    public function postBooking($id)
                    {

                              $programs = $this->programactivities_model->find($id);
                              $person = $this->input->post('person');
                              $total_person = $_POST['total_person'];
                              $this->form_validation->set_rules('person', 'Total person', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'Sorry, you must choose one of the price in the price list.');
                                        redirect('programactivities/view/' . $programs->pr_url);
                              }
                              else {
                                        if ($person == 1) {
                                                  $total_person_fix = $person;
                                                  $price = $programs->pr_price_1;
                                        }
                                        else if ($person == 2) {
                                                  $total_person_fix = $person;
                                                  $price = $programs->pr_price_2;
                                        }
                                        else if ($person == 3) {
                                                  if ($total_person > 2 && $total_person < 5) {
                                                            $total_person_fix = $total_person;
                                                            $price = $programs->pr_price_3_4;
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Please, fill “total guest (s) to come” in the box above. Thank you.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 5) {

                                                  if ($total_person > 4 && $total_person < 7) {
                                                            $total_person_fix = $total_person;
                                                            $price = $programs->pr_price_5_6;
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Please, fill “total guest (s) to come” in the box above. Thank you.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 7) {

                                                  if ($total_person > 6 && $total_person < 9) {
                                                            $total_person_fix = $total_person;
                                                            $price = $programs->pr_price_7_8;
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Please, fill “total guest (s) to come” in the box above. Thank you.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 9) {

                                                  if ($total_person > 8 && $total_person < 11) {
                                                            $total_person_fix = $total_person;
                                                            $price = $programs->pr_price_9_10;
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Please, fill “total guest (s) to come” in the box above. Thank you.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 11) {

                                                  if ($total_person > 10 && $total_person < 21) {
                                                            $total_person_fix = $total_person;
                                                            $price = $programs->pr_price_11_20;
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Please, fill “total guest (s) to come” in the box above. Thank you.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        //---------------------------------------------------------------------------------------------------------

                                        $data = array(
                                                       'id' => $programs->id,
                                                       'qty' => $this->input->post('qty'),
                                                       'price' => $price * $total_person_fix,
                                                       'name' => $programs->pr_title,
                                                       'typer' => 'programs',
                                                       'options' => array('Total guest (s)' => $total_person_fix)
                                        );
                                        $this->cart->insert($data);
                                        redirect(base_url('detail_booking'));
                                        //redirect(base_url('booking_include_all'));
                              }
                    }
                    
                    public function getBookingAll($id = NULL)
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             
                                             'rowid'=>$id,
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_all', $data);
                    }
                    
                    public function postBookingNext($id = NULL)
                    {

                              $this->form_validation->set_rules('or_departure_date', 'Departure Date', 'required');
                              $this->form_validation->set_rules('or_arrival_date', 'Arrival Date', 'required');
                              $this->form_validation->set_rules('or_pick_up_from', 'Pick up from', 'required');
                              $this->form_validation->set_rules('or_drop_off_to', 'Drop off to', 'required');
                              $this->form_validation->set_rules('or_guest_name[]', 'Guest Name', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'Please fill out this form completely. Thank you.');
                                        redirect(base_url('booking_include_all'));
                              }
                              else {

                                        // Loop through the array of checked box values ... 
                                        $person = "";
                                        $flag = 0;
                                        foreach ($_POST['or_guest_name'] as $entry) {
                                                  $person .= $entry . ", ";
                                                  $flag = 1;
                                        }
                                        if ($flag == 1) {
                                                  $person = rtrim($person);
                                        }

                                        $data = array(
                                                       //general info
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       
                                                       'rowid' => $id,
                                                       'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                                                       'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
                                                       'programs' => $this->programactivities_model->get(),
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       //programs info must be include
                                                       'or_departure_date' => $this->input->post('or_departure_date'),
                                                       'or_arrival_date' => $this->input->post('or_arrival_date'),
                                                       'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                                       'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                                       'or_guest_name' => $person
                                                  //bnb info alternative
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/booking_next', $data);
                              }
                    }

                    public function postProgramsBooked($rowid)
                    {
                              $this->form_validation->set_rules('or_check', 'Check agreement', 'required');
                              $this->form_validation->set_rules('or_full_name', 'Full name', 'required');
                              $this->form_validation->set_rules('or_address', 'Address', 'required');
                              $this->form_validation->set_rules('or_email', 'Email', 'required');
                              $this->form_validation->set_rules('or_phone', 'Phone', 'required');
                              $this->form_validation->set_rules('or_departure_date', 'Departure date', 'required');
                              $this->form_validation->set_rules('or_arrival_date', 'Arrival date', 'required');
                              $this->form_validation->set_rules('or_pick_up_from', 'Pick up from', 'required');
                              $this->form_validation->set_rules('or_payment_method', 'Payment methode', 'required');
                              $recaptcha = $this->input->post('g-recaptcha-response');
                              $response = $this->recaptcha->verifyResponse($recaptcha);
                              if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
                                        $this->session->set_flashdata('Errorbooking', 'Please fill out this form completely. Thank you.');
                                        redirect('booking_include_all');
                              }
                              else {
                                        $this->load->library('email');
                                        $this->email->set_mailtype('html');
                                        $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                        $this->email->to('royalrinjanitour@gmail.com');
                                        $this->email->subject($this->input->post('or_full_name').' Booking Programs');

                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Finish Programs Booking',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'programs' => $this->programactivities_model->get(),
                                                       
                                                       'id' => time() + 1993,
                                                       'or_full_name' => $this->input->post('or_full_name'),
                                                       'or_phone' => $this->input->post('or_phone'),
                                                       'or_home_phone' => $this->input->post('or_home_phone'),
                                                       'or_email' => $this->input->post('or_email'),
                                                       'or_address' => $this->input->post('or_address'),
                                                       'or_departure_date' => $this->input->post('or_departure_date'),
                                                       'or_arrival_date' => $this->input->post('or_arrival_date'),
                                                       'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                                       'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                                       'or_payment_method' => $this->input->post('or_payment_method'),
                                                       'or_guest_name' => $this->input->post('or_guest_name'),
                                                       'or_medical_condition' => $this->input->post('or_medical_condition'),
                                                       'or_special_diets' => $this->input->post('or_special_diets'),
                                                       'or_message' => $this->input->post('or_message'),
                                                       'or_price' => $this->input->post('or_price'),
                                                       'or_date' => date('d-m-Y H:i:s'),
                                                       'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                       'or_status' => 'unpaid'
                                        );
                                        $message_admin = $this->load->view('notification/email_programs_to_admin', $data, TRUE);
                                        $this->email->message($message_admin);
                                        
                                        $insert = $this->booking_model->post($rowid);
                                        
                                        if ($insert) {
                                                  $send_email_admin = $this->email->send();

                                                  if ($send_email_admin) {

                                                            $this->load->library('email');
                                                            $this->email->set_mailtype('html');
                                                            $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                                            $this->email->to($this->input->post('or_email'));
                                                            $this->email->subject('Details Booking Programs Royal Rinjani Tour');

                                                            $data2 = array(
                                                                           'id' => time() + 1993,
                                                                           'or_full_name' => $this->input->post('or_full_name'),
                                                                           'or_phone' => $this->input->post('or_phone'),
                                                                           'or_home_phone' => $this->input->post('or_home_phone'),
                                                                           'or_email' => $this->input->post('or_email'),
                                                                           'or_address' => $this->input->post('or_address'),
                                                                           'or_departure_date' => $this->input->post('or_departure_date'),
                                                                           'or_arrival_date' => $this->input->post('or_arrival_date'),
                                                                           'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                                                           'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                                                           'or_payment_method' => $this->input->post('or_payment_method'),
                                                                           'or_guest_name' => $this->input->post('or_guest_name'),
                                                                           'or_medical_condition' => $this->input->post('or_medical_condition'),
                                                                           'or_special_diets' => $this->input->post('or_special_diets'),
                                                                           'or_message' => $this->input->post('or_message'),
                                                                           'or_price' => $this->input->post('or_price'),
                                                                           'or_date' => date('d-m-Y H:i:s'),
                                                                           'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                                           'or_status' => 'unpaid'
                                                            );
                                                            $message_to_member = $this->load->view('notification/email_programs_booking', $data2, TRUE);
                                                            $this->email->message($message_to_member);
                                                            $this->email->send();
                                                            $finish = $this->load->view('notification/finish_program_booking', $data);
                                                            if ($finish) {
                                                                      $this->cart->remove($rowid);
                                                            }
                                                  }
                                        }
                              }
                    }
                    
                    //END OF PROGRAMS BOOKING------------------------------------------------------------------------------------------------------------------------------
                    
                    //START BNB BOOKING======================================================================================
                    public function postBookingBnb($id)
                    {
                              $bnb = $this->bnb_model->find($id);
                              $data = array(
                                             'id' => $bnb->id,
                                             'qty' => 1,
                                             'price' => $bnb->bnb_price,
                                             'name' => $bnb->bnb_name,
                                             'typer' => 'bnb'
                              );
                              $this->cart->insert($data);
                              redirect(base_url('detail_booking'));
                              //redirect(base_url('booking_bnb'));
                    }
                    
                    public function getBookingBnb($id = NULL)
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             
                                             'rowid'=> $id,
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'bnb' => $this->bnb_model->get(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_bnb', $data);
                    }

                    public function postBookingBnbNext($id = NULL)
                    {
                              $this->form_validation->set_rules('or_checkin_date', 'Check in Date', 'required');
                              $this->form_validation->set_rules('or_checkout_date', 'Check out Date', 'required');
                              $this->form_validation->set_rules('or_guest_name[]', 'Guest Name', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'Please fill out this form completely. Thank you.');
                                        redirect(base_url('booking_bnb'));
                              }
                              else {
                                        // Loop through the array of checked box values ... 
                                        $person = "";
                                        $flag = 0;
                                        foreach ($_POST['or_guest_name'] as $entry) {
                                                  $person .= $entry . ", ";
                                                  $flag = 1;
                                        }
                                        if ($flag == 1) {
                                                  $person = rtrim($person);
                                        }
                                        $long_night = ((abs(strtotime($this->input->post('or_checkin_date')) - strtotime($this->input->post('or_checkout_date')))) / (60 * 60 * 24));
                                        $data = array(
                                                       //general info
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       
                                                       'rowid'=> $id,
                                                       'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
                                                       'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
                                                       'bnb' => $this->bnb_model->get(),
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       //bnb info
                                                       'or_checkin_date' => $this->input->post('or_checkin_date'),
                                                       'or_checkout_date' => $this->input->post('or_checkout_date'),
                                                       'or_long_night' => $long_night,
                                                       'or_guest_name' => $person
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/booking_bnb_next', $data);
                              }
                    }

                    public function postBnbBooked($rowid)
                    {
                              $this->form_validation->set_rules('or_check', 'Check agreement', 'required');
                              $this->form_validation->set_rules('or_full_name', 'Full name', 'required');
                              $this->form_validation->set_rules('or_address', 'Address', 'required');
                              $this->form_validation->set_rules('or_email', 'Email', 'required');
                              $this->form_validation->set_rules('or_phone', 'Phone', 'required');
                              $this->form_validation->set_rules('or_checkin_date', 'Checkin date', 'required');
                              $this->form_validation->set_rules('or_checkout_date', 'Checkout date', 'required');
                              $this->form_validation->set_rules('or_payment_method', 'Payment methode', 'required');
                              
                              $recaptcha = $this->input->post('g-recaptcha-response');
                              $response = $this->recaptcha->verifyResponse($recaptcha);
                              
                              if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
                                        $this->session->set_flashdata('Errorbooking', 'Please fill out this form completely. Thank you.');
                                        redirect('booking_bnb');
                              }
                              else {
                                        $this->load->library('email');
                                        $this->email->set_mailtype('html');
                                        $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                        $this->email->to('royalrinjanitour@gmail.com');
                                        $this->email->subject($this->input->post('or_full_name').' Booking Bed and Breakfast');

                                        $data = array(
                                                       'bnb' => $this->bnb_model->get(),
                                                       'id' => time() + 1993,
                                                       'or_full_name' => $this->input->post('or_full_name'),
                                                       'or_phone' => $this->input->post('or_phone'),
                                                       'or_home_phone' => $this->input->post('or_home_phone'),
                                                       'or_email' => $this->input->post('or_email'),
                                                       'or_address' => $this->input->post('or_address'),
                                                       'or_long_night' => $this->input->post('or_long_night'),
                                                       'or_checkin_date' => $this->input->post('or_checkin_date'),
                                                       'or_checkout_date' => $this->input->post('or_checkout_date'),
                                                       'or_payment_method' => $this->input->post('or_payment_method'),
                                                       'or_special_diets' => $this->input->post('or_special_diets'),
                                                       'or_price_includes' => $this->input->post('or_price_includes'),
                                                       'or_guest_name' => $this->input->post('or_guest_name'),
                                                       'or_message' => $this->input->post('or_message'),
                                                       'or_price' => $this->input->post('or_price'),
                                                       'or_date' => date('d-m-Y, H:i:s'),
                                                       'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                       'or_status' => 'unpaid'
                                        );
                                        $message_to_admin = $this->load->view('notification/email_bnb_to_admin', $data, TRUE);
                                        $this->email->message($message_to_admin);
                                        
                                        $insert = $this->booking_model->postBnb($rowid);
                                        
                                        if ($insert) {
                                                  $send_email_to_member = $this->email->send();

                                                  if ($send_email_to_member) {

                                                            $this->load->library('email');
                                                            $this->email->set_mailtype('html');
                                                            $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                                            $this->email->to($this->input->post('or_email'));
                                                            $this->email->subject('Details Booking Bed and Breakfast Royal Rinjani Tour');

                                                            $data2 = array(
                                                                           'app_name' => $this->web_title,
                                                                           'page_base' => 'Finish Bed and Breakfast Booking',
                                                                           'page_name' => '',
                                                                           'web_setting' => $this->home_model->getWebSetting(),
                                                                           'help_footer' => $this->home_model->getPostByCategory(15),
                                                                           'other_footer' => $this->home_model->getPostByCategory(16),
                                                                           'bnb' => $this->bnb_model->get(),
                                                                           'id' => time() + 1993,
                                                                           'or_full_name' => $this->input->post('or_full_name'),
                                                                           'or_phone' => $this->input->post('or_phone'),
                                                                           'or_home_phone' => $this->input->post('or_home_phone'),
                                                                           'or_email' => $this->input->post('or_email'),
                                                                           'or_address' => $this->input->post('or_address'),
                                                                           'or_checkin_date' => $this->input->post('or_checkin_date'),
                                                                           'or_checkout_date' => $this->input->post('or_checkout_date'),
                                                                           'or_payment_method' => $this->input->post('or_payment_method'),
                                                                           'or_price_includes' => $this->input->post('or_price_includes'),
                                                                           'or_guest_name' => $this->input->post('or_guest_name'),
                                                                           'or_special_diets' => $this->input->post('or_special_diets'),
                                                                           'or_message' => $this->input->post('or_message'),
                                                                           'or_price' => $this->input->post('or_price'),
                                                                           'or_date' => date('d-m-Y H:i:s'),
                                                                           'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                                           'or_status' => 'unpaid'
                                                            );
                                                            $message_to_member = $this->load->view('notification/email_bnb_booking', $data2, TRUE);
                                                            $this->email->message($message_to_member);
                                                            $this->email->send();
                                                            $finish = $this->load->view('notification/finish_bnb_booking', $data2);
                                                            
                                                            if ($finish) {
                                                                      $this->cart->remove($rowid);
                                                            }
                                                  }
                                        }
                              }
                    }
                    //END OF THE BNB BOOKING================================================================================
                    
                    public function getDetailBooking()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             
                                             'program_activities'=>  $this->home_model->getProgramActivities(4),
                                             'programs' => $this->programactivities_model->get(),
                                             'bnb' => $this->bnb_model->get(),
                                             'bnb_bottom'=>  $this->bnb_model->get(4),
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/detail_booking', $data);
                    }

                    public function getBookingForm()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_form', $data);
                    }

                    public function remove($rowid)
                    {
                              $this->cart->remove($rowid);
                              redirect(base_url('detail_booking'));
                    }

                    public function clear()
                    {
                              $this->cart->destroy();
                              redirect(base_url('detail_booking'));
                    }

                    public function bookingPrint()
                    {
                              $data = array(
                                             'programs' => $this->programactivities_model->get(),
                                             'id' => $this->input->post('id'),
                                             'or_full_name' => $this->input->post('or_full_name'),
                                             'or_phone' => $this->input->post('or_phone'),
                                             'or_home_phone' => $this->input->post('or_home_phone'),
                                             'or_email' => $this->input->post('or_email'),
                                             'or_address' => $this->input->post('or_address'),
                                             'or_departure_date' => $this->input->post('or_departure_date'),
                                             'or_arrival_date' => $this->input->post('or_arrival_date'),
                                             'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                             'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                             'or_payment_method' => $this->input->post('or_payment_method'),
                                             'or_guest_name' => $this->input->post('or_guest_name'),
                                             'or_medical_condition' => $this->input->post('or_medical_condition'),
                                             'or_price_includes' => $this->input->post('or_price_includes'),
                                             'or_special_diets' => $this->input->post('or_special_diets'),
                                             'or_message' => $this->input->post('or_message'),
                                             'or_price' => $this->input->post('or_price'),
                                             'or_date' => $this->input->post('or_date'),
                                             'or_status' => 'unpaid'
                              );
                              $this->load->view('notification/order_print', $data);
                    }

                    public function bookingPrintBnb()
                    {
                              $data = array(
                                             'bnb' => $this->bnb_model->get(),
                                             'id' => $this->input->post('id'),
                                             'or_full_name' => $this->input->post('or_full_name'),
                                             'or_phone' => $this->input->post('or_phone'),
                                             'or_home_phone' => $this->input->post('or_home_phone'),
                                             'or_email' => $this->input->post('or_email'),
                                             'or_address' => $this->input->post('or_address'),
                                             'or_checkin_date' => $this->input->post('or_checkin_date'),
                                             'or_checkout_date' => $this->input->post('or_checkout_date'),
                                             'or_payment_method' => $this->input->post('or_payment_method'),
                                             'or_guest_name' => $this->input->post('or_guest_name'),
                                             'or_price_includes' => $this->input->post('or_price_includes'),
                                             'or_special_diets' => $this->input->post('or_special_diets'),
                                             'or_message' => $this->input->post('or_message'),
                                             'or_price' => $this->input->post('or_price'),
                                             'or_date' => $this->input->post('or_date'),
                                             'or_status' => 'unpaid'
                              );
                              $this->load->view('notification/order_print_bnb', $data);
                    }

          }
          