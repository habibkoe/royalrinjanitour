<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Booking
           *
           * @author Habib Koe Tkj
           */
          class Booking extends CI_Controller
          {

                    private $web_title = 'Booking Programs | Royal Rinjani Tour';

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('home_model');
                              $this->load->model('booking_model');
                              $this->load->model('programactivities_model');
                              $this->load->model('programsetting_model');
                    }

                    public function postBooking($id)
                    {

                              $programs = $this->programactivities_model->find($id);
                              $person = $this->input->post('person');
                              $total_person = $_POST['total_person'];
                              $this->form_validation->set_rules('person', 'Total person', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'Sorry you must choose one of the price in the price list.');
                                        redirect('programactivities/view/' . $programs->pr_url);
                              }
                              else {

                                        if (isset($_POST['bnb'])) {
                                                  $bnb_fix = $_POST['bnb'];
                                        }
                                        else {
                                                  $bnb_fix = 'No';
                                        }
                                        //------------------------------------------------------------------------------------
                                        if (isset($_POST['rent_a_car'])) {
                                                  $rent_a_car_fix = $_POST['rent_a_car'];
                                        }
                                        else {
                                                  $rent_a_car_fix = 'No';
                                        }
                                        //------------------------------------------------------------------------------------
                                        if ($person == 1) {
                                                  $total_person_fix = $person;
                                                  $price = $programs->pr_price_1 + $bnb_fix + $rent_a_car_fix;
                                        }
                                        else if ($person == 2) {
                                                  $total_person_fix = $person;
                                                  $price = ($programs->pr_price_2 + $bnb_fix + $rent_a_car_fix);
                                        }
                                        else if ($person == 3) {

                                                  if ($total_person > 2 && $total_person < 5) {
                                                            $total_person_fix = $total_person;
                                                            $price = ($programs->pr_price_3_4 + $bnb_fix + $rent_a_car_fix);
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Sorry you the people you input must be under 5 or above 2 people.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 5) {

                                                  if ($total_person > 4 && $total_person < 7) {
                                                            $total_person_fix = $total_person;
                                                            $price = ($programs->pr_price_5_6 + $bnb_fix + $rent_a_car_fix);
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Sorry you the people you input must be under 7 or above 4 people.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 7) {

                                                  if ($total_person > 6 && $total_person < 9) {
                                                            $total_person_fix = $total_person;
                                                            $price = ($programs->pr_price_7_8 + $bnb_fix + $rent_a_car_fix);
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Sorry you the people you input must be under 9 or above 6 people.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 9) {

                                                  if ($total_person > 8 && $total_person < 11) {
                                                            $total_person_fix = $total_person;
                                                            $price = ($programs->pr_price_9_10 + $bnb_fix + $rent_a_car_fix);
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Sorry you the people you input must be under 11 or above 8 people.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        else if ($person == 11) {

                                                  if ($total_person > 10 && $total_person < 21) {
                                                            $total_person_fix = $total_person;
                                                            $price = ($programs->pr_price_11_20 + $bnb_fix + $rent_a_car_fix);
                                                  }
                                                  else {
                                                            $this->session->set_flashdata('Errorbooking', 'Sorry you the people you input must be under 21 or above 10 people.');
                                                            redirect('programactivities/view/' . $programs->pr_url);
                                                  }
                                        }
                                        //---------------------------------------------------------------------------------------------------------

                                        $data = array(
                                                       'id' => $programs->id,
                                                       'qty' => $total_person_fix,
                                                       'price' => $price,
                                                       'name' => $programs->pr_title,
                                                       'options' => array('B&B' => $bnb_fix, 'Rent a car' => $rent_a_car_fix)
                                        );
                                        $this->cart->insert($data);

                                        if (isset($_POST['rent_a_car']) == 'yes' && empty($_POST['bnb'])) {
                                                  redirect(base_url('booking_include_rac'));
                                        }
                                        else if (isset($_POST['bnb']) == 'yes' && empty($_POST['rent_a_car'])) {
                                                  redirect(base_url('booking_include_bnb'));
                                        }
                                        else if (isset($_POST['bnb']) == 'yes' && isset($_POST['rent_a_car']) == 'yes') {
                                                  redirect(base_url('booking_include_all'));
                                        }
                                        else if (empty($_POST['bnb']) && empty($_POST['rent_a_car'])) {
                                                  redirect(base_url('booking_include_none'));
                                        }
                              }
                    }

                    public function postBookingNext()
                    {

                              $this->form_validation->set_rules('or_departure_date', 'Departure Date', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'Sorry you must choose one of the price in the price list.');
                                        echo 'error';
                              }
                              else {

                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'programs' => $this->programactivities_model->get(),
                                                       'web_setting'=>  $this->home_model->getWebSetting(),
                                                       
                                                       'or_departure_date' => $this->input->post('or_departure_date'),
                                                       'or_arrival_date' => $this->input->post('or_arrival_date'),
                                                       'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                                       'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                                       'or_checkin_date' => $this->input->post('or_checkin_date'),
                                                       'or_checkout_date' => $this->input->post('or_checkout_date'),
                                                       'or_long_night' => $this->input->post('or_long_night'),
                                                       'or_room_type' => $this->input->post('or_room_type'),
                                                       'or_rent_start_date' => $this->input->post('or_rent_start_date'),
                                                       'or_rent_end_date' => $this->input->post('or_rent_end_date'),
                                                       'or_prefered_car' => $this->input->post('or_prefered_car'),
                                                       
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/booking_next', $data);
                              }
                    }

                    public function postCustomerBooked()
                    {
                              $this->form_validation->set_rules('or_full_name', 'Post Title', 'required');
                              $this->form_validation->set_rules('or_address', 'Post Type', 'required');
                              $this->form_validation->set_rules('or_email', 'Post Category', 'required');
                              $this->form_validation->set_rules('or_phone', 'Short Description', 'required');
                              $this->form_validation->set_rules('or_departure__date', 'Post Title', 'required');
                              $this->form_validation->set_rules('or_arrival_date', 'Post Type', 'required');
                              $this->form_validation->set_rules('or_flight_from', 'Post Category', 'required');
                              $this->form_validation->set_rules('or_payment_method', 'Short Description', 'required');
                              if ($this->form_validation->run() == FALSE) {
                                        $this->session->set_flashdata('Errorbooking', 'You must fill the required form.');
                                        redirect('booking_form');
                              }
                              else {
                                        $this->booking_model->post();
                                        redirect('success_booked');
                              }
                    }

                    public function getDetailBooking()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/detail_booking', $data);
                    }

                    public function getBookingAll()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'bnb'=>  $this->programsetting_model->getByCategory('bnb')
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_all', $data);
                    }

                    public function getBookingWithBnb()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'bnb'=>  $this->programsetting_model->getByCategory('bnb')
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_with_bnb', $data);
                    }

                    public function getBookingWithRac()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_with_rac', $data);
                    }

                    public function getBookingWithNone()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_with_none', $data);
                    }

                    public function getBookingForm()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Home',
                                             'page_name' => '',
                                             'programs' => $this->programactivities_model->get(),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/booking_form', $data);
                    }

                    public function remove($rowid)
                    {
                              $this->cart->remove($rowid);
                              redirect(base_url('detail_booking'));
                    }

                    public function clear()
                    {
                              $this->cart->destroy();
                              redirect(base_url('detail_booking'));
                    }

          }
          