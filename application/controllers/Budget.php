<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Budget
           *
           * @author Habib Koe Tkj
           */
          class Budget extends CI_Controller
          {

                    private $web_title = 'Budgeting | Royal Rinjani Tour';

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('budget_model');
                              $this->load->model('home_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Budgeting',
                                             'page_name' => '',
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/budget', $data);
                    }

                    public function searchBudget()
                    {
                              $bu_from = $this->input->post('bu_from');
                              $bu_far = $this->input->post('bu_far');
                              $bu_cost = $this->input->post('bu_cost');

                              if ($bu_far == 'sangat_dekat') {
                                        $far_max = 200;
                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'budget'=> $this->budget_model->getBudget($bu_from, $bu_cost, $far_max)
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/budget_result', $data);
                              }
                              else if ($bu_far == 'dekat') {
                                        $far_max = 400;
                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'budget'=> $this->budget_model->getBudget($bu_from, $bu_cost, $far_max)
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/budget_result', $data);
                              }
                              else if ($bu_far == 'sedang') {
                                        $far_max = 800;
                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'budget'=> $this->budget_model->getBudget($bu_from, $bu_cost, $far_max)
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/budget_result', $data);
                              }
                              else if ($bu_far == 'jauh') {
                                        $far_max = 1200;
                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'budget'=> $this->budget_model->getBudget($bu_from, $bu_cost, $far_max)
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/budget_result', $data);
                              }
                              else if ($bu_far == 'sangat_jauh') {
                                        $far_max = 2000;
                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Home',
                                                       'page_name' => '',
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                                       'budget'=> $this->budget_model->getBudget($bu_from, $bu_cost, $far_max)
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('home/budget_result', $data);
                              }
                    }

          }
          