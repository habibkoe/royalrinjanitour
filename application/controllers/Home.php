<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Home
           *
           * @author Habib Koe Tkj
           */
          class Home extends CI_Controller
          {
                    private $web_title = 'Royal Tour Operator and Mount Rinjani Trekking Packages';

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('home_model');
                              $this->load->model('bnb_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name'=>$this->web_title,
                                             'page_base'=>'Home',
                                             'page_name'=>'',
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                                             
                                             'first_slider'=>  $this->home_model->getSlider(1, 0),
                                             'next_slider'=>  $this->home_model->getSlider(3, 1),
                                             'program_activities'=>  $this->home_model->getProgramActivities(4),
                                             'food_and_drink'=>  $this->home_model->getPostByCategory(1, 4),
                                             'art_and_culture'=>  $this->home_model->getPostByCategory(7, 4),
                                             'nature'=>  $this->home_model->getPostByCategory(11, 4),
                                             'bnb'=>  $this->bnb_model->get(4),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/index', $data);
                            
                    }

          }
          