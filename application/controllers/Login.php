<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Login
           *
           * @author Habib Koe Tkj
           */
          class Login extends CI_Controller
          {

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('login_model');
                    }

                    public function index()
                    {
                              $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric');

                              if ($this->form_validation->run() == FALSE) {
                                        $data = array(
                                             'app_name' => 'Login Admin',
                                        );
                                        $this->load->view('layout/login_head', $data);
                                        $this->load->view('login');
                              }
                              else {
                                        //cek to database email and password
                                        //$email = set_value('email');
                                        $password = set_value('password');
                                        $valid_user = $this->login_model->cek_data_user($password);

                                        if ($valid_user == TRUE) {
                                                  //login berhasil
                                                  $this->session->set_userdata('loggedin', TRUE);
                                                  $this->session->set_userdata('admin_id', $valid_user->admin_id);
                                                  $this->session->set_userdata('admin_name', $valid_user->admin_name);
                                                  $this->session->set_userdata('admin_status', $valid_user->admin_status);
                                                  $this->session->set_userdata('admin_level', $valid_user->admin_level);
                                                  redirect('backend/dashboard');
                                        }
                                        else {
                                                  //username dan password salah
                                                  $this->session->set_flashdata('error', 'wrong username / password');
                                                  redirect('login');
                                        }
                              }
                    }
                    
                    public function viewLoginCustomer()
                    {
                              $this->form_validation->set_rules('password', 'Password', 'trim|required|alpha_numeric');

                              if ($this->form_validation->run() == FALSE) {
                                        $data = array(
                                             'app_name' => 'Login Customer',
                                        );
                                        $this->load->view('layout/login_head', $data);
                                        $this->load->view('login_customer');
                              }
                    }

                    public function loginCustomer()
                    {
                              
                              $this->form_validation->set_rules('or_email', 'Email', 'trim|required');
                              $this->form_validation->set_rules('or_booking_number', 'Nomor Booking', 'trim|required');

                              if ($this->form_validation->run() == FALSE) {
                                        $this->load->view('layout/login_head');
                                        $this->load->view('login');
                              }
                              else {
                                        //cek to database email and password
                                        //$email = set_value('email');
                                        $or_email = set_value('or_email');
                                        $or_booking_number = set_value('or_booking_number');
                                        $valid_user = $this->login_model->cek_data_customer($or_email, $or_booking_number);

                                        if ($valid_user == TRUE) {
                                                  //login berhasil
                                                  $this->session->set_userdata('customerin', TRUE);
                                                  $this->session->set_userdata('or_full_name', $valid_user->or_full_name);
                                                  $this->session->set_userdata('or_booking_number', $valid_user->id);
                                                  $this->session->set_userdata('or_email', $valid_user->or_email);
                                                  $this->session->set_userdata('or_status', $valid_user->or_status);
                                                  redirect('transaction');
                                        }
                                        else {
                                                  //username dan password salah
                                                  $this->session->set_flashdata('error', 'wrong username / password');
                                                  redirect('customer-login');
                                        }
                              }
                    }
                    
                    
          }
          