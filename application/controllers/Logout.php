<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Logout
           *
           * @author Habib Koe Tkj
           */
          class Logout extends CI_Controller
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    function index()
                    {
                              $this->session->sess_destroy();
                              $data = array(
                                             'app_name' => 'Login Admin',
                                             'logout'=> 'You have been logged out.'
                                        );
                              $this->load->view('layout/login_head', $data);
                              $this->load->view('login', $data);
                    }
                    
                    function customer()
                    {
                              $this->session->sess_destroy();
                              $data = array(
                                             'app_name' => 'Login Admin',
                                             'logout'=> 'You have been logged out.'
                                        );
                              $this->load->view('layout/login_head', $data);
                              $this->load->view('login', $data);
                    }
          }
          