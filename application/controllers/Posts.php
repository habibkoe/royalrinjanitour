<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Post
           *
           * @author Habib Koe Tkj
           */
          class Posts extends CI_Controller
          {
                    private $web_title = 'Info Lombok | Royal Tour Operator and Mount Rinjani Trekking Packages';
                    
                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('post_model');
                              $this->load->model('bnb_model');
                              $this->load->model('programactivities_model');
                              $this->load->model('home_model');
                              $this->load->model('attribut_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name'=>$this->web_title,
                                             'page_base'=>'Info',
                                             'page_name'=>'Index of Information about Lombok',
                                             'posts'=>  $this->post_model->get(),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/posts', $data);
                    }
                    
                    public function getInfo()
                    {
                              $data = array(
                                             'app_name'=>$this->web_title,
                                             'page_base'=>'Info',
                                             'page_name'=>'Index of Information about Lombok',
                                             
                                             'posts'=>  $this->post_model->getByType('info'),
                                             'bnb_side'=> $this->bnb_model->get(4),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/posts', $data);
                    }
                    
                    
                    public function getAbout()
                    {
                              $data = array(
                                             'app_name'=>'About | Royal Rinjani Tour',
                                             'page_base'=>'Info',
                                             'page_name'=>'About Royal Rinjani Tour',
                                             'posts_one'=>  $this->post_model->getOneByType('page',12),
                                             'bnb_side'=> $this->bnb_model->get(),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/about_us', $data);
                    }
                    
                    public function view($url)
                    {
                              $data = array(
                                             'app_name'=>$this->web_title,
                                             'page_base'=>'Info',
                                             'page_name'=>'Index of Information about Lombok',
                                             
                                             'posts_one'=>  $this->post_model->findByUrl($url),
                                             'posts_side'=>  $this->post_model->getByType('info',4),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/posts_one', $data);
                    }
                    
                    public function getHelp($url)
                    {
                              $data = array(
                                             'app_name'=>'Help | Royal Rinjani Tour',
                                             'page_base'=>'Info',
                                             'page_name'=>'',
                                             
                                             'posts_one'=>  $this->post_model->findByUrl($url),
                                             'posts_side'=>  $this->post_model->getByType('info',4),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/help_one', $data);
                    }
                    
                    public function getOther($url)
                    {
                              $data = array(
                                             'app_name'=>'Other | Royal Rinjani Tour',
                                             'page_base'=>'Info',
                                             'page_name'=>'',
                                             
                                             'posts_one'=>  $this->post_model->findByUrl($url),
                                             'posts_side'=>  $this->post_model->getByType('info',4),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/other_one', $data);
                    }
                    
                    public function getByCategory($category)
                    {
                              
                              $data_category = $this->attribut_model->findByUrl($category);
                              
                              $data = array(
                                             'app_name'=>$data_category->at_name.' | Royal Tour Operator and Mount Rinjani Trekking Packages',
                                             'page_base'=>'Info',
                                             'page_name'=>'Index of '.$data_category->at_name,
                                             
                                             'bnb_side'=> $this->bnb_model->get(4),
                                             'posts_by_url'=>  $this->post_model->getByCategory($category),
                                             'program_activities_side'=>  $this->programactivities_model->get(4),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/posts_by_url', $data);
                    }
                    
                    public function getQa()
                    {
                              $data = array(
                                             'app_name'=>'Question and Answer | Royal Rinjani Tour',
                                             'page_base'=>'Info',
                                             'page_name'=>'Question and Answer Royal Rinjani Tour',
                                             'bnb_side'=> $this->bnb_model->get(),
                                             
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/qa', $data);
                    }
          }
          