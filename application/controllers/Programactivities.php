<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of ProgramActivities
           *
           * @author Habib Koe Tkj
           */
          class ProgramActivities extends CI_Controller
          {
                    private $web_title = 'Programs | Royal Tour Operator and Mount Rinjani Trekking Packages';

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->model('programactivities_model');
                              $this->load->model('post_model');
                              $this->load->model('bnb_model');
                              $this->load->model('home_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Program Activities',
                                             'page_name' => 'Index of Programs',
                                             'program_activities' => $this->programactivities_model->get(),
                                             
                                             'bnb_side'=> $this->bnb_model->get(4),
                                             'posts_side' => $this->post_model->getByType('info', 4),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/program_activities', $data);
                    }

                    public function view($url)
                    {
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Program Activities',
                                             'page_name' => '',
                                             'program_activities_one' => $this->programactivities_model->findByUrl($url),
                                             'program_activities_side' => $this->programactivities_model->get(4),
                                             'posts_side' => $this->post_model->getByType('info', 4),
                                             'web_setting'=>  $this->home_model->getWebSetting(),
                                             'help_footer'=>$this->home_model->getPostByCategory(15),
                                             'other_footer'=>$this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('home/program_activities_one', $data);
                    }

          }
          