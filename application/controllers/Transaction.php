<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Transaction
           *
           * @author Habib Koe Tkj
           */
          class Transaction extends CI_Controller
          {

                    private $web_title = 'Transaction | Royal Tour Operator and Mount Rinjani Trekking Packages';
                    private $bID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('customerin');
                              $this->bID = $this->session->userdata('or_booking_number');
                              if ($session == FALSE) {
                                        redirect('customer-login');
                                        exit();
                              }
                              $this->load->model('home_model');
                              $this->load->model('booking_model');
                              $this->load->model('transaction_model');
                    }

                    public function index()
                    {
                              $booking = $this->booking_model->find($this->bID);
                              if ($booking->or_type == 'programs') {
                                        $booking_list_one = $this->booking_model->findBookingList();
                              }
                              else {
                                        $booking_list_one = $this->booking_model->findBookingBnbList();
                              }
                              $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Transaction',
                                             'page_name' => '',
                                             'booking_one' => $this->booking_model->find($this->bID),
                                             'booking_list_one' => $booking_list_one,
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                              );
                              $this->load->view('layout/main', $data);
                              $this->load->view('transaction/index', $data);
                    }

                    public function confirm($booking_id)
                    {
                              $this->form_validation->set_rules('bc_bank_account', 'Bank Account', 'required');
                              $this->form_validation->set_rules('bc_bank', 'Bank Name', 'required');
                              $this->form_validation->set_rules('bc_total_paid', 'Total Paid', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => $this->web_title,
                                                       'page_base' => 'Transaction',
                                                       'page_name' => '',
                                                       'booking_one' => $this->booking_model->find($booking_id),
                                                       'booking_list_one' => $this->booking_model->findBookingList(),
                                                       'web_setting' => $this->home_model->getWebSetting(),
                                                       'help_footer' => $this->home_model->getPostByCategory(15),
                                                       'other_footer' => $this->home_model->getPostByCategory(16),
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('transaction/index', $data);
                              }
                              else {
                                        $booking_one = $this->booking_model->find($booking_id);
                                        $this->load->library('email');
                                        $this->email->set_mailtype('html');
                                        $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                        $this->email->to('royalrinjanitour@gmail.com');
                                        $this->email->subject($this->input->post('or_full_name') . ' Confirm Booking Programs');

                                        $data_admin = array(
                                                       'id'=>$booking_one->id,
                                                       'bc_bank_account' => $this->input->post('bc_bank_account'),
                                                       'bc_bank' => $this->input->post('bc_bank'),
                                                       'bc_total_paid' => $this->input->post('bc_total_paid'),
                                                       'or_email' => $booking_one->or_email,
                                                       'or_full_name' => $booking_one->or_full_name,
                                                       'or_phone' => $booking_one->or_phone,
                                                       'or_arrival_date' => $booking_one->or_arrival_date,
                                                       'or_drop_off_to' => $booking_one->or_drop_off_to,
                                                       'or_payment_method' => $booking_one->or_payment_method,
                                                       'or_date' => date('d-m-Y H:i:s'),
                                                       'or_status' => 'paid'
                                        );
                                        $message_admin = $this->load->view('notification/email_confirm_programs_to_admin', $data_admin, TRUE);
                                        $this->email->message($message_admin);

                                        $confirm = $this->transaction_model->confirm($booking_id);

                                        if ($confirm) {
                                                  $send_email_admin = $this->email->send();

                                                  if ($send_email_admin) {
                                                            
                                                            $booking_one_member = $this->booking_model->find($booking_id);
                                                            $this->load->library('email');
                                                            $this->email->set_mailtype('html');
                                                            $this->email->from('admin@royalrinjanitour.com', 'Royalrinjanitour.com');
                                                            $this->email->to($booking_one_member->or_email);
                                                            $this->email->subject('Confirm Booking Programs Royal Rinjani Tour');

                                                            $data_member = array(
                                                                           'id'=>$booking_one_member->id,
                                                                           'bc_bank_account' => $this->input->post('bc_bank_account'),
                                                                           'bc_bank' => $this->input->post('bc_bank'),
                                                                           'bc_total_paid' => $this->input->post('bc_total_paid'),
                                                                           'or_email' => $booking_one_member->or_email,
                                                                           'or_full_name' => $booking_one_member->or_full_name,
                                                                           'or_phone' => $booking_one_member->or_phone,
                                                                           'or_arrival_date' => $booking_one_member->or_arrival_date,
                                                                           'or_drop_off_to' => $booking_one_member->or_drop_off_to,
                                                                           'or_payment_method' => $booking_one_member->or_payment_method,
                                                                           'or_date' => date('d-m-Y H:i:s'),
                                                                           'or_status' => 'paid'
                                                            );
                                                            
                                                            $message_to_member = $this->load->view('notification/email_confirm_programs_booking', $data_member, TRUE);
                                                            $this->email->message($message_to_member);
                                                            $this->email->send();
                                                  }
                                        }
                                        redirect('transaction');
                              }
                    }

                    public function view()
                    {
                              
                    }

          }
          