<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Bnb
           *
           * @author Habib Koe Tkj
           */
          class Bnb extends CI_Controller
          {
                    
                    private $aID;
                    
                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('attribut_model');
                              $this->load->model('bnb_model');
                    }
                    
                    
                    public function index()
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Bed and Breakfast',
                                             'page_name' => 'All bnb',
                                             'all_bnb'=> $this->bnb_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('bnb/index', $data);
                    }

                    public function post()
                    {
                              $this->form_validation->set_rules('bnb_name', 'Bed and Breakfast Name', 'required');
                              $this->form_validation->set_rules('bnb_type', 'Bed and Breakfast Type', 'required');
                              $this->form_validation->set_rules('bnb_short_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Bed and Breakfast',
                                                       'page_name' => 'Create bnb'
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('bnb/post', $data);
                              }
                              else {
                                        $this->bnb_model->post($this->aID);
                                        redirect('backend/bnb');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('bnb_name', 'Bed and Breakfast Title', 'required');
                              $this->form_validation->set_rules('bnb_short_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Bed and Breakfast',
                                                       'page_name' => 'Edit bnb',
                                                       'bnb_one' => $this->bnb_model->find($id)
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('bnb/put', $data);
                              }
                              else {
                                        $this->bnb_model->put($this->aID, $id);
                                        redirect('backend/bnb');
                              }
                    }

                    public function find($id)
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Bed and Breakfast',
                                             'page_name' => 'All bnb'
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('bnb/index', $data);
                    }

                    public function delete($id)
                    {
                              $this->bnb_model->delete($id);
                              redirect('backend/bnb');
                    }
          }
          