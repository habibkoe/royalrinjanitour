<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Booking
           *
           * @author Habib Koe Tkj
           */
          class Booking extends CI_Controller
          {
                    private $aID;
                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('booking_model');
                              $this->load->model('transaction_model');
                              $this->load->model('bnb_model');
                    }
                    
                    public function index()
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'Booking',
                                             'page_name'=>'All Booking',
                                             'recent_booking' => $this->booking_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('booking/index', $data);
                    }
                    
                    public function post()
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'Booking',
                                             'page_name'=>'Post Booking'
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('booking/index', $data);
                    }
                    
                    public function disable($id)
                    {
                              $this->booking_model->disable($id);
                                        redirect('backend/booking');
                    }
                    
                    public function view($id)
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'Booking',
                                             'page_name'=>'booking programs',
                                             'booking_one' => $this->booking_model->find($id),
                                             'booking_confirm' => $this->booking_model->find_booking_confirm($id),
                                             'booking_list_one' => $this->booking_model->findBookingList()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('booking/view', $data);
                    }
                    
                    public function view_bnb($id)
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'Booking',
                                             'page_name'=>'booking bnb',
                                             'booking_one' => $this->booking_model->find($id),
                                             
                                             'booking_confirm' => $this->booking_model->find_booking_confirm($id),
                                             
                                             'booking_list_one' => $this->booking_model->findBookingBnbList()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('booking/view_bnb', $data);
                    }
                    
                    public function view_rac($id)
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'Booking',
                                             'page_name'=>'View booking',
                                             'booking_one' => $this->booking_model->find($id),
                                             'booking_confirm' => $this->booking_model->find_booking_confirm($id),
                                             'booking_list_one' => $this->booking_model->findBookingRacList()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('booking/view_rac', $data);
                    }
                    
                    public function confirm($booking_id)
                    {
                              $this->form_validation->set_rules('bc_bank_account', 'Bank Account', 'required');
                              $this->form_validation->set_rules('bc_bank', 'Bank Name', 'required');
                              $this->form_validation->set_rules('bc_total_paid', 'Total Paid', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                             'app_name' => $this->web_title,
                                             'page_base' => 'Transaction',
                                             'page_name' => '',
                                             'booking_one' => $this->booking_model->find($booking_id),
                                             'booking_list_one' => $this->booking_model->findBookingList(),
                                             'web_setting' => $this->home_model->getWebSetting(),
                                             'help_footer' => $this->home_model->getPostByCategory(15),
                                             'other_footer' => $this->home_model->getPostByCategory(16),
                                        );
                                        $this->load->view('layout/main', $data);
                                        $this->load->view('transaction/index', $data);
                              }
                              else {
                                        $this->transaction_model->confirm($booking_id);
                                        redirect('transaction');
                              }
                              
                    }
                    
                    public function delete($id)
                    {
                              $this->booking_model->delete($id);
                              redirect('backend/booking');
                    }
          }
          