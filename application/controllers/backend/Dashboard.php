<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Dashboard
           *
           * @author Habib Koe Tkj
           */
          class Dashboard extends CI_Controller
          {

                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('dashboard_model');
                              $this->load->model('post_model');
                              $this->load->model('bnb_model');
                              $this->load->model('programactivities_model');
                              $this->load->model('booking_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Dashboard',
                                             'page_name' => '',
                                             
                                             'recent_post' => $this->post_model->getByType('info',5),
                                             'recent_bnb' => $this->bnb_model->get(5),
                                             'total_transaction' => $this->booking_model->total_transaction(),
                                             'total_visitor' => $this->dashboard_model->total_visitor(),
                                             'total_transaction_paid' => $this->booking_model->total_transaction_paid(),
                                             'total_transaction_unpaid' => $this->booking_model->total_transaction_unpaid(),
                                             'total_transaction_disable' => $this->booking_model->total_transaction_disable(),
                                             
                                             'recent_program' => $this->programactivities_model->get(5),
                                             'recent_booking' => $this->booking_model->get(5)
                                                  
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('dashboard/index', $data);
                              //$this->load->view('layout/footer_admin');
                    }

                    public function setting($id)
                    {
                              $this->form_validation->set_rules('web_title', 'Judul website', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Dashboard',
                                             'page_name' => '',
                                             'web_setting' => $this->dashboard_model->find_setting($id)
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('dashboard/setting', $data);
                              }
                              else {
                                        $this->dashboard_model->put_setting($id);
                                        redirect('backend/dashboard/setting/'.$id);
                              }
                              
                    }

          }
          