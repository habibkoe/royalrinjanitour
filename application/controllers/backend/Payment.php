<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Payment
           *
           * @author Habib Koe Tkj
           */
          class Payment extends CI_Controller
          {
                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('attribut_model');
                    }

                    public function index()
                    {
                              $all_attribut = $this->attribut_model->get();
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Attribut',
                                             'page_name' => 'All attribut',
                                             'all_attribut'=>$all_attribut
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('attribut/index', $data);
                    }

                    public function post()
                    {
                              $this->form_validation->set_rules('at_name', 'Judul Attribut', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Attribut',
                                                       'page_name' => 'Post attribut'
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('attribut/post', $data);
                              }
                              else {
                                        $this->attribut_model->post();
                                        redirect('backend/attribut');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('at_name', 'Judul Attribut', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Attribut',
                                                       'page_name' => 'Edit attribut',
                                                       'attribut_one' => $this->attribut_model->find($id)
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('attribut/put', $data);
                              }
                              else {
                                        $this->attribut_model->put($id);
                                        redirect('backend/attribut');
                              }
                    }
                    
                    public function delete($id)
                    {
                              $this->attribut_model->delete($id);
                              redirect('backend/attribut');
                    }
          }
          