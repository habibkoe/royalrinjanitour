<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of News
           *
           * @author Habib Koe Tkj
           */
          class Posts extends CI_Controller
          {

                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('attribut_model');
                              $this->load->model('post_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Post',
                                             'page_name' => 'All post',
                                             'all_post'=> $this->post_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('post/index', $data);
                    }

                    public function post()
                    {
                              $this->form_validation->set_rules('posts_title', 'Post Title', 'required');
                              $this->form_validation->set_rules('posts_type', 'Post Type', 'required');
                              $this->form_validation->set_rules('posts_category', 'Post Category', 'required');
                              $this->form_validation->set_rules('posts_sort_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Post',
                                                       'page_name' => 'Create post',
                                                       'category' => $this->attribut_model->getByFor('post')
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('post/post', $data);
                              }
                              else {
                                        $this->post_model->post($this->aID);
                                        redirect('backend/posts');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('posts_title', 'Post Title', 'required');
                              $this->form_validation->set_rules('posts_category', 'Post Category', 'required');
                              $this->form_validation->set_rules('posts_short_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Post',
                                                       'page_name' => 'Edit post',
                                                       'category' => $this->attribut_model->getByFor('post'),
                                                       'post_one' => $this->post_model->find($id)
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('post/put', $data);
                              }
                              else {
                                        $this->post_model->put($this->aID, $id);
                                        redirect('backend/posts');
                              }
                    }

                    public function find($id)
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Post',
                                             'page_name' => 'All post'
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('post/index', $data);
                    }

                    public function delete($id)
                    {
                              $this->post_model->delete($id);
                              redirect('backend/posts');
                    }

          }
          