<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of ProgramActivities
           *
           * @author Habib Koe Tkj
           */
          class Programactivities extends CI_Controller
          {
                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('programactivities_model');
                              $this->load->model('attribut_model');
                    }

                    public function index()
                    {
                              $all_programactivities = $this->programactivities_model->get();
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Programs',
                                             'page_name' => 'All programs',
                                             'all_programactivities'=>$all_programactivities
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('program_activities/index', $data);
                    }

                    public function post()
                    {
                              $this->form_validation->set_rules('category', 'Category', 'required');
                              $this->form_validation->set_rules('pr_title', 'Judul Program Activities', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Programs',
                                                       'page_name' => 'Create programs',
                                                       'category' => $this->attribut_model->getByFor('program-activities')
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('program_activities/post', $data);
                              }
                              else {
                                        $this->programactivities_model->post($this->aID);
                                        redirect('backend/programactivities');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('category', 'Category', 'required');
                              $this->form_validation->set_rules('pr_title', 'Judul Program Activities', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Programs',
                                                       'page_name' => 'Edit programs',
                                                       'category' => $this->attribut_model->getByFor('program-activities'),
                                                       'programactivities_one' => $this->programactivities_model->find($id)
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('program_activities/put', $data);
                              }
                              else {
                                        $this->programactivities_model->put($this->aID, $id);
                                        redirect('backend/programactivities');
                              }
                    }
                    
                    public function delete($id)
                    {
                              $this->programactivities_model->delete($id);
                              redirect('backend/programactivities');
                    }
          }
          