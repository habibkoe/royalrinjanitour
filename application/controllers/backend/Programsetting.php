<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Programsetting
           *
           * @author Habib Koe Tkj
           */
          class Programsetting extends CI_Controller
          {
                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('programsetting_model');
                              $this->load->model('post_model');
                    }
                    
                    public function post()
                    {
                              $this->form_validation->set_rules('prs_name', 'Judul Program Activities', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Program Setting',
                                                       'page_name' => 'B&B Rent a car',
                                                       'bnb' => $this->post_model->getByType('bb_rent_a_car'),
                                                       'all_program_setting' => $this->programsetting_model->get()
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('program_activities/setting', $data);
                              }
                              else {
                                        $this->programsetting_model->post();
                                        redirect('backend/programsetting/post');
                              }
                    }
                    
                    public function put($id)
                    {
                              $this->form_validation->set_rules('prs_name', 'Judul Program Activities', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Program Setting',
                                                       'page_name' => 'B&B Rent a car',
                                                       'bnb' => $this->post_model->getByType('bb_rent_a_car'),
                                                       'program_setting_one'=>  $this->programsetting_model->find($id)
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('program_activities/put_setting', $data);
                              }
                              else {
                                        $this->programsetting_model->put($id);
                                        redirect('backend/programsetting/post');
                              }
                    
                              
                    }
                    public function delete($id)
                    {
                              $this->programsetting_model->delete($id);
                              redirect('backend/programsetting/post');
                    }
          }
          