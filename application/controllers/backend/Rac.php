<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Rac
           *
           * @author Habib Koe Tkj
           */
          class Rac extends CI_Controller
          {
                    private $aID;
                    
                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              $this->load->model('attribut_model');
                              $this->load->model('rac_model');
                    }
                    
                    public function index()
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Rent a Car',
                                             'page_name' => 'All rac',
                                             'all_rac'=> $this->rac_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('rac/index', $data);
                    }

                    public function post()
                    {
                              $this->form_validation->set_rules('rac_name', 'Rent a Car Name', 'required');
                              $this->form_validation->set_rules('rac_type', 'Rent a Car Type', 'required');
                              $this->form_validation->set_rules('rac_short_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Rent a Car',
                                                       'page_name' => 'Create rac'
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('rac/post', $data);
                              }
                              else {
                                        $this->rac_model->post($this->aID);
                                        redirect('backend/rac');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('rac_name', 'Rent a Car Title', 'required');
                              $this->form_validation->set_rules('rac_short_description', 'Short Description', 'max_length[230]');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Rent a Car',
                                                       'page_name' => 'Edit rac',
                                                       'rac_one' => $this->rac_model->find($id)
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('rac/put', $data);
                              }
                              else {
                                        $this->rac_model->put($this->aID, $id);
                                        redirect('backend/rac');
                              }
                    }

                    public function find($id)
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Rent a Car',
                                             'page_name' => 'All rac'
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('rac/index', $data);
                    }

                    public function delete($id)
                    {
                              $this->rac_model->delete($id);
                              redirect('backend/rac');
                    }
          }
          