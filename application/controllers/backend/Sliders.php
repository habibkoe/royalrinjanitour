<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Slider
           *
           * @author Habib Koe Tkj
           */
          class Sliders extends CI_Controller
          {

                    private $aID;

                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }

                              $this->load->model('slider_model');
                    }

                    public function index()
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Slider',
                                             'page_name' => 'All slider',
                                             'all_slider'=> $this->slider_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('slider/index', $data);
                    }

                    public function post()
                    {

                              $this->form_validation->set_rules('slider_title', 'Judul Slider', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'Slider',
                                                       'page_name' => 'Create slider'
                                        );
                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('slider/post', $data);
                              }
                              else {
                                        $this->slider_model->post($this->aID);
                                        redirect('backend/sliders');
                              }
                    }

                    public function put($id)
                    {
                              $this->form_validation->set_rules('slider_title', 'Judul Slider', 'required');
                              if ($this->form_validation->run() == FALSE) {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Slider',
                                             'page_name' => 'Edit slider',
                                             'slider_one'=>$this->slider_model->find($id)
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('slider/put', $data);
                              }
                              else {
                                        $this->slider_model->put($this->aID, $id);
                                        redirect('backend/sliders');
                              }
                    }

                    public function find($id)
                    {
                              $data = array(
                                             'app_name' => 'Admin Royal',
                                             'page_base' => 'Slider',
                                             'page_name' => 'All slider'
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('slider/index', $data);
                    }

                    public function delete($id)
                    {
                              $this->slider_model->delete($id);
                              redirect('backend/sliders');
                    }

          }
          