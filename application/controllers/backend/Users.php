<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Use
           *
           * @author Habib Koe Tkj
           */
          class Users extends CI_Controller
          {
                    private $aID;
                    public function __construct()
                    {
                              parent::__construct();
                              $session = $this->session->userdata('loggedin');
                              $this->aID = $this->session->userdata('admin_id');
                              if ($session == FALSE) {
                                        redirect('login');
                                        exit();
                              }
                              
                              $this->load->model('user_model');
                    }
                    
                    public function index()
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'User',
                                             'page_name'=>'All user',
                                             'all_user'=>  $this->user_model->get()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('user/index', $data);
                    }
                    
                    public function post()
                    {
                              $this->form_validation->set_rules('admin_name', 'Nama user', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'User',
                                                       'page_name' => 'Create user'
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('user/post', $data);
                              }
                              else {
                                        $this->user_model->post();
                                        redirect('backend/users');
                              }
                    }
                    
                    public function put($id)
                    {
                              $this->form_validation->set_rules('admin_name', 'Nama user', 'required');
                              if ($this->form_validation->run() == FALSE) {

                                        $data = array(
                                                       'app_name' => 'Admin Royal',
                                                       'page_base' => 'User',
                                                       'page_name' => 'Edit user',
                                                       'user_one' => $this->user_model->find($id)
                                        );

                                        $this->load->view('layout/main_admin', $data);
                                        $this->load->view('layout/sidebar_admin', $data);
                                        $this->load->view('user/put', $data);
                              }
                              else {
                                        $this->user_model->put($id);
                                        redirect('backend/users');
                              }
                    }
                    
                    public function find($id)
                    {
                              
                    }
                    
                    public function delete($id)
                    {
                              $this->user_model->delete($id);
                              redirect('backend/users');
                    }
                    
                    
                    public function getVisitor()
                    {
                              $data = array(
                                             'app_name'=>'Admin Royal',
                                             'page_base'=>'User',
                                             'page_name'=>'All user',
                                             'all_visitor'=>  $this->user_model->getVisitor()
                              );
                              $this->load->view('layout/main_admin', $data);
                              $this->load->view('layout/sidebar_admin', $data);
                              $this->load->view('user/visitor', $data);
                    }
          }
          