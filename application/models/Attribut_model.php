<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Attribut_model
           *
           * @author Habib Koe Tkj
           */
          class Attribut_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function get()
                    {
                              $query = $this->db->order_by('at_name', 'desc')
                                                            ->get('attribut');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getByFor($for)
                    {
                              $query = $this->db->order_by('at_name', 'desc')
                                                            ->where('at_for', $for)
                                                            ->get('attribut');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post()
                    {
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('at_name'));
                              $data = array(
                                             'at_name' => $this->input->post('at_name'),
                                             'at_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'at_for' => $this->input->post('at_for'),
                                             'at_description' => $this->input->post('at_description'),
                              );
                              $this->db->insert('attribut', $data);
                    }

                    public function put($id)
                    {
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('at_name'));
                              $data = array(
                                             'at_name' => $this->input->post('at_name'),
                                             'at_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'at_for' => $this->input->post('at_for'),
                                             'at_description' => $this->input->post('at_description'),
                              );
                              $this->db->where('at_id', $id)
                                             ->update('attribut', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('at_id', $id)
                                             ->delete('attribut');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('at_id', $id)
                                                            ->limit(1)
                                                            ->get('attribut');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function findByUrl($url)
                    {
                              $query = $this->db->where('at_url', $url)
                                                            ->limit(1)
                                                            ->get('attribut');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

          }
          