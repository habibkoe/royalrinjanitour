<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Bnb_model
           *
           * @author Habib Koe Tkj
           */
          class Bnb_model extends CI_Model
          {
                    
                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->library('upload');
                    }

                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->limit($limit)
                                                            ->join('admin','admin.admin_id = bnb.author', 'left')
                                                            ->get('bnb');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post($aID)
                    {
                              if(empty($_POST['bnb_image'])){
                              
                                        $post_image = 'No image';
                              }else{
                                        $post_image = $this->input->post('bnb_image');
                              }

                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('bnb_name'));
                              $data = array(
                                             'id'=> (time() + 583) - 15439,
                                             'bnb_name' => $this->input->post('bnb_name'),
                                             'bnb_description' => $this->input->post('bnb_description'),
                                             'bnb_short_description' => $this->input->post('bnb_short_description'),
                                             'bnb_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'bnb_image' => $post_image,
                                             'bnb_image_credit' => $this->input->post('bnb_image_credit'),
                                             'bnb_type' => $this->input->post('bnb_type'),
                                             'bnb_stock' => $this->input->post('bnb_stock'),
                                             'bnb_capacity' => $this->input->post('bnb_capacity'),
                                             'bnb_price' => $this->input->post('bnb_price'),
                                             'bnb_status' => $this->input->post('bnb_status'),
                                             'author' => $aID
                              );
                              $this->db->insert('bnb', $data);
                    }

                    public function put($aID, $id)
                    {
                              if(empty($_POST['bnb_image'])){
                              
                                        $post_image = $this->input->post('post_image_old');
                              }else{
                                        $post_image = $this->input->post('bnb_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('bnb_name'));
                              $data = array(
                                             'bnb_name' => $this->input->post('bnb_name'),
                                             'bnb_description' => $this->input->post('bnb_description'),
                                             'bnb_short_description' => $this->input->post('bnb_short_description'),
                                             'bnb_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'bnb_image' => $post_image,
                                             'bnb_image_credit' => $this->input->post('bnb_image_credit'),
                                             'bnb_type' => $this->input->post('bnb_type'),
                                             'bnb_stock' => $this->input->post('bnb_stock'),
                                             'bnb_capacity' => $this->input->post('bnb_capacity'),
                                             'bnb_price' => $this->input->post('bnb_price'),
                                             'bnb_status' => $this->input->post('bnb_status'),
                                             'author' => $aID
                              );
                              $this->db->where('id', $id)
                                        ->update('bnb', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                        ->delete('bnb');
                    }

                    public function find($id = NULL)
                    {
                              $query = $this->db->where('id', $id)
                                                            ->join('admin','admin.admin_id = bnb.author', 'left')
                                                            ->limit(1)
                                                            ->get('bnb');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function findByUrl($url = NULL)
                    {
                              $query = $this->db->where('bnb_url', $url)
                                                            ->join('admin','admin.admin_id = bnb.author', 'left')
                                                            ->limit(1)
                                                            ->get('bnb');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
          }
          