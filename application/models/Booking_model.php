<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of booking
           *
           * @author Habib Koe Tkj
           */
          class Booking_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('created_at', 'desc')
                                                            ->limit($limit)
                                                            ->get('booking');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    //PROGRAMS AREA================================================================================
                    public function post($rowid = NULL)
                    {
                              $data_insert = array(
                                             'id' => time() + 1993,
                                             'or_type' => 'programs',
                                             'or_full_name' => $this->input->post('or_full_name'),
                                             'or_phone' => $this->input->post('or_phone'),
                                             'or_home_phone' => $this->input->post('or_home_phone'),
                                             'or_email' => $this->input->post('or_email'),
                                             'or_address' => $this->input->post('or_address'),
                                             'or_departure_date' => $this->input->post('or_departure_date'),
                                             'or_arrival_date' => $this->input->post('or_arrival_date'),
                                             'or_pick_up_from' => $this->input->post('or_pick_up_from'),
                                             'or_drop_off_to' => $this->input->post('or_drop_off_to'),
                                             'or_payment_method' => $this->input->post('or_payment_method'),
                                             'or_medical_condition' => $this->input->post('or_medical_condition'),
                                             'or_special_diets' => $this->input->post('or_special_diets'),
                                             'or_message' => $this->input->post('or_message'),
                                             'or_price' => $this->input->post('or_price'),
                                             'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                             'or_status' => 'unpaid'
                              );
                              $this->db->insert('booking', $data_insert);
                              foreach ($this->cart->contents() as $items) {
                                        if($items['rowid'] == $rowid){
                                                  $data_list = array(
                                                                 'booking_id' => time() + 1993,
                                                                 'programs_id' => $items['id'],
                                                                 'booking_programs_name' => $items['name'],
                                                                 'booking_guest_name' => $this->input->post('or_guest_name'),
                                                                 'booking_guest_total' => $this->input->post('booking_guest_total'),
                                                                 'booking_price_includes' => $this->input->post('or_price_includes'),
                                                                 'booking_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                                 'qty' => $items['qty'],
                                                                 'price' => $items['price']
                                                  );
                                                  $booking_list = $this->db->insert('booking_list', $data_list);
                                        }
                              }
                              return TRUE;
                    }
                    
                    //BNB AREA==============================================================================================
                    public function postBnb($rowid = NULL)
                    {
                              $data_insert = array(
                                             'id' => time() + 563,
                                             'or_type' => 'bnb',
                                             'or_full_name' => $this->input->post('or_full_name'),
                                             'or_phone' => $this->input->post('or_phone'),
                                             'or_home_phone' => $this->input->post('or_home_phone'),
                                             'or_email' => $this->input->post('or_email'),
                                             'or_address' => $this->input->post('or_address'),
                                             'or_departure_date' => $this->input->post('or_checkin_date'),
                                             'or_arrival_date' => $this->input->post('or_checkout_date'),
                                             'or_long_night'=>  $this->input->post('or_long_night'),
                                             'or_payment_method' => $this->input->post('or_payment_method'),
                                             'or_special_diets' => $this->input->post('or_special_diets'),
                                             'or_message' => $this->input->post('or_message'),
                                             'or_price' => $this->input->post('or_price'),
                                             'or_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                             'or_status' => 'unpaid'
                              );
                              $this->db->insert('booking', $data_insert);
                              foreach ($this->cart->contents() as $items) {
                                         if($items['rowid'] == $rowid){
                                                  $data_list = array(
                                                                 'booking_id' => time() + 563,
                                                                 'programs_id' => $items['id'],
                                                                 'booking_programs_name' => $items['name'],
                                                                 'booking_guest_name' => $this->input->post('or_guest_name'),
                                                                 'booking_guest_total' => $this->input->post('booking_guest_total'),
                                                                 'booking_price_includes' => $this->input->post('or_price_includes'),
                                                                 'booking_due_date' => date('Y-m-d H:i:s', mktime(date('H'), date('i'), date('s'), date('m'), date('d') + 1, date('Y'))),
                                                                 'qty' => $items['qty'],
                                                                 'price' => $items['price']
                                                  );
                                                  $booking_list = $this->db->insert('booking_list', $data_list);
                                                  $this->db->where('id', $items['id']) ->update('bnb',array ('bnb_stock'=>'full'));
                                         }
                              }
                                        
                              
                              
                              return TRUE;
                    }

                    public function disable($id)
                    {
                              
                              $data = array(
                                             'or_status' => 'disable',
                              );
                              $this->db->where('id', $id)
                                             ->update('booking', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                             ->delete('booking');
                    }

                    public function find($id = NULL)
                    {
                              $query = $this->db->where('booking.id', $id)
                                                            ->limit(1)
                                                            ->join('booking_list', 'booking_list.booking_id = booking.id', 'left')
                                                            ->get('booking');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function find_booking_confirm($booking_id = NULL)
                    {
                              $query = $this->db->where('booking_id', $booking_id)
                                                            ->limit(1)
                                                            ->get('booking_confirm');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function findBookingList($booking_id = NULL)
                    {
                              $query = $this->db->select('*')
                                                            ->join('program_activities', 'program_activities.id = booking_list.programs_id', 'left')
                                                            ->get('booking_list');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function findBookingBnbList($booking_id = NULL)
                    {
                              $query = $this->db->select('*')
                                                            ->join('bnb', 'bnb.id = booking_list.programs_id', 'left')
                                                            ->get('booking_list');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function findBookingRacList($booking_id = NULL)
                    {
                              $query = $this->db->select('*')
                                                            ->join('rac', 'rac.id = booking_list.programs_id', 'left')
                                                            ->get('booking_list');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function total_transaction() {
                              $select = array(
                                             'count(id) as total'
                              );
                              $query = $this->db->select($select)
                                                            ->from('booking')
                                                            ->get();
                               if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    
                    public function total_transaction_unpaid() {
                              $select = array(
                                             'count(id) as total'
                              );
                              $query = $this->db->select($select)
                                                            ->from('booking')
                                                            ->where('or_status', 'unpaid')
                                                            ->get();
                               if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    
                    public function total_transaction_disable() {
                              $select = array(
                                             'count(id) as total'
                              );
                              $query = $this->db->select($select)
                                                            ->from('booking')
                                                            ->where('or_status', 'disable')
                                                            ->get();
                               if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }
                    
                    public function total_transaction_paid() {
                              $select = array(
                                             'count(id) as total'
                              );
                              $query = $this->db->select($select)
                                                            ->from('booking')
                                                            ->where('or_status', 'paid')
                                                            ->get();
                               if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }

          }
          