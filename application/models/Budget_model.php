<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Budget_model
           *
           * @author Habib Koe Tkj
           */
          class Budget_model extends CI_Model
          {
                    public function __construct()
                    {
                              parent::__construct();
                    }
                    
                    public function getBudget($from, $cost, $far)
                    {
                              $query = $this->db->where('bu_cost <=', $cost)
                                                            ->where('bu_far <=', $far)
                                                            ->where('bu_from', $from)
                                                            ->get('budget');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
          }
          