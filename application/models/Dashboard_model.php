<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of dashboard
           *
           * @author Habib Koe Tkj
           */
          class Dashboard_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function find_setting($id)
                    {
                              $query = $this->db->where('id',$id)
                                                            ->get('web_setting');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function put_setting($id)
                    {
                              if (empty($_POST['web_logo'])) {
                                        $logo = $this->input->post('web_logo_old');
                              }
                              else {
                                        $logo = md5($this->input->post('web_logo'));
                              }

                              $data = array(
                                             'web_title' => $this->input->post('web_title'),
                                             'web_logo' => $logo,
                                             'web_owner' => $this->input->post('web_owner'),
                                             'web_author' => $this->input->post('web_author'),
                                             'web_meta' => $this->input->post('web_meta'),
                                             'web_description' => $this->input->post('web_description'),
                                             'web_office_address' => $this->input->post('web_office_address'),
                                             'web_maps' => $this->input->post('web_maps'),
                                             'web_call_center' => $this->input->post('web_call_center'),
                                             'web_link_1' => $this->input->post('web_link_1'),
                                             'web_link_2' => $this->input->post('web_link_2'),
                                             'web_link_3' => $this->input->post('web_link_3'),
                                             'web_bnb_notif' => $this->input->post('web_bnb_notif'),
                                             'web_rac_notif' => $this->input->post('web_rac_notif'),
                              );
                              $this->db->where('id', $id)
                                        ->update('web_setting', $data);
                    }
                    
                    public function total_visitor() {
                              $select = array(
                                             'count(id) as total'
                              );
                              $query = $this->db->select($select)
                                                            ->from('ci_sessions')
                                                            ->get();
                               if ($query->num_rows() > 0) {
                                        return $query->result();
                              } else {
                                        return array();
                              }
                    }

          }
          