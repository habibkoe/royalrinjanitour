<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of home_model
           *
           * @author Habib Koe Tkj
           */
          class Home_model extends CI_Model
          {
                    
                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function getPostByCategory($category, $limit = NULL)
                    {
                              $query = $this->db->order_by('created_at', 'desc')
                                                            ->where('category', $category)
                                                            ->where('posts_status','publish')
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->limit($limit)
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getProgramActivities($limit)
                    {
                              $query = $this->db->order_by('created_at', 'desc')
                                                            ->where('pr_status','publish')
                                                            ->limit($limit)
                                                            ->get('program_activities');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getSlider($limit, $limit2)
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->limit($limit, $limit2)
                                                            ->get('slider');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getWebSetting()
                    {
                              $query = $this->db->where('id', 1)
                                                            ->limit(1)
                                                            ->get('web_setting');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
          }
          