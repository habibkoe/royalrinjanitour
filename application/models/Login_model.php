<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of login_model
           *
           * @author Habib Koe Tkj
           */
          class Login_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function cek_data_user($password)
                    {
                              //$email = set_value('password');

                              $result = $this->db->where('admin_password', md5($password))
                                        ->limit(1)
                                        ->get('admin');
                              if ($result->num_rows() > 0) {
                                        return $result->row();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function cek_data_customer($or_email, $or_nomor_booking)
                    {
                              //$email = set_value('email');
                              $result = $this->db->where('or_email', $or_email)
                                                            ->where('id', $or_nomor_booking)
                                                            ->limit(1)
                                                            ->get('booking');
                              if ($result->num_rows() > 0) {
                                        return $result->row();
                              }
                              else {
                                        return array();
                              }
                    }

          }
          