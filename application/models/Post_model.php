<?php

          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of news
           *
           * @author Habib Koe Tkj
           */
          class Post_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->library('upload');
                    }

                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->limit($limit)
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post($aID)
                    {
                              if(empty($_POST['posts_image'])){
                              
                                        $post_image = 'No image';
                              }else{
                                        $post_image = $this->input->post('posts_image');
                              }

                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('posts_title'));
                              $data = array(
                                             'posts_title' => $this->input->post('posts_title'),
                                             'posts_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'posts_description' => $this->input->post('posts_description'),
                                             'posts_short_description' => $this->input->post('posts_short_description'),
                                             'category' => $this->input->post('posts_category'),
                                             'posts_image' => $post_image,
                                             'posts_image_credit' => $this->input->post('posts_image_credit'),
                                             'posts_status' => $this->input->post('posts_status'),
                                             'posts_type' => $this->input->post('posts_type'),
                                             'author' => $aID
                              );
                              $this->db->insert('posts', $data);
                    }

                    public function put($aID, $id)
                    {
                              if(empty($_POST['posts_image'])){
                              
                                        $post_image = $this->input->post('post_image_old');
                              }else{
                                        $post_image = $this->input->post('posts_image');
                              }
                              
                              if(empty($_POST['posts_type'])){
                              
                                        $posts_type = $this->input->post('posts_type_old');
                              }else{
                                        $posts_type = $this->input->post('posts_type');
                              }
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('posts_title'));
                              $data = array(
                                             'posts_title' => $this->input->post('posts_title'),
                                             'posts_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'posts_description' => $this->input->post('posts_description'),
                                             'posts_short_description' => $this->input->post('posts_short_description'),
                                             'category' => $this->input->post('posts_category'),
                                             'posts_image' => $post_image,
                                             'posts_status' => $this->input->post('posts_status'),
                                             'posts_type' => $posts_type,
                                             'posts_image_credit' => $this->input->post('posts_image_credit'),
                                             'author' => $aID
                              );
                              $this->db->where('id', $id)
                                        ->update('posts', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                        ->delete('posts');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('id', $id)
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->limit(1)
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getByCategory($category)
                    {
                              $query = $this->db->where('at_url', $category)
                                                            ->join('posts', 'posts.category = attribut.at_id', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->get('attribut');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getByType($type, $limit = NULL)
                    {
                              $query = $this->db->where('posts_type', $type)
                                                            ->where('posts_status','publish')
                                                            ->order_by('created_at','desc')
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->limit($limit)
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getByBnb()
                    {
                              $query = $this->db->where('posts_type', 'bb_rent_a_car')
                                                            ->where('posts_status','publish')
                                                            ->order_by('posts.created_at','desc')
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->join('program_setting','program_setting.prs_name = posts.id', 'left')
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getOneByType($type, $category = NULL)
                    {
                              $query = $this->db->where('posts_type', $type)
                                                            ->where('category', $category)
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function findByUrl($url)
                    {
                              $query = $this->db->where('posts_url', $url)
                                                            ->join('attribut', 'attribut.at_id = posts.category', 'left')
                                                            ->join('admin','admin.admin_id = posts.author', 'left')
                                                            ->join('program_setting','program_setting.prs_name = posts.id', 'left')
                                                            ->limit(1)
                                                            ->get('posts');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

          }
          