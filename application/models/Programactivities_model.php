<?php
          
          defined('BASEPATH') OR exit('No direct script access allowed');

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of ProgramActivities_Model
           *
           * @author Habib Koe Tkj
           */
          class Programactivities_model extends CI_Model
          {
                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->join('admin','admin.admin_id = program_activities.author', 'left')
                                                            ->join('attribut', 'attribut.at_id = program_activities.category', 'left')
                                                            ->limit($limit)
                                                            ->get('program_activities');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post($aID)
                    {
                              if(empty($_POST['pr_image'])){
                              
                                        $pr_image = 'No image';
                              }else{
                                        $pr_image = $this->input->post('pr_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('pr_title'));
                              $data = array(
                                             'id'=> (time() + 783) - 13930,
                                             'pr_title' => $this->input->post('pr_title'),
                                             'pr_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'pr_description' => $this->input->post('pr_description'),
                                             'pr_short_description' => $this->input->post('pr_short_description'),
                                             'pr_image' => $pr_image,
                                             'pr_image_credit' => $this->input->post('pr_image_credit'),
                                             'pr_owner' => 'Royal rinjani tour',
                                             'category' => $this->input->post('category'),
                                             'pr_price_1' => $this->input->post('pr_price_1'),
                                             'pr_price_2' => $this->input->post('pr_price_2'),
                                             'pr_price_3_4' => $this->input->post('pr_price_3_4'),
                                             'pr_price_5_6' => $this->input->post('pr_price_5_6'),
                                             'pr_price_7_8' => $this->input->post('pr_price_7_8'),
                                             'pr_price_9_10' => $this->input->post('pr_price_9_10'),
                                             'pr_price_11_20' => $this->input->post('pr_price_11_20'),
                                             'pr_stock' => 0,
                                             'pr_status' => $this->input->post('pr_status'),
                                             'author' => $aID
                                             
                              );
                              $this->db->insert('program_activities', $data);
                    }

                    public function put($aID, $id)
                    {
                              if(empty($_POST['pr_image'])){
                              
                                        $pr_image = $this->input->post('pr_image_old');
                              }else{
                                        $pr_image = $this->input->post('pr_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('pr_title'));
                              $data = array(
                                             'pr_title' => $this->input->post('pr_title'),
                                             'pr_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'pr_description' => $this->input->post('pr_description'),
                                             'pr_short_description' => $this->input->post('pr_short_description'),
                                             'pr_image' => $pr_image,
                                             'pr_image_credit' => $this->input->post('pr_image_credit'),
                                             'pr_owner' => 'Royal rinjani tour',
                                             'category' => $this->input->post('category'),
                                             'pr_price_1' => $this->input->post('pr_price_1'),
                                             'pr_price_2' => $this->input->post('pr_price_2'),
                                             'pr_price_3_4' => $this->input->post('pr_price_3_4'),
                                             'pr_price_5_6' => $this->input->post('pr_price_5_6'),
                                             'pr_price_7_8' => $this->input->post('pr_price_7_8'),
                                             'pr_price_9_10' => $this->input->post('pr_price_9_10'),
                                             'pr_price_11_20' => $this->input->post('pr_price_11_20'),
                                             'pr_stock' => 0,
                                             'pr_status' => $this->input->post('pr_status'),
                                             'pr_minimum_person' => $this->input->post('pr_minimum_person'),
                                             'author' => $aID
                              );
                              $this->db->where('id', $id)
                                             ->update('program_activities', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                             ->delete('program_activities');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('id', $id)
                                                            ->join('attribut', 'attribut.at_id = program_activities.category', 'left')
                                                            ->join('admin','admin.admin_id = program_activities.author', 'left')
                                                            ->limit(1)
                                                            ->get('program_activities');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function findByUrl($url)
                    {
                              $query = $this->db->where('pr_url', $url)
                                                            ->join('attribut', 'attribut.at_id = program_activities.category', 'left')
                                                            ->join('admin','admin.admin_id = program_activities.author', 'left')
                                                            ->limit(1)
                                                            ->get('program_activities');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
          }
          