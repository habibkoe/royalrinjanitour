<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Programsetting_model
           *
           * @author Habib Koe Tkj
           */
          class Programsetting_model extends CI_Model
          {
                    public function __construct()
                    {
                              parent::__construct();
                    }
                    
                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('program_setting.prs_id', 'desc')
                                                            ->join('posts','posts.id = program_setting.prs_name', 'left')
                                                            ->limit($limit)
                                                            ->get('program_setting');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getByCategory($category)
                    {
                              $query = $this->db->where('prs_category', $category)
                                                            ->join('posts','posts.id = program_setting.prs_name', 'left')
                                                            ->get('program_setting');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function post()
                    {
                              
                              $data = array(
                                             'prs_name' => $this->input->post('prs_name'),
                                             'prs_capacity' => $this->input->post('prs_capacity'),
                                             'prs_type' => $this->input->post('prs_type'),
                                             'prs_category' => $this->input->post('prs_category'),
                                             'prs_price' => $this->input->post('prs_price'),
                                             'prs_status' => $this->input->post('prs_status'),
                              );
                              $this->db->insert('program_setting', $data);
                    }

                    public function put($id)
                    {
                              $data = array(
                                             'prs_name' => $this->input->post('prs_name'),
                                             'prs_capacity' => $this->input->post('prs_capacity'),
                                             'prs_type' => $this->input->post('prs_type'),
                                             'prs_category' => $this->input->post('prs_category'),
                                             'prs_price' => $this->input->post('prs_price'),
                                             'prs_status' => $this->input->post('prs_status'),
                              );
                              $this->db->where('prs_id', $id)
                                             ->update('program_setting', $data);
                    }
                    public function find($id)
                    {
                              $query = $this->db->where('prs_id', $id)
                                                            ->join('posts','posts.id = program_setting.prs_name', 'left')
                                                            ->limit(1)
                                                            ->get('program_setting');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function delete($id)
                    {
                              $this->db->where('prs_id', $id)
                                             ->delete('program_setting');
                    }

          }
          