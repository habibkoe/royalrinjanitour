<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Rac_model
           *
           * @author Habib Koe Tkj
           */
          class Rac_model extends CI_Model
          {
                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->library('upload');
                    }

                    public function get($limit = NULL)
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->limit($limit)
                                                            ->join('admin','admin.admin_id = rac.author', 'left')
                                                            ->get('rac');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post($aID)
                    {
                              if(empty($_POST['rac_image'])){
                              
                                        $post_image = 'No image';
                              }else{
                                        $post_image = $this->input->post('rac_image');
                              }

                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('rac_name'));
                              $data = array(
                                             'id'=> (time() + 283) - 18439,
                                             'rac_name' => $this->input->post('rac_name'),
                                             'rac_description' => $this->input->post('rac_description'),
                                             'rac_short_description' => $this->input->post('rac_short_description'),
                                             'rac_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'rac_image' => $post_image,
                                             'rac_image_credit' => $this->input->post('rac_image_credit'),
                                             'rac_type' => $this->input->post('rac_type'),
                                             'rac_stock' => $this->input->post('rac_stock'),
                                             'rac_capacity' => $this->input->post('rac_capacity'),
                                             'rac_police_number' => $this->input->post('rac_police_number'),
                                             'rac_price' => $this->input->post('rac_price'),
                                             'rac_status' => $this->input->post('rac_status'),
                                             'author' => $aID
                              );
                              $this->db->insert('rac', $data);
                    }

                    public function put($aID, $id)
                    {
                              if(empty($_POST['rac_image'])){
                              
                                        $post_image = $this->input->post('post_image_old');
                              }else{
                                        $post_image = $this->input->post('rac_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('rac_name'));
                              $data = array(
                                             'rac_name' => $this->input->post('rac_name'),
                                             'rac_description' => $this->input->post('rac_description'),
                                             'rac_short_description' => $this->input->post('rac_short_description'),
                                             'rac_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'rac_image' => $post_image,
                                             'rac_image_credit' => $this->input->post('rac_image_credit'),
                                             'rac_type' => $this->input->post('rac_type'),
                                             'rac_stock' => $this->input->post('rac_stock'),
                                             'rac_capacity' => $this->input->post('rac_capacity'),
                                             'rac_price' => $this->input->post('rac_price'),
                                             'rac_status' => $this->input->post('rac_status'),
                                             'author' => $aID
                              );
                              $this->db->where('id', $id)
                                        ->update('rac', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                        ->delete('rac');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('id', $id)
                                                            ->join('admin','admin.admin_id = rac.author', 'left')
                                                            ->limit(1)
                                                            ->get('rac');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    
                    public function findByUrl($url)
                    {
                              $query = $this->db->where('rac_url', $url)
                                                            ->join('admin','admin.admin_id = rac.author', 'left')
                                                            ->join('program_setting','program_setting.prs_name = rac.id', 'left')
                                                            ->limit(1)
                                                            ->get('rac');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
          }
          