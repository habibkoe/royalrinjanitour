<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Slider_model
           *
           * @author Habib Koe Tkj
           */
          class Slider_model extends CI_Model
          {
                    
                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function get()
                    {
                              $query = $this->db->order_by('id', 'desc')
                                                            ->get('slider');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post($aID)
                    {
                              if(empty($_POST['slider_image'])){
                              
                                        $pr_image = 'No image';
                              }else{
                                        $pr_image = $this->input->post('slider_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('slider_title'));
                              $data = array(
                                             'slider_title' => $this->input->post('slider_title'),
                                             'slider_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'slider_description' => $this->input->post('slider_description'),
                                             'slider_image' => $this->input->post('slider_image'),
                                             'slider_status' => $this->input->post('slider_status'),
                                             'slider_author' => $aID,
                              );
                              $this->db->insert('slider', $data);
                    }

                    public function put($aID, $id)
                    {
                              
                              if(empty($_POST['slider_image'])){
                              
                                        $pr_image = $this->input->post('slider_image_old');
                              }else{
                                        $pr_image = $this->input->post('slider_image');
                              }
                              
                              $car = array('-', '/', '\\', ',', '.', '#', ':', ';', '\'', '"', '[', ']', '{', '}', ')', '(', '|', '`', '~', '!', '@', '%', '$', '^', '&', '*', '=', '?', '+');
                              $url = str_replace($car, "", $this->input->post('slider_title'));
                              $data = array(
                                             'slider_title' => $this->input->post('slider_title'),
                                             'slider_url' => rtrim(strtolower(str_replace(" ", "-", $url))),
                                             'slider_description' => $this->input->post('slider_description'),
                                             'slider_image' => $this->input->post('slider_image'),
                                             'slider_status' => $this->input->post('slider_status'),
                                             'slider_author' => $aID,
                              );
                              $this->db->where('id', $id)
                                             ->update('slider', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('id', $id)
                                             ->delete('slider');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('id', $id)
                                                            ->limit(1)
                                                            ->get('slider');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }
                    
          }
          