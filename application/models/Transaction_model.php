<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of Transaction_model
           *
           * @author Habib Koe Tkj
           */
          class Transaction_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                              $this->load->library('upload');
                    }

                    public function get()
                    {
                              
                    }

                    public function confirm($booking_id)
                    {
                              $image_name = time() . $_FILES["bc_image_paid"]['name'];
                              $config['file_name'] = $image_name;
                              $config['upload_path'] = './source/paid_confirm/';
                              $config['allowed_types'] = 'gif|jpg|png';
                              $config['max_size'] = 2000; //kilobyte
                              $config['max_width'] = 1000; //pixel
                              $config['max_height'] = 1000; //pixel
                              $this->upload->initialize($config);
                              $upload = $this->upload->do_upload('bc_image_paid');
                              $data = array(
                                             'booking_id' => $booking_id,
                                             'bc_bank_account' => $this->input->post('bc_bank_account'),
                                             'bc_total_paid' => $this->input->post('bc_total_paid'),
                                             'bc_bank' => $this->input->post('bc_bank'),
                                             'bc_image_paid' => $image_name,
                              );
                              $confirm = $this->db->insert('booking_confirm', $data);

                              if ($confirm) {
                                        $data = array(
                                                       'or_status' => 'paid',
                                                       'paid_date'=> date('d-m-Y H:i:s')
                                        );
                                        $this->db->where('id', $booking_id)
                                                  ->update('booking', $data);
                              }
                    }
          }
          