<?php

          /*
           * To change this license header, choose License Headers in Project Properties.
           * To change this template file, choose Tools | Templates
           * and open the template in the editor.
           */

          /**
           * Description of User_model
           *
           * @author Habib Koe Tkj
           */
          class User_model extends CI_Model
          {

                    public function __construct()
                    {
                              parent::__construct();
                    }

                    public function get()
                    {
                              $query = $this->db->order_by('admin_name', 'desc')
                                                            ->get('admin');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }
                    
                    public function getVisitor()
                    {
                              $query = $this->db->order_by('timestamp', 'desc')
                                                            ->get('ci_sessions');
                              if ($query->num_rows() > 0) {
                                        return $query->result();
                              }
                              else {
                                        return array();
                              }
                    }

                    public function post()
                    {

                              $data = array(
                                             'admin_name' => $this->input->post('admin_name'),
                                             'admin_email' => $this->input->post('admin_email'),
                                             'admin_phone' => $this->input->post('admin_phone'),
                                             'admin_username' => $this->input->post('admin_username'),
                                             'admin_password' => md5($this->input->post('admin_password')),
                                             'admin_level' => $this->input->post('admin_level'),
                                             'admin_position' => $this->input->post('admin_position'),
                                             'admin_status' => $this->input->post('admin_status'),
                              );
                              $this->db->insert('admin', $data);
                    }

                    public function put($id)
                    {
                              
                              if (empty($_POST['admin_password'])) {
                                        $password = $this->input->post('admin_password_old');
                              }
                              else {
                                        $password = md5($this->input->post('admin_password'));
                              }

                              $data = array(
                                             'admin_name' => $this->input->post('admin_name'),
                                             'admin_email' => $this->input->post('admin_email'),
                                             'admin_phone' => $this->input->post('admin_phone'),
                                             'admin_username' => $this->input->post('admin_username'),
                                             'admin_password' => $password,
                                             'admin_level' => $this->input->post('admin_level'),
                                             'admin_position' => $this->input->post('admin_position'),
                                             'admin_status' => $this->input->post('admin_status'),
                              );
                              $this->db->where('admin_id', $id)
                                        ->update('admin', $data);
                    }

                    public function delete($id)
                    {
                              $this->db->where('admin_id', $id)
                                        ->delete('admin');
                    }

                    public function find($id)
                    {
                              $query = $this->db->where('admin_id', $id)
                                        ->limit(1)
                                        ->get('admin');
                              if ($query->num_rows() > 0) {
                                        return $query->row();
                              }
                              else {
                                        return array();
                              }
                    }

          }
          