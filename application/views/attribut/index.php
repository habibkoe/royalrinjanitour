<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/attribut/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New Atribut</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-bookmark"></i>  <?= $page_name ?></span>
                              </h2>

                              <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover">
                                                  <thead>
                                                            <tr>
                                                                      <th>No</th>
                                                                      <th>Name</th>
                                                                      <th>Category for</th>
                                                                      <th>Action</th>
                                                            </tr>
                                                  </thead>
                                                  <tbody>
                                                            <?php $no = 1; foreach ($all_attribut as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no ?></td>
                                                                                          <td><?= $value->at_name ?></td>
                                                                                          <td><?= $value->at_for ?></td>
                                                                                          <td>
                                                                                                    <div class="btn-group" role="group" aria-label="...">
                                                                                                              <a href="<?= base_url('backend/attribut/put/' . $value->at_id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                              <a href="<?= base_url('backend/attribut/delete/' . $value->at_id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                                    </div>
                                                                                          </td>
                                                                                </tr>
                                                                      <?php $no++; endforeach; ?>
                                                  </tbody>
                                        </table>
                              </div>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>