<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/attribut') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Attributs</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-bookmark"></i> <?= $page_name ?></span>
                              </h2> 
                              <div class="row">
                                        <?= form_open_multipart('backend/attribut/post')?>
                                        <div class="col-md-8">
                                                  <label>Judul attribut</label>
                                                  <input type="text" name="at_name" placeholder="judul" class="form-control">
                                                  <hr>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>attribute ini untuk siapa.?</label>
                                                                      <select name="at_for" class="form-control">
                                                                                <option value="post">Post</option>
                                                                                <option value="program-activities">Program Activities</option>
                                                                      </select>
                                                            </div>
                                                  </div>
                                                  <hr>
                                                  <label>Deskripsi attribut</label>
                                                  <textarea name="at_description" placeholder="isi" class="form-control" rows="5"></textarea>
                                                  
                                        </div>
                                        <div class="col-md-4">
                                                  <label>Publish</label>
                                                  <br>
                                                  <input type="submit" name="ar_status" value="publish"  class="btn btn-primary">
                                                  
                                        </div>
                                        <?= form_close()?>
                              </div>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
