<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/attribut') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Attributs</a> 
                                        <a href="<?= base_url('backend/attribut/post') ?>" class="btn btn-primary">New Attribute</a> 
                                        <span class="pull-right"><i class="glyphicon glyphicon-bookmark"></i> <?= $page_name ?></span>
                              </h2> 
                              <div class="row">
                                        <?= form_open_multipart('backend/attribut/put/'. $attribut_one->at_id) ?>
                                        <div class="col-md-8">
                                                  <label>Judul attribut</label>
                                                  <input type="text" name="at_name" placeholder="judul" class="form-control" value="<?= $attribut_one->at_name ?>">
                                                  <hr>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>attribute ini untuk siapa.?</label>
                                                                      <select name="at_for" class="form-control">
                                                                                <option value="post" <?php if($attribut_one->at_for=='post'){ echo 'selected'; } ?>>Post</option>
                                                                                <option value="program-activities" <?php if($attribut_one->at_for=='program-activities'){ echo 'selected'; } ?>>Program Activities</option>
                                                                                
                                                                      </select>
                                                            </div>
                                                  </div>
                                                  <hr>
                                                  <label>Deskripsi attribut</label>
                                                  <textarea name="at_description" placeholder="isi" class="form-control" rows="5"><?= $attribut_one->at_description ?></textarea>

                                        </div>
                                        <div class="col-md-4">
                                                  <label>Publish</label>
                                                  <br>
                                                  <input type="submit" name="ar_status" value="publish"  class="btn btn-primary">

                                        </div>
                                        <?= form_close() ?>
                              </div>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
