<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/bnb/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New BnB</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-text-background"></i> <?= $page_name ?></span>
                              </h2>

                              <table class="table table-striped table-bordered table-hover" id="example1">
                                        <thead>
                                                  <tr>
                                                            <th>No</th>
                                                            <th>Image</th>
                                                            <th>Name</th>
                                                            <th>Status</th>
                                                            <th>Author</th>
                                                            <th>Action</th>
                                                  </tr>
                                        </thead>
                                        <tbody>
                                                  <?php $no=1; foreach ($all_bnb as $value): ?>
                                                                      <tr>
                                                                                <td><?= $no ?></td>
                                                                                <td>
                                                                                          <?php if ($value->bnb_image != 'No image'): ?>
                                                                                                    <img src="<?= $value->bnb_image ?>" width="100">
                                                                                          <?php else: ?>
                                                                                                    No Image
                                                                                          <?php endif; ?>
                                                                                </td>
                                                                                <td><?= $value->bnb_name ?></td>
                                                                                <td>
                                                                                          <?php if($value->bnb_status=='publish'): ?>
                                                                                          <a class="btn btn-xs btn-success"><?= $value->bnb_status ?></a>
                                                                                          <?php else: ?>
                                                                                          <a class="btn btn-xs btn-danger"><?= $value->bnb_status ?></a>
                                                                                          <?php endif;?>
                                                                                </td>
                                                                                <td><?= $value->admin_name ?></td>
                                                                                <td>
                                                                                          <div class="btn-group" role="group" aria-label="...">
                                                                                                    <a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" class="btn btn-success btn-xs" title="View posting" target="_blank"><span class="glyphicon glyphicon-zoom-in"></span></a>
                                                                                                    <a href="<?= base_url('backend/bnb/put/' . $value->id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                    <a href="<?= base_url('backend/bnb/delete/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                          </div>
                                                                                </td>
                                                                      </tr>
                                                            <?php $no++; endforeach; ?>
                                        </tbody>
                              </table>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/plugin/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script>
              $(function () {
                       $("#example1").DataTable();
              });
</script>
</body>
</html>