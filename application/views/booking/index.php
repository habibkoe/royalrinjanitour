<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/booking/post') ?>" class="btn btn-ungu">
                                                  <i class="glyphicon glyphicon-plus"></i> New Order
                                        </a><span class="pull-right"><?= $page_base ?></span>
                              </h2>
                                        <table class="table table-striped table-bordered table-hover" id="example1">
                                                  <thead>
                                                            <tr>
                                                                      <th>No</th>
                                                                      <th>ID Book</th>
                                                                      <th>Type</th>
                                                                      <th>Name</th>
                                                                      <th>Date</th>
                                                                      <th>Due Date</th>
                                                                      <th>Status</th>
                                                                      <th>Action</th>
                                                            </tr>
                                                  </thead>
                                                  <tbody>
                                                            <?php $no =1; foreach ($recent_booking as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no; ?></td>
                                                                                          
                                                                                          <td>
                                                                                                    <?php if($value->or_type=='programs'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view/'.$value->id) ?>">
                                                                                                    <?php elseif($value->or_type=='bnb'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view_bnb/'.$value->id) ?>">
                                                                                                    <?php elseif($value->or_type=='rac'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view_rac/'.$value->id) ?>">
                                                                                                    <?php endif; ?>
                                                                                                    <?= $value->id ?>
                                                                                                    </a>
                                                                                          </td>
                                                                                          <td>
                                                                                                    <?= $value->or_type ?><br>
                                                                                          </td>
                                                                                          <td>
                                                                                                    <?= $value->or_full_name ?><br>
                                                                                                    <?= $value->or_email ?>
                                                                                          </td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->created_at))  ?></td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->or_due_date))  ?></td>
                                                                                          <td>
                                                                                                    <?php if($value->or_status=='paid'): ?>
                                                                                                    <a class="btn btn-xs btn-success"><?= $value->or_status ?></a>
                                                                                                    <?php elseif($value->or_status=='disable'): ?>
                                                                                                    <a class="btn btn-xs btn-warning"><?= $value->or_status ?></a>
                                                                                                    <?php else: ?>
                                                                                                    <a class="btn btn-xs btn-danger"><?= $value->or_status ?></a>
                                                                                                    <?php endif;?>
                                                                                          </td>
                                                                                          <td>
                                                                                                    <div class="btn-group" role="group" aria-label="...">
                                                                                                              <a href="<?= base_url('backend/booking/disable/' . $value->id) ?>" class="btn btn-warning btn-xs" title="make it unavailable"><span class="glyphicon glyphicon-erase"></span></a>
                                                                                                              <a href="<?= base_url('backend/booking/put/' . $value->id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                              <a href="<?= base_url('backend/booking/delete/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                                    </div>
                                                                                          </td>
                                                                                </tr>
                                                                      <?php $no++; endforeach; ?>

                                                  </tbody>
                                        </table>
                    </div>
          </div>
</div>
<div class="modal fade" id="myModalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
                    <div class="modal-content">
                              <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Paid Confirm</h4>
                              </div>
                              <?= form_open_multipart('backend/booking/confirm/' . $booking_one->id) ?>
                              <div class="modal-body">
                                        <div class="form-group has-feedback">
                                                  <label>Your Bank Account</label>
                                                  <input type="text" name="bc_bank_account" class="form-control" id="invoice" placeholder="Account Name" required="">
                                                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                                        </div>
                                        <h6>example: Jack Miller</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Bank</label>
                                                  <input type="text" name="bc_bank" class="form-control" id="nope" placeholder="Bank" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>example: Western Union, Capital One Financial</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Total Paid</label>
                                                  <input type="text" name="bc_total_paid" class="form-control" id="nope" placeholder="You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>, just enter the bill without "US$"</h6>
                                        <label>Image Transfer</label>
                                        <div id="image-preview">
                                                  <label for="image-upload" id="image-label">image</label>
                                                  <input type="file" name="bc_image_paid" id="image-upload" required="" />
                                        </div>
                                        <h6>image extention: jpg, png | size: 100kb</h6>
                              </div>
                              <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger pull-left">Paid</button>
                              </div>
                              <?= form_close() ?>
                    </div>
          </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/plugin/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script>
          $(function () {
                    $("#example1").DataTable();
          });
</script>
</body>
</html>