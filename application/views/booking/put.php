<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-7 col-md-offset-2 main">
                              <h2 class="page-header">News</h2>
                              <?= form_open_multipart('news'); ?>
                              <input type="text" name="news_title" class="form-control">
                              <br>
                              <textarea name="news_description"></textarea>

                              <?= form_close(); ?>

                    </div>
                    <div class="col-sm-9  col-md-3 sidebar-right border">
                             <h1 class="page-header">Post</h1>
                             <div class="input-append">
	    <input id="fieldID" type="text" value="" >
                                        <a href="<?= base_url(); ?>filemanager/dialog.php?type=1&field_id=fieldID&relative_url=1" class="btn iframe-btn" type="button">Select</a>
	</div>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/tinymce/tinymce.min.js"></script>
<?php $this->load->view('layout/editor'); ?>        

</body>
</html>
