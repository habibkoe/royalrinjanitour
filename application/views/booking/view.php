<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/booking') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Booked</a>
                                        <a href="" class="btn btn-ungu">
                                                  <i class="glyphicon glyphicon-plus"></i> New Order
                                        </a><span class="pull-right">#<?= $booking_one->id ?></span>
                              </h2>
                              <div style="width: 100%;float: left;">
                                        
                                        <p>
                                                  From: <strong><?= $booking_one->or_full_name ?></strong><br><br>
                                                  <?php if($booking_one->or_status=='unpaid'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;text-transform: capitalize; color: #F00;"><?= $booking_one->or_status; ?></h3>
                                                  <?php elseif($booking_one->or_status=='paid'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;text-transform: capitalize; color: #008000;"><?= $booking_one->or_status; ?></h3>
                                                  <?php elseif($booking_one->or_status=='disable'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;text-transform: capitalize; color: #ff0000;"><?= $booking_one->or_status; ?></h3>
                                                  <?php endif; ?>
                                                            
                                                  <?php if($booking_one->or_status=='unpaid'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;">Total  Amount  To Be Paid  <span style="color: #F00">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></h3>
                                                  <?php elseif($booking_one->or_status=='paid'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;">Thank You For Paid  <span style="color: #008000">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></h3>
                                                  <?php elseif($booking_one->or_status=='disable'): ?>
                                                            <h3 style="text-align: left;font-weight: bold;">This Transaction Disabled  <span style="color: #ff0000">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></h3>
                                                  <?php endif; ?>

                                        </p>
                                        <div class="row">
                                                  <div class="col-md-6">

                                                            <h3 style="color: #CB0011;">DETAILS:</h3>
                                                            <table style="width: 100%">
                                                                      <tr><td colspan="3" style="background: #008000;color: #fff;padding: 10px;text-align: center;">Booking(s) on <strong><?= date('d-m-Y H:i:s', strtotime($booking_one->created_at)) ?></strong></td></tr>

                                                                      <tr>
                                                                                <td>Name</td><td>:</td><td><?= $booking_one->or_full_name ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Mobile Phone Number</td><td>:</td><td><?= $booking_one->or_phone ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Land Line Phone Number</td><td>:</td><td><?= $booking_one->or_home_phone ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Email</td><td>:</td><td><?= $booking_one->or_email ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Address</td><td>:</td><td><?= $booking_one->or_address ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Departure date</td><td>:</td><td><?= $booking_one->or_departure_date ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Arrival date</td><td>:</td><td><?= $booking_one->or_arrival_date ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Pick-up from</td><td>:</td><td><?= $booking_one->or_pick_up_from ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Drop off to</td><td>:</td><td><?= $booking_one->or_drop_off_to ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                               <td>Guest(s) name(s)</td><td>:</td><td><?= $booking_one->booking_guest_name ?></td>

                                                                     </tr>
                                                                      <tr>
                                                                                <td>Relevant medical conditions</td><td>:</td><td><?= $booking_one->or_medical_condition ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Allergies, Special Diets Or Vegetarian</td><td>:</td><td><?= $booking_one->or_special_diets ?></td>

                                                                      </tr>

                                                                      <tr>
                                                                                <td>Special request</td><td>:</td><td><?= $booking_one->or_message ?></td>

                                                                      </tr>

                                                            </table>
                                                            <hr>
                                                            <h4>Detail Programs</h4>
                                                            <?php if ($booking_list_one > 0): ?>
                                                                                <table style="width: 100%;">
                                                                                          <thead>
                                                                                                    <tr style="background: #008000;color: #fff;" style="padding: 10px;">
                                                                                                              <th colspan="2" style="padding: 10px;">Program (s)</th>
                                                                                                              <th style="padding: 10px;"><span style="float: right;">US$</span></th>
                                                                                                    </tr>
                                                                                          </thead>
                                                                                          <tbody>
                                                                                                    <?php
                                                                                                    foreach ($booking_list_one as $sp) :
                                                                                                              if ($sp->booking_id == $booking_one->id) {
                                                                                                                        ?>
                                                                                                                        <tr>
                                                                                                                                  <td  style="padding-top: 5px;"><img src="<?= $sp->pr_image ?>" width="100"></td>
                                                                                                                                  <td>
                                                                                                                                            <?= $sp->pr_title ?>
                                                                                                                                  </td>  
                                                                                                                                  <td>
                                                                                                                                            <?= number_format($sp->price, 2, ".", ","); ?>
                                                                                                                                  </td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                                  <td colspan="3">
                                                                                                                                            <p class="price-includes">
                                                                                                                                                      <strong>Price includes:</strong><br>
                                                                                                                                                      <?= $sp->pr_short_description ?>
                                                                                                                                            </p>
                                                                                                                                  </td>
                                                                                                                        </tr>
                                                                                                    <?php } endforeach; ?>
                                                                                          </tbody>
                                                                                          <tfoot>
                                                                                                    <tr>
                                                                                                              <th colspan="2" style="color: red;padding: 10px;text-align: left;">TOTAL  AMOUNT  TO BE PAID </th>

                                                                                                              <th style="background: red;padding: 10px;color: #fff;"><span class="pull-right total-price">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></th>
                                                                                                    </tr>
                                                                                          </tfoot>
                                                                                </table>
                                                                      <?php endif; ?>
                                                            <?php if($booking_one->or_status=='unpaid'): ?>
                                                            <a href="#" class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myModalConfirm">Confirm</a>
                                                            <?php endif; ?>
                                                  </div>
                                                  <div class="col-md-6">
                                                            <h3 style="color: #CB0011;">DATA CONFIRMATION</h3>
                                                            <?php if($booking_confirm != NULL): ?>
                                                            <table style="width: 100%">
                                                                      <tr><td colspan="3" style="background: #008000;color: #fff;padding: 10px;text-align: center;">Confirm(s) on <strong><?= date('d-m-Y H:i:s', strtotime($booking_confirm->bc_date)) ?></strong></td></tr>

                                                                      <tr>
                                                                                <td>Name</td><td>:</td><td><?= $booking_confirm->bc_bank_account ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Bank</td><td>:</td><td><?= $booking_confirm->bc_bank ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Total Paid Transfer </td><td>:</td><td><?= $booking_confirm->bc_total_paid ?></td>

                                                                      </tr>
                                                                      <tr>
                                                                                <td>Image Transaction</td><td>:</td><td><img src="<?= base_url('source/paid_confirm/'.$booking_confirm->bc_image_paid) ?>" width="50%"></td>

                                                                      </tr>
                                                            </table>
                                                            <?php else: ?>
                                                                      <h5>DATA CONFIRMATION EMPTY</h5>
                                                            <?php endif; ?>
                                                  </div>
                                        </div>
                              </div>

                    </div>
          </div>
</div>
<div class="modal fade" id="myModalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
                    <div class="modal-content">
                              <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Paid Confirm</h4>
                              </div>
                              <?= form_open_multipart('backend/booking/confirm/' . $booking_one->id) ?>
                              <div class="modal-body">
                                        <div class="form-group has-feedback">
                                                  <label>Your Bank Account</label>
                                                  <input type="text" name="bc_bank_account" class="form-control" id="invoice" placeholder="Account Name" required="">
                                                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                                        </div>
                                        <h6>example: Jack Miller</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Bank</label>
                                                  <input type="text" name="bc_bank" class="form-control" id="nope" placeholder="Bank" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>example: Western Union, Capital One Financial</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Total Paid</label>
                                                  <input type="text" name="bc_total_paid" class="form-control" id="nope" placeholder="You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>, just enter the bill without "US$"</h6>
                                        <label>Image Transfer</label>
                                        <div id="image-preview">
                                                  <label for="image-upload" id="image-label">image</label>
                                                  <input type="file" name="bc_image_paid" id="image-upload" required="" />
                                        </div>
                                        <h6>image extention: jpg, png | size: 100kb</h6>
                              </div>
                              <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger pull-left">Paid</button>
                              </div>
                              <?= form_close() ?>
                    </div>
          </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.uploadPreview.js'); ?>"></script>
<script type="text/javascript">
          $(document).ready(function () {
                    $.uploadPreview({
                              input_field: "#image-upload",
                              preview_box: "#image-preview",
                              label_field: "#image-label"
                    });
          });

</script>
</body>
</html>