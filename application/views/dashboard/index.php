<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header"><i class="glyphicon glyphicon-dashboard"></i>  Dashboard</h2>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <div class="row">
                                                            <div class="col-md-3 col-sm-6">
                                                                      <div class="col-md-12 box-notive">
                                                                                <div class="row">
                                                                                          <div class="col-md-6 col-sm-6 bg-4  center">
                                                                                                    <i class="fa fa-diamond fa-5x" aria-hidden="true"></i>
                                                                                          </div>
                                                                                          <div class="col-md-6 col-sm-6">
                                                                                                    <span style="font-size: 11px;">TOTAL TRANSACTION</span>
                                                                                                    <br>
                                                                                                    <span style="font-size: 22px; font-weight: bold; ">
                                                                                                    <?php
                                                                                                              foreach ($total_transaction as $value) {
                                                                                                                      echo $value->total;
                                                                                                              }
                                                                                                    ?> 
                                                                                                    </span>
                                                                                          </div>
                                                                                </div>
                                                                                
                                                                      </div>
                                                                                
                                                            </div>
                                                            
                                                            <div class="col-md-3 col-sm-6">
                                                                      <div class="col-md-12 box-notive">
                                                                                <div class="row">
                                                                                          <div class="col-md-6 col-sm-6 bg-3 center"">
                                                                                                    <i class="fa fa-credit-card-alt fa-5x" aria-hidden="true"></i>
                                                                                          </div>
                                                                                          <div class="col-md-6 col-sm-6">
                                                                                                    <span style="font-size: 11px;">TRANSACTION PAID</span>
                                                                                                    <br>
                                                                                                    <span style="font-size: 22px; font-weight: bold; ">
                                                                                                    <?php
                                                                                                              foreach ($total_transaction_paid as $value) {
                                                                                                                      echo $value->total;
                                                                                                              }
                                                                                                    ?> 
                                                                                                    </span>
                                                                                          </div>
                                                                                </div>
                                                                                
                                                                      </div>
                                                                                
                                                            </div>
                                                            <div class="col-md-3 col-sm-6">
                                                                      <div class="col-md-12 box-notive">
                                                                                <div class="row">
                                                                                          <div class="col-md-6 col-sm-6 bg-1 center">
                                                                                                    <i class="fa fa-credit-card fa-5x" aria-hidden="true"></i>
                                                                                          </div>
                                                                                          <div class="col-md-6 col-sm-6">
                                                                                                    <span style="font-size: 11px;">TRANSACTION UNPAID</span>
                                                                                                    <br>
                                                                                                    <span style="font-size: 22px; font-weight: bold; ">
                                                                                                    <?php
                                                                                                              foreach ($total_transaction_unpaid as $value) {
                                                                                                                      echo $value->total;
                                                                                                              }
                                                                                                    ?> 
                                                                                                    </span>
                                                                                          </div>
                                                                                </div>
                                                                                
                                                                      </div>
                                                                                
                                                            </div>
                                                            
                                                            <div class="col-md-3 col-sm-6">
                                                                      <div class="col-md-12 box-notive">
                                                                                <div class="row">
                                                                                          <div class="col-md-6 col-sm-6 bg-7 center">
                                                                                                    <i class="fa fa-credit-card fa-5x" aria-hidden="true"></i>
                                                                                          </div>
                                                                                          <div class="col-md-6 col-sm-6">
                                                                                                    <span style="font-size: 11px;">TRANSACTION DISABLE</span>
                                                                                                    <br>
                                                                                                    <span style="font-size: 22px; font-weight: bold; ">
                                                                                                    <?php
                                                                                                              foreach ($total_transaction_disable as $value) {
                                                                                                                      echo $value->total;
                                                                                                              }
                                                                                                    ?> 
                                                                                                    </span>
                                                                                          </div>
                                                                                </div>
                                                                                
                                                                      </div>
                                                                                
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-6">
                                                  
                                                  <h4 class="sub-header"><i  class="glyphicon glyphicon-book"></i> New booking</h4>
                                                  <div class="table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                      <thead>
                                                                                <tr>
                                                                                          <th>ID Book</th>
                                                                                          <th>Name</th>
                                                                                          <th>Status</th>
                                                                                          <th>Date</th>
                                                                                </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                                <?php foreach ($recent_booking as $value): ?>
                                                                                <tr>
                                                                                          <td>
                                                                                                    <?php if($value->or_type=='programs'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view/'.$value->id) ?>">
                                                                                                    <?php elseif($value->or_type=='bnb'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view_bnb/'.$value->id) ?>">
                                                                                                    <?php elseif($value->or_type=='rac'): ?>
                                                                                                              <a href="<?= base_url('backend/booking/view_rac/'.$value->id) ?>">
                                                                                                    <?php endif; ?>
                                                                                                              <?= $value->id ?>
                                                                                                    </a>
                                                                                          </td>
                                                                                          <td>
                                                                                                    <?= $value->or_full_name ?><br>
                                                                                                    <?= $value->or_email ?>
                                                                                          </td>
                                                                                          <td>
                                                                                                    <?php if($value->or_status=='paid'): ?>
                                                                                                    <a class="btn btn-xs btn-success"><?= $value->or_status ?></a>
                                                                                                    <?php elseif($value->or_status=='disable'): ?>
                                                                                                    <a class="btn btn-xs btn-warning"><?= $value->or_status ?></a>
                                                                                                    <?php else: ?>
                                                                                                    <a class="btn btn-xs btn-danger"><?= $value->or_status ?></a>
                                                                                                    <?php endif;?>
                                                                                          </td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->created_at))  ?></td>
                                                                                </tr>
                                                                                <?php endforeach; ?>
                                                                                
                                                                      </tbody>
                                                            </table>
                                                  </div>
                                                  
                                                  <h4 class="sub-header"><i class="glyphicon glyphicon-list"></i> New programs</h4>
                                                  <div class="table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                      <thead>
                                                                                <tr>
                                                                                          <th>No</th>
                                                                                          <th>Title</th>
                                                                                          <th>Editor</th>
                                                                                          <th>Date</th>
                                                                                </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                                <?php $no=1; foreach($recent_program as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no ?></td>
                                                                                          <td><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" target="_blank"><?= $value->pr_title ?></a></td>
                                                                                          <td><?= $value->admin_name ?></td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->created_at))  ?></td>
                                                                                </tr>
                                                                                <?php $no++; endforeach; ?>
                                                                      </tbody>
                                                            </table>
                                                  </div>
                                        </div>
                                        <div class="col-md-6">
                                                  
                                                  <h4 class="sub-header"><i class="glyphicon glyphicon-text-background"></i>  New post</h4>
                                                  <div class="table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                      <thead>
                                                                                <tr>
                                                                                          <th>No</th>
                                                                                          <th>Title</th>
                                                                                          <th>Category</th>
                                                                                          <th>Author</th>
                                                                                          <th>Created at</th>
                                                                                </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                                <?php $no=1; foreach($recent_post as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no ?></td>
                                                                                          <td><a href="<?= base_url('posts/view/' . $value->posts_url) ?>" target="_blank"><?= $value->posts_title ?></a></td>
                                                                                          <td><?= $value->at_name ?></td>
                                                                                          <td><?= $value->admin_name ?></td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->created_at))  ?></td>
                                                                                </tr>
                                                                                <?php $no++; endforeach; ?>
                                                                      </tbody>
                                                            </table>
                                                  </div>
                                                  
                                                  <h4 class="sub-header"><i class="glyphicon glyphicon-cutlery"></i>  New bnb</h4>
                                                  <div class="table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                      <thead>
                                                                                <tr>
                                                                                          <th>No</th>
                                                                                          <th>Image</th>
                                                                                          <th>Name</th>
                                                                                          <th>Author</th>
                                                                                          <th>Created at</th>
                                                                                </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                                <?php $no=1; foreach($recent_bnb as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no ?></td>
                                                                                          <td><img src="<?= $value->bnb_image ?>" width="50" height="50" style="border-radius: 50%;"></td>
                                                                                          <td><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" target="_blank"><?= $value->bnb_name ?></a></td>
                                                                                          
                                                                                          <td><?= $value->admin_name ?></td>
                                                                                          <td><?= date('d-m-Y H:i:s', strtotime($value->created_at))  ?></td>
                                                                                </tr>
                                                                                <?php $no++; endforeach; ?>
                                                                      </tbody>
                                                            </table>
                                                  </div>
                                        </div>
                              </div>
                    </div>
          </div>
          </div>

          <!-- Bootstrap core JavaScript
          ================================================== -->
          <!-- Placed at the end of the document so the pages load faster -->
          <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
          <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
