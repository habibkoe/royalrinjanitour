<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header"><i class="glyphicon glyphicon-cog"></i> Base Setting</h2>
                              <div class="row">
                                         <?= form_open('backend/dashboard/setting/'.$web_setting->id) ?>
                                        <div class="col-md-6">
                                                  <h4>General Setting</h4>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Title</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <input type="text" name="web_title" class="form-control" value="<?= $web_setting->web_title ?>">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Logo</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <input type="text" name="web_logo" class="form-control">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Owner</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <input type="text" name="web_owner" class="form-control" value="<?= $web_setting->web_owner ?>">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Author</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <input type="text" name="web_author" class="form-control" value="<?= $web_setting->web_author ?>">
                                                            </div>
                                                  </div>
                                                  <hr>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Meta Keyword</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <textarea name="web_meta" class="form-control" rows="4"><?= $web_setting->web_meta ?></textarea>
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Description</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <textarea name="web_description" class="form-control" rows="4"><?= $web_setting->web_description ?></textarea>
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Office Address</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <textarea name="web_office_address" class="form-control" rows="4"><?= $web_setting->web_office_address ?></textarea>
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Maps</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <textarea name="web_maps" class="form-control" rows="4"><?= $web_setting->web_maps ?></textarea>
                                                            </div>
                                                  </div>
                                                  <hr>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Call Center</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      
                                                                      <input type="text" name="web_call_center" class="form-control" value="<?= $web_setting->web_call_center ?>">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Facebook</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <input type="text" name="web_link_1" class="form-control" value="<?= $web_setting->web_link_1 ?>">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Youtube</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <input type="text" name="web_link_2" class="form-control" value="<?= $web_setting->web_link_2 ?>">
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Weebly</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <input type="text" name="web_link_3" class="form-control" value="<?= $web_setting->web_link_3 ?>">
                                                            </div>
                                                  </div>
                                                 <br>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <input type="submit" name="web_status" class="btn btn-primary btn-success" value="Save">
                                                            </div>
                                                  </div>
                                                  
                                        </div>
                                        <div class="col-md-6">
                                                  <h4>Notification Setting</h4>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website B&B Notification</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <textarea name="web_bnb_notif" class="form-control" rows="4"><?= $web_setting->web_bnb_notif ?></textarea>
                                                            </div>
                                                  </div>
                                                 <br>
                                                 <div class="row">
                                                            <div class="col-md-4">
                                                                      <label>Website Rent a Car Notification</label>
                                                            </div>
                                                            <div class="col-md-8">
                                                                      <textarea name="web_rac_notif" class="form-control" rows="4"><?= $web_setting->web_rac_notif ?></textarea>
                                                            </div>
                                                  </div>
                                                 <br>
                                        </div>
                                        <?= form_close() ?>
                              </div>
                    </div>
          </div>

          <!-- Bootstrap core JavaScript
          ================================================== -->
          <!-- Placed at the end of the document so the pages load faster -->
          <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
          <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>
