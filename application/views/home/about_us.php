<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <?= $posts_one->posts_title ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-detail"><?= $posts_one->posts_title ?></h3>
                              <h6>
                                        <i class="glyphicon glyphicon-calendar"></i> 
                                        Post date: <?= date('l, j F Y', strtotime($posts_one->created_at)) ?> - 
                                        <i class="glyphicon glyphicon-user"></i>
                                        Editor: <?= $posts_one->admin_name ?>
                              </h6>
                              <?php if ($posts_one->posts_image != 'No image'): ?>
                                                  <img class="img-responsive img-rounded-2px" src="<?= $posts_one->posts_image ?>">
                                        <?php endif; ?>
                              <h6>Picture: <?= $posts_one->posts_image_credit ?></h6>
                              <?= $posts_one->posts_description ?>
                              
                    </div>
                    <div class="col-md-4  box-side">
                              <div class="fb-page" data-href="https://www.facebook.com/rinjanitourtrekking" data-tabs="timeline" data-width="360" data-height="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/rinjanitourtrekking" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/rinjanitourtrekking">Royal Rinjani Tour</a></blockquote></div>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-home"></i> Other Bed and Breakfast</h3>
                              <?php foreach ($bnb_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->bnb_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=178073548932787";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>