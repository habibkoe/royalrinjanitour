<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home ></a> <a href="<?= base_url('bed-and-breakfast') ?>"><?= $page_name ?> ></a> <?= $bnb_one->bnb_name ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-detail"><?= $bnb_one->bnb_name ?></h3>
                              <h6>
                                        <i class="glyphicon glyphicon-calendar"></i> 
                                        Post date: <?= date('l, j F Y', strtotime($bnb_one->created_at)) ?> - 
                                        <i class="glyphicon glyphicon-user"></i>
                                        <?= $bnb_one->admin_position ?> : <?= $bnb_one->admin_name ?>
                              </h6>
                              <div class="row">
                                        <div class="col-md-8">
                                                  <?php if ($bnb_one->bnb_image != 'No image'): ?>
                                                                      <img class="img-responsive img-rounded-2px" src="<?= $bnb_one->bnb_image ?>">
                                                            <?php endif; ?>
                                                  <h6>Picture: <?= $bnb_one->bnb_image_credit ?></h6>
                                                  <div style="width: 100%; float: left;">
                                                            <div style="float: left;margin-right: 5px;width: 80px;">
                                                                      <div class="fb-share-button" data-href="<?= base_url('bnb/view/' . $bnb_one->bnb_url) ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('bnb/view/' . $bnb_one->bnb_url) ?>&amp;src=sdkpreparse">Share</a></div>

                                                            </div>
                                                            <div style="float: left;margin-right: 5px;width: 80px;">
                                                                      <a href="<?= base_url('bnb/view/' . $bnb_one->bnb_url) ?>" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

                                                             </div>
                                                            <div style="float: left !important;margin-right: 5px;width: 100px;">
                                                                      <!-- Place this tag where you want the share button to render. -->
                                                                      <div class="g-plus" data-action="share"></div>
                                                            </div>
                                                  </div>
                                        </div>
                                        <div class="col-md-4">
                                                  <h6 class="h6-style-1">
                                                            <b>To arrange your B&B, please click on the button. Thank you.</b>
                                                  </h6>
                                                  Price: US$ <?= $bnb_one->bnb_price ?> @<?= $bnb_one->bnb_type ?><br>
                                                  Capacity: <?= $bnb_one->bnb_capacity ?> people<br>
                                                  Status: 
                                                  <?php if($bnb_one->bnb_stock=='available'): ?>
                                                  <a class="btn btn-success btn-xs"><?= $bnb_one->bnb_stock ?></a>
                                                  <?php else: ?>
                                                  <a class="btn btn-default btn-xs"><?= $bnb_one->bnb_stock ?></a>
                                                  <?php endif; ?>
                                                  <hr>
                                                  <?php if ($this->session->flashdata('Errorbooking')): ?>
                                                            <div class="row">
                                                                      <div class="col-md-12">
                                                                                <div class="alert alert-block alert-danger fade in">
                                                                                          <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                                                          <?= $this->session->flashdata('Errorbooking') ?>
                                                                                </div>
                                                                      </div>
                                                            </div>
                                                            <?php endif ?>
                                                  <div class="total_person"><span class="pesan" name="qty">1</span> <?= $bnb_one->bnb_type ?></div>
                                                  <span class="atention"><?= $bnb_one->bnb_short_description ?></span>
                                                  <hr class="hr-booking">
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <?php if ($bnb_one->bnb_stock=='available' && $this->cart->total_items() <= 0): ?>
                                                                                <a href="<?= base_url('booking_bnb_now/' . $bnb_one->id) ?>"class="btn btn-primary btn-block">
                                                                                          <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Booking
                                                                                </a>
                                                                      <?php elseif($bnb_one->bnb_stock=='available' && $this->cart->total_items() > 0): ?>
                                                                                <?php foreach ($this->cart->contents() as $items): ?>
                                                                                          <?php if($items['typer'] == 'bnb' && $bnb_one->id== $items['id']): ?>
                                                                                          <a href="<?= base_url('detail_booking') ?>" class="btn btn-default btn-block">
                                                                                                    <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> You've Choose This 
                                                                                          </a>
                                                                                          <?php else: ?>
                                                                                          <a href="<?= base_url('booking_bnb_now/' . $bnb_one->id) ?>"class="btn btn-primary btn-block">
                                                                                                    <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Booking
                                                                                          </a>
                                                                                          <?php endif; ?>
                                                                                <?php endforeach; ?>
                                                                      <?php elseif($bnb_one->bnb_stock=='full'): ?>
                                                                      <a class="btn btn-default btn-block">
                                                                                <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Full
                                                                      </a>
                                                                      <?php endif; ?>
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                              <hr>
                              <?= $bnb_one->bnb_description ?>
                              <div class="fb-comments" data-href="<?= base_url('bnb/view/' . $bnb_one->bnb_url) ?>" data-width="100%" data-numposts="5"></div>
                    </div>
                    
                     
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-usd"></i> Currency Converter</h3>
                              <div class="col-md-12 box-side-inside">
                              <!-- Currency Converter Script - EXCHANGERATEWIDGET.COM -->
                                        <div style="width:100%;">
                                                  <script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=USD&t=IDR&a=1&d=FFFFFF&n=FFFFFF&o=000000&v=1"></script>
                                        </div>
                             <!-- End of Currency Converter Script -->
                              </div>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-home"></i> Other Bed and Breakfast</h3>
                              <?php foreach ($bnb_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" data-src="<?= $value->bnb_image ?>" src="<?= base_url('assets/img/facebook2.gif') ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-plane"></i> Search ticket</h3>
                              <div class="tiket-root" data-widget="tiket-boxsearchwidget" data-businessid="21071732" data-lang="en" data-size_type="normal" data-width="350" data-height="600" data-position="sidebar-right" data-product_type="flight|hotel|train"></div><script type="text/javascript" src="http://www.tiket.com/js/new_widget/tiket_widgetframe_v3.js" async="true"></script>
                    </div>
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script src="<?= base_url('assets/js/jQuery.loadScroll.js'); ?>"></script>
<script>
$(function() {  

    // Custom fadeIn Duration
    $('.img-read-side img').loadScroll(1000);

});
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    },5000);

          });
//-->
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                              return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
                    fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>