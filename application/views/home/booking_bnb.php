<div class="container box-home">
          <div class="row">
                    <?php if ($this->session->flashdata('Errorbooking')): ?>
                                        <div class="col-md-12">
                                                  <div class="alert alert-block alert-danger fade in">
                                                            <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                            <?= $this->session->flashdata('Errorbooking') ?>
                                                  </div>
                                        </div>
                              <?php endif ?>
                    <div class="col-md-12 breadcrumb-booking">
                              <div class="row">
                                        <div class="col-md-3">
                                                  .
                                        </div>
                                        <div class="col-md-3 active">
                                                  <h5>Enter Booking Details</h5>
                                        </div>
                                        <div class="col-md-3 deactive">
                                                  <h5>Booking Completed</h5>
                                        </div>
                                        <div class="col-md-3">
                                                  .
                                        </div>
                              </div>

                    </div>
                    <?= form_open('booking_bnb_next/'.$rowid,'id="bookingForm"') ?>
                    <div class="col-md-7">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Bed and Breakfast: Sembalun Lodge</h4>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Check-in date<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                                  <!-- Date dd/mm/yyyy -->
                                                  <div class="form-group">
                                                            <div class="input-group">
                                                                      <div class="input-group-addon">
                                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                                      </div>
                                                                      <input type="text" name="or_checkin_date" class="form-control" id="or_checkin_date" required>

                                                            </div><!-- /.input group -->
                                                  </div><!-- /.form group -->


                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Check-out date<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-4">
                                                  <div class="form-group">
                                                            <div class="input-group">
                                                                      <div class="input-group-addon">
                                                                                <i class="glyphicon glyphicon-calendar"></i>
                                                                      </div>
                                                                      <input type="text" name="or_checkout_date" class="form-control" id="or_checkout_date" required>
                                                            </div><!-- /.input group -->
                                                  </div><!-- /.form group -->
                                        </div>
                              </div>

                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Full name<span class="required">*</span></label>
                                                  <br>
                                                  <span class="atention">
                                                            Please list all guests names. Thank you.
                                                  </span>
                                        </div>
                                        <div class="col-md-9">
                                                  <div class="input_fields_wrap">
                                                            <button class="add_field_button btn btn-primary btn-xs">Add</button><br>
                                                            <div>
                                                                      <input type="text" class="form-control form-width-70" name="or_guest_name[]" placeholder="Full name" required>
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">

                                        <div class="col-md-12">
                                                  <label>(<span class="required">*</span>) Indicates required field.</label>
                                        </div>

                              </div>
                    </div>

                    <div class="col-md-5 box-side-round">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Summary:</h4>
                                        </div>
                              </div>
                              <?php if ($this->cart->total_items() > 0): ?>
                                                  <table class="table table-responsive">
                                                            <?php foreach ($this->cart->contents() as $items): ?>
                                                            <?php if($items['typer'] == 'bnb' && $items['rowid']==$rowid):?>
                                                            <thead>
                                                                      <tr>
                                                                                <th colspan="2">Bed and Breakfast</th>
                                                                                <th><span class="pull-right">US$</span></th>
                                                                      </tr>
                                                            </thead>
                                                            <tbody>
                                                                                <?php foreach ($bnb as $sp) : ?>
                                                                                          <?php if ($sp->id == $items['id']): ?>
                                                                                                    <tr>

                                                                                                              <td><img src="<?= $sp->bnb_image ?>" width="70"></td>
                                                                                                              <td>
                                                                                                                        <?= $sp->bnb_name ?>
                                                                                                                        <p><?= $items['qty']; ?> <?= $sp->bnb_type ?>.<br>
                                                                                                                                  Capacity: <?= $sp->bnb_capacity ?> people<br>
                                                                                                                                  US$ <?= number_format($items['price'], 2, ".", ","); ?>
                                                                                                                        </p>

                                                                                                              </td>

                                                                                                              <td>
                                                                                                                        <span class="pull-right"><?= number_format($items['price'], 2, ".", ","); ?></span>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                              <td colspan="3">
                                                                                                                        <p class="price-includes">
                                                                                                                                  <strong>Price includes:</strong><br>
                                                                                                                                  <?= $sp->bnb_short_description ?>
                                                                                                                        </p>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <?php endif; ?>
                                                                                          <?php endforeach; ?>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                                                                          <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                                    <tr>
                                                                                                              <td><?= $option_name; ?></td><td><?= $option_value ?></td><td></td>
                                                                                                    </tr>          

                                                                                          <?php endforeach; ?>
                                                                               <?php endif; ?>
                                                            </tbody>
                                                            <tfoot>
                                                                      <tr>
                                                                                <th colspan="2">TOTAL AMOUNT TO BE PAID </th>
                                                                                <th width="30%"><span class="pull-right total-price">US$<?= number_format($items['price'], 2, ".", ","); ?></span></th>
                                                                      </tr>
                                                            </tfoot>
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                  </table>
                                        <?php endif; ?>
                              <input type="submit" name="Oke" value="Continue Booking" class="btn btn-primary ok-button">
                    </div>
                    <?= form_close() ?>
                    
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>

<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>
<!-- Select2 -->
<script src="<?= base_url('assets/plugin/sweet-allert/sweetalert2.min.js');?>"></script>
<script src="<?= base_url('assets/plugin/select2/select2.full.min.js'); ?>"></script>
<!-- InputMask -->
<script src="<?= base_url('assets/plugin/input-mask/jquery.inputmask.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/input-mask/jquery.inputmask.date.extensions.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/input-mask/jquery.inputmask.extensions.js'); ?>"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<!-- ajax validate -->
<script src="<?= base_url('assets/plugin/ajax_validate/jquery.validate.min.js');?>"></script>
<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    },5000);

          });
//-->
</script>
<script>
          $("bookingForm").validate();
</script>
<script>
          $('.ok-button').on('click', function() {
                    swal({
                      title: 'Royal Rinjani Tour',
                      text: 'Continue your booking..',
                      type: 'success',
                      animation: false
                    }).done();
                  });
          
</script>
<script>
                       var dateToday = new Date();
                       var dates = $("#or_checkin_date, #or_checkout_date").datepicker({
                             defaultDate: "+1w",
                             changeMonth: true,
                             numberOfMonths: 3,
                             minDate: dateToday,
                             onSelect: function (selectedDate) {
                                              var option = this.id == "or_checkin_date" ? "minDate" : "maxDate",
                                                    instance = $(this).data("datepicker"),
                                                    date = $.datepicker.parseDate(instance.settings.dateFormat || $.datepicker._defaults.dateFormat, selectedDate, instance.settings);
                                                    dates.not(this).datepicker("option", option, date);
                                             }
                            });
                           $("#or_checkin_date, #or_checkout_date").datepicker("option", "dateFormat", "dd-mm-yy");
                                        
</script>
<script>
          $(document).ready(function () {
                    var max_fields = 10; //maximum input boxes allowed
                    var wrapper = $(".input_fields_wrap"); //Fields wrapper
                    var add_button = $(".add_field_button"); //Add button ID

                    var x = 1; //initlal text box count
                    $(add_button).click(function (e) { //on add input button click
                              e.preventDefault();
                              if (x < max_fields) { //max input box allowed
                                        x++; //text box increment
                                        $(wrapper).append('\
                                        <div><hr class="hr-float"><input type="text" class="form-control form-width-70" name="or_guest_name[]" placeholder="Full name"><a href="#" class="remove_field"><i class="glyphicon glyphicon-minus-sign"></i></a></div><br>\
                                        '); //add input box
                              }
                    });

                    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
                              e.preventDefault();
                              $(this).parent('div').remove();
                              x--;
                    });
          });
</script>
<script>
          $(function () {
                    //Initialize Select2 Elements
                    $(".select2").select2();

                    //Datemask dd/mm/yyyy
                    $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                    //Datemask2 mm/dd/yyyy
                    $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                    //Money Euro
                    $("[data-mask]").inputmask();
          });
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script type="text/javascript">
          $(function () {
                    var offset = $("#sidebar-fly").offset();
                    var topPadding = 40;
                    $(window).scroll(function () {
                              if ($(window).scrollTop() > offset.top) {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: $(window).scrollTop() - offset.top + topPadding
                                        });
                              } else {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: 0
                                        });
                              }
                              ;
                    });
          });
</script>
<script>
          (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                              (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
          })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>