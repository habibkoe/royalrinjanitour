<div class="container box-home">
          <div class="row">
                    <?php if ($this->session->flashdata('Errorbooking')): ?>
                                        <div class="col-md-12">
                                                  <div class="alert alert-block alert-danger fade in">
                                                            <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                            <?= $this->session->flashdata('Errorbooking') ?>
                                                  </div>
                                        </div>
                              <?php endif ?>
                    <div class="col-md-12 breadcrumb-booking">
                              <div class="row">
                                        <div class="col-md-3">
                                                  .
                                        </div>
                                        <div class="col-md-3 activated">
                                                  <h5>Enter Booking Details</h5>
                                        </div>
                                        <div class="col-md-3 active">
                                                  <h5>Booking Completed</h5>
                                        </div>
                                        <div class="col-md-3">
                                                  .
                                        </div>
                              </div>

                    </div>
                    <?= form_open('bnb_booking_finish/'.$rowid, 'id="bookingForm2"') ?>
                    <div class="col-md-7">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Booking Contact</h4>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Full Name<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                                  <input type="text" class="form-control" name="or_full_name" required>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Address<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                                  <textarea class="form-control" name="or_address" rows="4" required></textarea>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Email<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                                  <input type="email" class="form-control" name="or_email" required>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Mobile Phone<span class="required">*</span></label>
                                        </div>
                                        <div class="col-md-8">
                                                  <input type="text" class="form-control" name="or_phone" placeholder="(+)(country code)(number)" required>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Home / Work Phone</label>
                                        </div>
                                        <div class="col-md-8">
                                                  <input type="text" class="form-control" name="or_home_phone" placeholder="(+)(country code)(area code)(number)" required>
                                        </div>
                              </div>
                              <hr class="hr-booking">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Health Info</h4>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Allergies, Special Diets Or Vegetarian</label>
                                        </div>
                                        <div class="col-md-8">
                                                  <textarea class="form-control" name="or_special_diets"rows="4"></textarea>
                                        </div>
                              </div>
                              <hr class="hr-booking">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Special Request</h4>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>Your Message</label>
                                        </div>
                                        <div class="col-md-8">
                                                  <textarea class="form-control" name="or_message"rows="4"></textarea>
                                                  <small>We are happy to help your plan and organize your holiday activities</small>
                                        </div>
                              </div>
                              <hr class="hr-booking">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Payment Methods<span class="required">*</span></h4>
                                        </div>
                              </div>

                              <div class="row margin-bottom-10">

                                        <div class="col-md-4">

                                        </div>
                                        <div class="col-md-8">
                                                  Paypal Bank Mandiri:<br>
                                                  Account Number -.
                                                  <hr>
                                                  Western Union:<br>
                                                  Recipient: Royal Sembahulun.
                                                  <hr>
                                                  PT. Bank Rakyat Indonesia. <br>
                                                  Branch Address: Jl. Pejagik No.16, Mataram, <br>
                                                  Nusa Tenggara Barat, Indonesia. <br>
                                                  Office: +62212510244. <br>
                                                  <br>
                                                  Branch: Mataram.<br>
                                                  Account number 759801003465538. <br>
                                                  Name: Royal Sebahulun. <br>
                                                  Swift Code (BIC): BRINIJA. <br>
                                                  National Code Bank: 0020307.<br>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-4">
                                                  <label>I would like to pay by</label>
                                        </div>
                                        <div class="col-md-8">
                                                  <div class="form-group">
                                                            <input type="radio" name="or_payment_method" value="Paypal Mandiri" id="paypal">
                                                            <label for="paypal">Paypal Bank Mandiri.</label>
                                                  </div>
                                                  <div class="form-group">
                                                            <input type="radio" name="or_payment_method" value="Western Union" id="western">
                                                            <label for="western">Western Union.</label>
                                                  </div>
                                                  <div class="form-group">
                                                            <input type="radio" name="or_payment_method" value="PT. Bank Rakyat Indonesia" id="bri">
                                                            <label for="bri">Bank Transfer: PT. Bank Rakyat Indonesia.</label>
                                                  </div> 
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">

                                        <div class="col-md-12">
                                                  <label>(<span class="required">*</span>) Indicates required field.</label>
                                        </div>

                              </div>
                    </div>

                    <div class="col-md-5 box-side-round">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Summary:</h4>
                                        </div>
                              </div>
                              <?php if ($this->cart->total_items() > 0): ?>
                                                  <table class="table table-responsive">
                                                            <?php foreach ($this->cart->contents() as $items): ?>
                                                            <?php if($items['typer'] == 'bnb' && $items['rowid']==$rowid):?>
                                                            <thead>
                                                                      <tr>
                                                                                <th colspan="2">Bed and Breakfast</th>
                                                                                <th><span class="pull-right">US$</span></th>
                                                                      </tr>
                                                            </thead>
                                                            <tbody>
                                                                                <?php foreach ($bnb as $sp) : ?>
                                                                                          <?php if ($sp->id == $items['id']): ?>
                                                                                                    <tr>
                                                                                                              <td><img src="<?= $sp->bnb_image ?>" width="70"></td>
                                                                                                              <td>
                                                                                                                        <?= $sp->bnb_name ?>
                                                                                                                        <p><?= $items['qty']; ?> <?= $sp->bnb_type ?>.<br>
                                                                                                                                  Capacity: <?= $sp->bnb_capacity ?> people<br>
                                                                                                                                  US$ <?= number_format($items['price'], 2, ".", ","); ?>
                                                                                                                                  <input type="hidden" name="booking_guest_total" value="<?= $sp->bnb_capacity ?>">
                                                                                                                        </p>

                                                                                                              </td>

                                                                                                              <td>
                                                                                                                        <span class="pull-right"><?= number_format($items['price'], 2, ".", ","); ?></span>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                              <td colspan="3">
                                                                                                                        <p class="price-includes">
                                                                                                                                  <strong>Price includes:</strong><br>
                                                                                                                                            <?= $sp->bnb_short_description ?>
                                                                                                                                  <input type="hidden" name="or_price_includes" value="<?= $sp->bnb_short_description ?>">
                                                                                                                        </p>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <?php endif; ?>
                                                                                          <?php endforeach; ?>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                                                                          <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                                    <tr>
                                                                                                              <td><?= $option_name; ?></td><td><?= $option_value ?></td><td></td>
                                                                                                    </tr>          

                                                                                          <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                                <tr>
                                                                                          <td colspan="3">
                                                                                                    <?= anchor('booking/remove/' . $items['rowid'], ' ', ['class' => 'glyphicon glyphicon-trash', 'onclick' => 'return confirm(\'I would like to cancel my booking.\')', 'title' => 'cencel booking']); ?>
                                                                                          </td>
                                                                                </tr>
                                                                     

                                                                      <tr>
                                                                                <td>
                                                                                          Check in date
                                                                                </td>
                                                                                <td>
                                                                                          <?= $or_checkin_date ?>
                                                                                          <input type="hidden" name="or_checkin_date" value="<?= $or_checkin_date ?>">
                                                                                </td>
                                                                                <td>

                                                                                </td>
                                                                      </tr>

                                                                      <tr>
                                                                                <td>
                                                                                          Check out date
                                                                                </td>
                                                                                <td>
                                                                                          <?= $or_checkout_date ?>
                                                                                          <input type="hidden" name="or_checkout_date" value="<?= $or_checkout_date ?>">
                                                                                </td>
                                                                                <td>

                                                                                </td>
                                                                      </tr>
                                                                      <tr>
                                                                                <td colspan="2">
                                                                                          Night(s)
                                                                                </td>
                                                                                <td>
                                                                                          <span class="pull-right"><?= $or_long_night ?></span>
                                                                                          <input type="hidden" name="or_long_night" value="<?= $or_long_night ?>">
                                                                                </td>
                                                                                <td>

                                                                                </td>
                                                                      </tr>
                                                                      <tr>
                                                                                <td>
                                                                                          Guest (s)
                                                                                </td>
                                                                                <td>
                                                                                          <?= $or_guest_name ?>
                                                                                          <input type="hidden" name="or_guest_name" value="<?= $or_guest_name ?>">
                                                                                </td>
                                                                                <td>

                                                                                </td>
                                                                      </tr>
                                                            </tbody>
                                                            <tfoot>
                                                                      <tr>
                                                                                <th colspan="2">Total Bed and Breakfast </th>
                                                                                <input type="hidden" name="or_price" value="<?= $items['price'] * $or_long_night ?>">
                                                                                <th width="30%"><span class="pull-right total-price">US$<?= number_format($items['price'] * $or_long_night, 2, ".", ","); ?></span></th>
                                                                      </tr>
                                                                      <tr>
                                                                                <td colspan="3">
                                                                                          <input type="checkbox" name="or_check" value="yes" required checked="checked">
                                                                                          I acknowledge that I have read, understand, 
                                                                                          and agree to Royal Rinjani Tour and Bed and Breakfast Sembalun Lodge Terms and Conditions
                                                                                </td>
                                                                      </tr>
                                                                      <tr>
                                                                                <td colspan="3">
                                                                                          <?php echo $captcha // tampilkan recaptcha ?>
                                                                                </td>
                                                                      </tr>
                                                                      
                                                            </tfoot>
                                                            <?php endif; ?>
                                                             <?php endforeach; ?>
                                                  </table>
                                        <?php endif; ?>
                              <input type="submit" name="Oke" value="Finish Booking"  class="btn btn-primary  ok-button">
                    </div>
                    <?= form_close() ?>
                    
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<?php echo $script_captcha; // javascript recaptcha ?>
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>
<!-- ajax validate -->
<script src="<?= base_url('assets/plugin/sweet-allert/sweetalert2.min.js');?>"></script>
<script src="<?= base_url('assets/plugin/ajax_validate/jquery.validate.min.js');?>"></script>
<script>
          $("bookingForm2").validate();
</script>
<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    },5000);

          });
//-->
</script>
<script>
          $('.ok-button').on('click', function() {
                    swal({
                      title: 'Royal Rinjani Tour',
                      text: 'Your book finished. Thank you.',
                      imageUrl: 'http://royalrinjanitour.com/assets/img/rinjani_ok.JPG',
                      imageWidth: 400,
                      imageHeight: 200,
                      animation: false
                    }).done();
                  });
</script>
<script>
                                        $(document).ready(function () {
                                                  $('input').iCheck({
                                                            checkboxClass: 'icheckbox_square-yellow',
                                                            radioClass: 'iradio_square-yellow',
                                                            increaseArea: '20%' // optional
                                                  });
                                        });
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script type="text/javascript">
          $(function () {
                    var offset = $("#sidebar-fly").offset();
                    var topPadding = 40;
                    $(window).scroll(function () {
                              if ($(window).scrollTop() > offset.top) {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: $(window).scrollTop() - offset.top + topPadding
                                        });
                              } else {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: 0
                                        });
                              }
                              ;
                    });
          });
</script>
<script>
          (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                              (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
          })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>