<div class="container box-home">
          <div class="row">
                    <?php if ($this->session->flashdata('Errorbooking')): ?>
                                        <div class="col-md-12">
                                                  <div class="alert alert-block alert-danger fade in">
                                                            <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                            <?= $this->session->flashdata('Errorbooking') ?>
                                                  </div>
                                        </div>
                              <?php endif ?>
                    <div class="col-md-12">
                              <h3><?= $page_name ?></h3>
                    </div>
                    <?= form_open('booking_next') ?>
                    <div class="col-md-7">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Programs</h4>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Departure date*</label>
                                        </div>
                                        <div class="col-md-4">
                                                  <input type="date" class="form-control" name="or_departure_date" placeholder="mm/dd/yyyy">
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Arrival date*</label>
                                        </div>
                                        <div class="col-md-4">
                                                  <input type="date" class="form-control" name="or_arrival_date" placeholder="mm/dd/yyyy">
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Pick up from*</label>
                                        </div>
                                        <div class="col-md-9">
                                                  <textarea class="form-control" name="or_pick_up_from" rows="4"></textarea>
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Drop off to*</label>
                                        </div>
                                        <div class="col-md-9">
                                                  <textarea class="form-control" name="or_drop_of_to" rows="4"></textarea>
                                        </div>
                              </div>
                              <hr class="hr-booking">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Rent a car</h4>
                                        </div>
                              </div>
                              
                              <div class="row margin-bottom-10">
                                        <div class="col-md-3">
                                                  <label>Date from</label>
                                        </div>
                                        <div class="col-md-4">
                                                  <input type="date" name="or_rent_start_date" class="form-control" placeholder="mm/dd/yyyy">
                                        </div>
                                        <div class="col-md-1">
                                                  <label>to</label>
                                        </div>
                                        <div class="col-md-4">
                                                  <input type="date" name="or_rent_end_date" class="form-control" placeholder="mm/dd/yyyy">
                                        </div>
                              </div>
                              <div class="row margin-bottom-10">

                                        <div class="col-md-3">
                                                  <label>Preferred car:</label>
                                        </div>
                                        <div class="col-md-9">
                                                  <input type="text" name="or_prefered_car" class="form-control">
                                        </div>
                              </div>
                              
                    </div>
                    <div class="col-md-5 box-side-round">
                              <div class="row">
                                        <div class="col-md-12">
                                                  <h4 class="h4-booking">Summary:</h4>
                                        </div>
                              </div>
                              <?php if ($this->cart->total_items() > 0): ?>
                                                  <table class="table table-responsive">
                                                            <thead>
                                                                      <tr>
                                                                                <th colspan="2">Programs</th>
                                                                                <th><span class="pull-right">US$</span></th>
                                                                      </tr>
                                                            </thead>
                                                            <tbody>
                                                                      <?php foreach ($this->cart->contents() as $items): ?>
                                                                                <tr>
                                                                                          <?php
                                                                                          foreach ($programs as $sp) :
                                                                                                    if ($sp->id == $items['id']) {
                                                                                                              ?>
                                                                                                              <td><img src="<?= $sp->pr_image ?>" width="70"></td>
                                                                                                              <td>
                                                                                                                        <?= $sp->pr_title ?>
                                                                                                                        <p>Total guest(s) <?= $items['qty']; ?> person.<br>
                                                                                                                        US$ <?= number_format($items['price'], 2, ".", ","); ?> @person.
                                                                                                                        </p>
                                                                                                                        
                                                                                                              </td>
                                                                                                    <?php } endforeach; ?>
                                                                                          <td>
                                                                                                    <span class="pull-right"><?= number_format($items['price'] * $items['qty'], 2, ".", ","); ?></span>
                                                                                          </td>
                                                                                </tr>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                                                                          <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                                    <tr>
                                                                                                              <td><?= $option_name; ?></td><td><?= $option_value?></td><td></td>
                                                                                                    </tr>          

                                                                                          <?php endforeach; ?>

                                                                                <?php endif; ?>
                                                                                                    <tr><td colspan="3"><?= anchor('booking/remove/'.$items['rowid'], ' ', ['class'=>'glyphicon glyphicon-trash','onclick' => 'return confirm(\'Anda yakin ingin hapus barang.?\')', 'title' =>'hapus programs']); ?></td></tr>
                                                                      <?php endforeach; ?>
                                                            </tbody>
                                                            <tfoot>
                                                                      <tr>
                                                                                <th colspan="2">Total US$</th>
                                                                                <th width="30%"><span class="pull-right"><?= number_format($this->cart->total(), 2, ".", ","); ?></span></th>
                                                                      </tr>
                                                            </tfoot>
                                                  </table>
                                        <?php endif; ?>
                              <input type="submit" name="Oke" value="Continue Booking" class="btn btn-primary">
                    </div>
                    <?= form_close() ?>
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script type="text/javascript">
          $(function () {
                    var offset = $("#sidebar-fly").offset();
                    var topPadding = 40;
                    $(window).scroll(function () {
                              if ($(window).scrollTop() > offset.top) {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: $(window).scrollTop() - offset.top + topPadding
                                        });
                              } else {
                                        $("#sidebar-fly").stop().animate({
                                                  marginTop: 0
                                        });
                              }
                              ;
                    });
          });
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>