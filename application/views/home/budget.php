<div class="container box-home">
          <?= form_open('budget/searchbudget') ?>
          <div class="row box-find-budget">
                    <div class="col-md-12 box-head">
                              Search and book cheap holiday packet in 3 easy steps here!
                              
                    </div>
                    <div class="col-md-4">
                              <h3>Your country</h3>
                              <select name="bu_from" class="form-control">
                                        <option value="malaysia">Malaysia</option>
                                        <option value="filiphina">Filiphina</option>
                                        <option value="thailand">Thailand</option>
                                        <option value="kambodja">Kambodja</option>
                                        <option value="jepang">Jepang</option>
                                        <option value="korea">Korea</option>
                              </select>
                    </div>
                    <div class="col-md-4">
                              <h3>Far</h3>
                              <select name="bu_far" class="form-control">
                                        <option value="sangat_dekat">Sangat dekat</option>
                                        <option value="dekat">Dekat</option>
                                        <option value="sedang">Sedang</option>
                                        <option value="jauh">Jauh</option>
                                        <option value="sangat_jauh">Sangat jauh</option>
                              </select>
                              
                    </div>
                    <div class="col-md-4">
                              <h3>Your cost budget</h3>
                              <input type="text" class="form-control" name="bu_cost">
                              <hr>
                              <input type="submit" name="search" value="Find" class="btn btn-primary">
                    </div>
          </div>
          <?= form_close() ?>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>

<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    }, 3000);

          });
//-->
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });

          $('input#person').on('ifChanged', function () {
                    $('.konten').hide();
                    $('.' + $(this).data('rel')).show();
          });
          $('input#no-person').on('ifChanged', function () {
                    $('.konten').hide();
          });
</script>

<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>