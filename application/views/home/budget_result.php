<div class="container box-home">
          <?= form_open('budget/searchbudget') ?>
          <div class="row">
                    <div class="col-md-12">
                              Your result found
                    </div>
          </div>
          <div class="row">
                    <div class="col-md-12">
                              <table class="table table-striped table-bordered">
                                        <tr><th>Name destination</th><th>Price</th><th>Book</th></tr>
                                        <?php foreach($budget as $value): ?>
                                        <tr><td><?= $value->bu_destination_name ?></td><td><?= $value->bu_cost ?></td><td><a class="btn btn-warning">Booking now</a></td></tr>
                                        <?php endforeach; ?>
                              </table>
                    </div>
          </div>
          <?= form_close() ?>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>

<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    }, 3000);

          });
//-->
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });

          $('input#person').on('ifChanged', function () {
                    $('.konten').hide();
                    $('.' + $(this).data('rel')).show();
          });
          $('input#no-person').on('ifChanged', function () {
                    $('.konten').hide();
          });
</script>

<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>