<div class="container box-home" style="min-height: 400px;">
          <div class="row">
                    <div class="col-md-12">
                              <h3><?= $page_name ?></h3>
                    </div>
                    <div class="col-md-2">
                              .
                    </div>
                    <div class="col-md-8">
                              <?php if ($this->cart->total_items() > 0): ?>
                              <div class="row">
                                        <table class="table table-condensed table-hover">
                                                  <thead>
                                                            <tr>
                                                                      <th colspan="2">Booking List</th>
                                                                      <th>Qty</th>
                                                                      <th>Price</th>
                                                                      <th>Total Price</th>
                                                                      <th></th>
                                                            </tr>
                                                  </thead>
                                                  <tbody>
                                                            <?php foreach ($this->cart->contents() as $items): ?>
                                                            <?php if($items['typer']=='programs'): ?>
                                                            <tr>
                                                                    <?php foreach($programs as $sp):
                                                                            if($sp->id==$items['id']){ ?>
                                                                      
                                                                      <td>
                                                                              <img src="<?= $sp->pr_image ?>" width="150">
                                                                      </td>
                                                                      <td>
                                                                                      <?= $sp->pr_title ?>
                                                                                <br>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                                                                <?php foreach ($this->cart->product_options($items['rowid']) as $meong => $garit): ?>
                                                                                <strong><?= $meong ?>:</strong> <?= $garit ?> people
                                                                                <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <strong>Type:</strong> <span style="color: tomato"><?= $items['typer'] ?></span>
                                                                                <hr>
                                                                                <?= $sp->pr_short_description ?>
                                                                      
                                                                      </td>
                                                                      
                                                                      <td><input type="number" name="person" class="input-person" value="<?= $items['qty']; ?>" readonly=""></td>
                                                                      
                                                                      <td>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                                                                <?php foreach ($this->cart->product_options($items['rowid']) as $meong => $garit): ?>
                                                                                $ <?= number_format($items['price'] / $garit, 2, ".", ","); ?>
                                                                                <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                      </td>
                                                                      <td>
                                                                                $ <?= number_format($items['price'] * $items['qty'], 2, ".", ","); ?>
                                                                      </td>
                                                                      <td><?= anchor('booking/remove/'.$items['rowid'], ' ', ['class'=>'glyphicon glyphicon-trash','onclick' => 'return confirm(\'I would like to cancel my booking.\')', 'title' =>'cencel booking']); ?></td>
                                                            
                                                            <?php } endforeach; ?>
                                                            </tr>
                                                            <tr>
                                                                      <td colspan="6"><a href="<?= base_url('booking_include_all/'.$items['rowid']) ?>" class="btn-sm btn-primary">Continue Booking</a></td>
                                                            </tr>
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            
                                                             <?php foreach ($this->cart->contents() as $items): ?>
                                                            <?php if($items['typer']=='bnb'): ?>
                                                            <tr>
                                                                    <?php foreach($bnb as $sp):
                                                                            if($sp->id==$items['id']){ ?>
                                                                      
                                                                      <td>
                                                                              <img src="<?= $sp->bnb_image ?>" width="150">
                                                                      </td>
                                                                      <td>
                                                                                      <?= $sp->bnb_name ?>
                                                                                <br>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                                                                <?php foreach ($this->cart->product_options($items['rowid']) as $meong => $garit): ?>
                                                                                <strong><?= $meong ?>:</strong> <?= $garit ?> people
                                                                                <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                                <br>
                                                                                <strong>Type:</strong> <span style="color: tomato"><?= $items['typer'] ?></span>
                                                                                <hr>
                                                                                <?= $sp->bnb_short_description ?>
                                                                                
                                                                      
                                                                      </td>
                                                                      
                                                                      <td><input type="number" name="person" class="input-person" value="<?= $items['qty']; ?>" readonly=""></td>
                                                                      
                                                                      <td>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>
                                                                                <?php foreach ($this->cart->product_options($items['rowid']) as $meong => $garit): ?>
                                                                                $ <?= number_format($items['price'] / $garit, 2, ".", ","); ?>
                                                                                <?php endforeach; ?>
                                                                                <?php endif; ?>
                                                                      </td>
                                                                      <td>
                                                                                $ <?= number_format($items['price'] * $items['qty'], 2, ".", ","); ?>
                                                                      </td>
                                                                      <td><?= anchor('booking/remove/'.$items['rowid'], ' ', ['class'=>'glyphicon glyphicon-trash','onclick' => 'return confirm(\'I would like to cancel my booking.\')', 'title' =>'cencel booking']); ?></td>
                                                            
                                                            <?php } endforeach; ?>
                                                            </tr>
                                                            <tr>
                                                                      <td colspan="6"><a href="<?= base_url('booking_bnb/'.$items['rowid']) ?>" class="btn-sm btn-primary">Continue Booking</a></td>
                                                            </tr>
                                                            <?php endif; ?>
                                                            <?php endforeach; ?>
                                                            
                                                  </tbody>
                                                  <tfoot>
                                                            
                                                  </tfoot>
                                        </table>
                              
                              </div>
                              <?php else: ?>
                              <div class="row" style="text-align: center;">
                                        <h3 style="text-align: center;"><strong>Your Item(s) is empty.</strong></h3>
                                        <br>
                                        <img src="<?= base_url('assets/img/empty_cart.png') ?>" width="250">
                              </div>
                              
                              <?php endif; ?>
                              <div class="row" style="text-align: center;">
                              <hr>
                              <h5>Booking another package</h5>
                               <a href="<?= base_url('programactivities') ?>" class="btn btn-success">Programs package</a> | <a href="<?= base_url('bed-and-breakfast') ?>" class="btn btn-success"><img src="<?= base_url('assets/img/sl-logo.JPG') ?>" width="20" style="border-radius: 50%;"> BnB package</a>
                              </div>
                    </div>

                    <div class="col-md-2">
                              .
                    </div>

          </div>
          <div class="row">
                    <div class="container box-home">
                    <?php foreach ($program_activities as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->pr_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->pr_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><?= $value->pr_title ?></h3>
                                                                                To continue, please click “More Details and Booking" button. Thank you.
                                                                      <p><a class="btn btn-default btn-sm" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                            </a>
                                                  </div><!-- /.col-md-4 -->
                                        
                                        <?php endforeach; ?>
                              <div class="col-md-12">
                                        <a href="<?= base_url('programactivities') ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
                    </div>
          </div>
          <div class="row">
                    <div class="container box-home">
                              <?php foreach ($bnb_bottom as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->bnb_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->bnb_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->bnb_short_description)) {
                                                                                          echo $value->bnb_short_description;

                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                                      <p><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" class="btn btn-default btn-sm" role="button">More Details and Booking &raquo;</a></p>
                                                            </a>
                                                  </div><!-- /.col-md-3 -->
                                        <?php endforeach; ?>
                              <div class="col-md-12 col-lg-12">
                                        <a href="<?= base_url('bed-and-breakfast') ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/jQuery.loadScroll.js'); ?>"></script>
<script>
$(function() {  

    // Custom fadeIn Duration
    $('.box-home-inside a img').loadScroll(1000);

});
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>