<!-- Carousel
    ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->
          <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
          </ol>
          <div class="carousel-inner" role="listbox">
                    <?php foreach ($first_slider as $value): ?>
                                        <div class="item active">
                                                  <img class="first-slide" src="<?= $value->slider_image ?>" alt="<?= $value->slider_title ?>">
                                                  <div class="container">
                                                            <div class="carousel-caption">
                                                            </div>
                                                  </div>
                                        </div>
                              <?php endforeach; ?>
                    <?php foreach ($next_slider as $value): ?>
                                        <div class="item">
                                                  <img class="first-slide" src="<?= $value->slider_image ?>" alt="<?= $value->slider_title ?>">
                                                  <div class="container">
                                                            <div class="carousel-caption">
                                                            </div>
                                                  </div>
                                        </div>
                              <?php endforeach; ?>
          </div>
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
          </a>
</div><!-- /.carousel -->
<section id="program_activities">
          <div class="title-home bg-6">
                    <a href="<?= base_url('programactivities') ?>">PROGRAMS</a>
          </div>
          <div class="container box-home">
                              <?php foreach ($program_activities as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->pr_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->pr_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><?= $value->pr_title ?></h3>
                                                                                To continue, please click “More Details and Booking" button. Thank you.
                                                                      <p><a class="btn btn-default btn-sm" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                            </a>
                                                  </div><!-- /.col-md-4 -->
                                        
                                        <?php endforeach; ?>
                              <div class="col-md-12">
                                        <a href="<?= base_url('programactivities') ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
</section>
<section id="accomodation-transportation">
          <div class="title-home bg-1">
                    <img src="<?= base_url('assets/img/sl-logo.JPG') ?>" width="20" style="border-radius: 50%;"> <a href="<?= base_url('bed-and-breakfast') ?>"> BnB-Rent a Car </a>
          </div>
          <div class="container box-home">
                              <?php foreach ($bnb as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->bnb_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->bnb_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->bnb_short_description)) {
                                                                                          echo $value->bnb_short_description;

                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                                      <p><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" class="btn btn-default btn-sm" role="button">More Details and Booking &raquo;</a></p>
                                                            </a>
                                                  </div><!-- /.col-md-3 -->
                                        <?php endforeach; ?>
                              <div class="col-md-12 col-lg-12">
                                        <a href="<?= base_url('bed-and-breakfast') ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
</section>

<section id="traditional-culture">
          <div class="title-home bg-3">
                    <a href="<?= base_url('posts/category/nature-and-history') ?>"> Nature and History</a>
          </div>
          <div class="container box-home">
                              <?php foreach ($nature as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('posts/view/' . $value->posts_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->posts_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->posts_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->posts_short_description)) {
                                                                                          echo $value->posts_short_description;
                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                            </a>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                              <div class="col-md-12">
                                        <a href="<?= base_url('posts/category/' . $value->at_url) ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
</section>

<section id="food-and-drink">
          <div class="title-home bg-4">
                    <a href="<?= base_url('posts/category/food-and-drink') ?>">Food and Drink</a>
          </div>
          <div class="container box-home">
                              <?php foreach ($food_and_drink as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('posts/view/' . $value->posts_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->posts_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->posts_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->posts_short_description)) {
                                                                                          echo $value->posts_short_description;
                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                            </a>
                                                  </div><!-- /.col-md-3 -->
                                        <?php endforeach; ?>
                              <div class="col-md-12">
                                        <a href="<?= base_url('posts/category/' . $value->at_url) ?>"class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
</section>
<section id="traditional-culture">
          <div class="title-home bg-5">
                    <a href="<?= base_url('posts/category/art-and-culture') ?>"> Art and Culture</a>
          </div>
          <div class="container box-home">
                              <?php foreach ($art_and_culture as $value): ?>
                                                  <div class="col-md-3 col-sm-6 col-xs-12 box-home-inside">
                                                            <a href="<?= base_url('posts/view/' . $value->posts_url) ?>">
                                                                      <img class="img-responsive img-rounded-2px" data-src="<?= $value->posts_image ?>" src="<?= base_url('assets/img/facebook.gif') ?>">
                                                                      <h6 class="img-credit">Picture: <?= $value->posts_image_credit ?></h6>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->posts_short_description)) {
                                                                                          echo $value->posts_short_description;
                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                            </a>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                              <div class="col-md-12">
                                        <a href="<?= base_url('posts/category/' . $value->at_url) ?>" class="pull-right btn btn-default btn-sm btn-pink">More +</a>
                              </div>
          </div>
</section>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/jQuery.loadScroll.js'); ?>"></script>
<script>
$(function() {  

    // Custom fadeIn Duration
    $('.box-home-inside a img').loadScroll(1000);

});
</script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>
