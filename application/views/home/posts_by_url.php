<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <?= $page_name ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-index"><?= $page_name ?></h3>
                              <div class="row">
                              <?php foreach ($posts_by_url as $value): ?>
                                                  <div class="col-md-6 col-sm-6 col-xs-12 box-detail">
                                                            <a href="<?= base_url('posts/view/'.$value->posts_url) ?>">
                                                                      <h6 class="box-category"><?= $value->at_name ?></h6>
                                                                      <?php if ($value->posts_image != 'No image'): ?>
                                                                                          <img class="img-responsive img-rounded-2px" src="<?= $value->posts_image ?>">
                                                                                          <h6 class="img-credit">Picture: <?= $value->posts_image_credit ?></h6>
                                                                                <?php endif; ?>
                                                                      <h3 class="sub-title-home"><a href="<?= base_url('posts/view/'.$value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                      <div class="box-content">
                                                                                <?php
                                                                                if (!empty($value->posts_short_description)) {
                                                                                          echo $value->posts_short_description;
                                                                                }
                                                                                else {
                                                                                          echo '';
                                                                                }
                                                                                ?>
                                                                      </div>
                                                            </a>
                                                  </div><!-- /.col-md-3 -->
                                        <?php endforeach; ?>
                              </div>
                    </div>

                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-heart"></i> Other Bed and Breakfast</h3>
                              <?php foreach ($bnb_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->bnb_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-link"></i> Other Programs</h3>
                              <?php foreach ($program_activities_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->pr_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></h3>

                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details &raquo;</a></p>

                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>