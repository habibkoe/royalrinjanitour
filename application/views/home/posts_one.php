<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <a href="<?= base_url('info') ?>"><?= $page_name ?> ></a> <?= $posts_one->posts_title ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-detail"><?= $posts_one->posts_title ?></h3>
                              <h6>
                                        <i class="glyphicon glyphicon-calendar"></i> 
                                        Post date: <?= date('l, j F Y', strtotime($posts_one->created_at)) ?> - 
                                        <i class="glyphicon glyphicon-user"></i>
                                        <?= $posts_one->admin_position ?> : <?= $posts_one->admin_name ?>
                              </h6>
                              <?php if ($posts_one->posts_image != 'No image'): ?>
                                                  <img class="img-responsive img-rounded-2px" src="<?= $posts_one->posts_image ?>">
                                        <?php endif; ?>
                              <h6>Picture: <?= $posts_one->posts_image_credit ?></h6>
                              
                              <div style="float: left;margin-right: 5px;width: 80px;">
                                        <div class="fb-share-button" data-href="<?= base_url('posts/view/'.$posts_one->posts_url) ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('posts/view/'.$posts_one->posts_url) ?>&amp;src=sdkpreparse">Share</a></div>
                              </div>
                              <div style="float: left;margin-right: 5px;width: 80px;">
                                       <a href="<?= base_url('posts/view/'.$posts_one->posts_url) ?>" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                  
                              </div>
                              <div style="float: left;margin-right: 5px;width: 100px;">
                                        <!-- Place this tag where you want the share button to render. -->
                                        <div class="g-plus" data-action="share"></div>
                              </div>
                              
                              <div style="float: left; width: 100%;">
                              
                              <?= $posts_one->posts_description ?>
                              </div>
                              <div class="fb-comments" data-href="<?= base_url('posts/view/'.$posts_one->posts_url) ?>" data-width="100%" data-numposts="5"></div>
                    </div>

                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-heart"></i> Recent Info Lombok</h3>
                              <?php foreach ($posts_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->posts_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                                <h6 class="atention"><i class="glyphicon glyphicon-calendar"></i> Post date: <?= date('l, j F Y', strtotime($value->created_at)) ?></h6>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-link"></i> Other Programs</h3>
                              <?php foreach ($program_activities_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->pr_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></h3>

                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details &raquo;</a></p>

                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>