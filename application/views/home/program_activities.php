<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <?= $page_name ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-index"><?= $page_name ?></h3>
                              <div class="row">
                                        <?php foreach ($program_activities as $value): ?>
                                                            <div class="col-md-6 col-sm-6 col-xs-12 box-programs">
                                                                      <a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>">
                                                                                <h6 class="box-category"><?= $value->at_name ?></h6>

                                                                                <?php if ($value->pr_image != 'No image'): ?>
                                                                                          <img class="img-responsive img-rounded-2px" src="<?= $value->pr_image ?>">
                                                                                          <h6 class="img-credit">Picture: <?= $value->pr_image_credit ?></h6>
                                                                                <?php endif; ?>
                                                                                <h3 class="sub-title-home"><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></h3>
                                                                                To continue, please click “More Details and Booking" button. Thank you.
                                                                                <p><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" class="btn btn-default btn-sm" href="#" role="button">More Details and Booking &raquo;</a></p>
                                                                      </a>
                                                            </div><!-- /.col-md-4 -->
                                                  <?php endforeach; ?>
                              </div>
                    </div>
                    

                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-home"></i> Other Bed and Breakfast</h3>
                              <?php foreach ($bnb_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->bnb_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('bnb/view/' . $value->bnb_url) ?>"><?= $value->bnb_name ?></a></h3>
                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('bnb/view/' . $value->bnb_url) ?>" role="button">More Details and Booking &raquo;</a></p>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-heart"></i> Recent Info Lombok</h3>
                              <?php foreach ($posts_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->posts_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                                <h6 class="atention"><i class="glyphicon glyphicon-calendar"></i> Post date: <?= date('D j M Y', strtotime($value->created_at)) ?></h6>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-plane"></i> Search ticket</h3>
                              <div class="tiket-root" data-widget="tiket-boxsearchwidget" data-businessid="21071732" data-lang="en" data-size_type="normal" data-width="350" data-height="600" data-position="sidebar-right" data-product_type="flight|hotel|train"></div><script type="text/javascript" src="http://www.tiket.com/js/new_widget/tiket_widgetframe_v3.js" async="true"></script>
                    </div>

          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>