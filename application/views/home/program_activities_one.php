<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <a href="<?= base_url('programactivities') ?>">Programs and activities > </a>
                    <?= $program_activities_one->pr_title ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-detail"><?= $program_activities_one->pr_title ?></h3>
                              <h6>
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        Post date: <?= date('l, j F Y', strtotime($program_activities_one->created_at)) ?> -
                                        <i class="glyphicon glyphicon-user"></i>
                                        <?= $program_activities_one->admin_position ?> : <?= $program_activities_one->admin_name ?>
                                        
                              </h6>
                              <div class="row">
                                        <div class="col-md-8">
                                                  <?php if ($program_activities_one->pr_image != 'No image'): ?>
                                                                      <img class="img-responsive img-rounded-2px" src="<?= $program_activities_one->pr_image ?>">
                                                            <?php endif; ?>
                                                  <h6>Picture: <?= $program_activities_one->pr_image_credit ?></h6>
                                                  <div style="width: 100%; float: left;">
                                                            <div style="float: left;margin-right: 5px;width: 80px;">
                                                                      <div class="fb-share-button" data-href="<?= base_url('programactivities/view/'.$program_activities_one->pr_url) ?>" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?= base_url('programactivities/view/'.$program_activities_one->pr_url) ?>&amp;src=sdkpreparse">Share</a></div>
                                                            </div>
                                                            <div style="float: left;margin-right: 5px;width: 80px;">
                                                            <a href="<?= base_url('programactivities/view/'.$program_activities_one->pr_url) ?>" class="twitter-share-button" data-show-count="false">Tweet</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                                  
                                                            </div>
                                                            <div style="float: left;margin-right: 5px;width: 100px;">
                                                                      <!-- Place this tag where you want the share button to render. -->
                                                                      <div class="g-plus" data-action="share"></div>
                                                            </div>
                                                            
                                                  </div>
                                        </div>
                                        <?= form_open('booking_now/' . $program_activities_one->id); ?>
                                        <input type="hidden" name="qty" value="1">
                                        <div class="col-md-4">
                                                  <h6 class="h6-style-1">
                                                            <b>To arrange your booking choices, please click on the circles. Thank you.</b>
                                                  </h6>
                                                  <table class="table-price">
                                                            <tr>
                                                                      <th>Guest (s)</th>
                                                                      <th>Price</th>
                                                                      <th></th>
                                                            </tr>
                                                            <tr>
                                                                      <td><small class="atention-black">1 person</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_1, 2, ".", ","); ?> @person</small></td>
                                                                      <td>
                                                                                <input type="radio" id="no-person" name="person" value="1" class="no-trigger">

                                                                      </td>
                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">2 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_2, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="no-person" name="person" value="2" class="no-trigger"></td>
                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">3-4 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_3_4, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="person" name="person" value="3" class="trigger" data-rel="total_person" /></td>

                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">5-6 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_5_6, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="person" name="person" value="5" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">7-8 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_7_8, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="person" name="person" value="7" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">9-10 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_9_10, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="person" name="person" value="9" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td><small class="atention-black">11-20 people</small></td>
                                                                      <td><small class="atention-black">US$<?= number_format($program_activities_one->pr_price_11_20, 2, ".", ","); ?> @person</small></td>
                                                                      <td><input type="radio" id="person" name="person" value="11" class="trigger" data-rel="total_person" /></td>
                                                            </tr>


                                                  </table>
                                                  <?php if ($this->session->flashdata('Errorbooking')): ?>
                                                            <div class="row">
                                                                      <div class="col-md-12">
                                                                                <div class="alert alert-block alert-danger fade in">
                                                                                          <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                                                          <?= $this->session->flashdata('Errorbooking') ?>
                                                                                </div>
                                                                      </div>
                                                            </div>
                                                            <?php endif ?>
                                                  <div class="total_person konten"><input type="number" class="pesan" name="total_person"> total guest (s) to come</div>

                                                  <hr class="hr-booking">
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <button type="submit" name="add_basket"class="btn btn-primary btn-block">
                                                                                <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Booking
                                                                      </button>
                                                            </div>
                                                  </div>

                                        </div>
                                        <?= form_close() ?>
                              </div>
                              <hr>
                              <h4 class="h4-style-1">Description</h4>
                              <?= $program_activities_one->pr_description ?>
                              <div class="fb-comments" data-href="<?= base_url('programactivities/view/'.$program_activities_one->pr_url) ?>" data-width="100%"  data-numposts="5"></div>
                    </div>
                    
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-usd"></i> Currency Converter</h3>
                              <div class="col-md-12 box-side-inside">
                              <!-- Currency Converter Script - EXCHANGERATEWIDGET.COM -->
                                        <div style="width:100%;">
                                                  <script type="text/javascript" src="//www.exchangeratewidget.com/converter.php?l=en&f=USD&t=IDR&a=1&d=FFFFFF&n=FFFFFF&o=000000&v=1"></script>
                                        </div>
                             <!-- End of Currency Converter Script -->
                              </div>
                    </div> 
                    
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-link"></i> Other programs</h3>
                              <?php foreach ($program_activities_side as $value): ?>
                                                  <div class="col-md-12 box-side-inside">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->pr_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></h3>

                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details &raquo;</a></p>

                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>
                    <div class="col-md-4  box-side">
                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-plane"></i> Search ticket</h3>
                              <div class="tiket-root" data-widget="tiket-boxsearchwidget" data-businessid="21071732" data-lang="en" data-size_type="normal" data-width="350" data-height="600" data-position="sidebar-right" data-product_type="flight|hotel|train"></div><script type="text/javascript" src="http://www.tiket.com/js/new_widget/tiket_widgetframe_v3.js" async="true"></script>
                    </div>
          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    },5000);

          });
//-->
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });

          $('input#person').on('ifChanged', function () {
                    $('.konten').hide();
                    $('.' + $(this).data('rel')).show();
          });
          $('input#no-person').on('ifChanged', function () {
                    $('.konten').hide();
          });
</script>

<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>