<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <a href="<?= base_url('programactivities') ?>">Programs and activities > </a>
                    <?= $program_activities_one->pr_title ?>
          </div>
          <div class="row">
                    <div class="col-md-8">
                              <h3 class="h3-title-detail"><?= $program_activities_one->pr_title ?></h3>
                              <h6>
                                        <i class="glyphicon glyphicon-calendar"></i>
                                        Post date: <?= date('D j M Y', strtotime($program_activities_one->created_at)) ?> -
                                        <i class="glyphicon glyphicon-user"></i>
                                        <?= $program_activities_one->admin_position ?> : <?= $program_activities_one->admin_name ?>
                              </h6>
                              <div class="row">
                                        <div class="col-md-8">
                                                  <?php if ($program_activities_one->pr_image != 'No image'): ?>
                                                                      <img class="img-responsive" src="<?= $program_activities_one->pr_image ?>">
                                                            <?php endif; ?>
                                                  <h6>Picture: <?= $program_activities_one->pr_image_credit ?></h6>
                                        </div>
                                        <?= form_open('booking_now/' . $program_activities_one->id); ?>
                                        <div class="col-md-4">
                                                  <h6 class="h6-style-1">
                                                            <b>To arrange your booking choices, please click on the circles or squares below. Thank you.</b>
                                                  </h6>
                                                  <table class="table-price">
                                                            <tr>
                                                                      <th>Guest</th>
                                                                      <th>Price</th>
                                                                      <th></th>
                                                            </tr>
                                                            <tr>
                                                                      <td>1 person</td>
                                                                      <td> $<?= $program_activities_one->pr_price_1 ?> @person</td>
                                                                      <td>
                                                                                <input type="radio" id="no-person" name="person" value="1" class="no-trigger">

                                                                      </td>
                                                            </tr>

                                                            <tr>
                                                                      <td>2 people</td>
                                                                      <td>$<?= $program_activities_one->pr_price_2 ?> @person</td>
                                                                      <td><input type="radio" id="no-person" name="person" value="2" class="no-trigger"></td>
                                                            </tr>

                                                            <tr>
                                                                      <td>3-4 people</td>
                                                                      <td>$<?= $program_activities_one->pr_price_3_4 ?> @person</td>
                                                                      <td><input type="radio" id="person" name="person" value="3" class="trigger" data-rel="total_person" /></td>

                                                            </tr>

                                                            <tr>
                                                                      <td>5-6 people</td>
                                                                      <td>$<?= $program_activities_one->pr_price_5_6 ?> @person</td>
                                                                      <td><input type="radio" id="person" name="person" value="5" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td>7-8 people</td>
                                                                      <td>$<?= $program_activities_one->pr_price_7_8 ?> @person</td>
                                                                      <td><input type="radio" id="person" name="person" value="7" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td>9-10 people</td>
                                                                      <td>$ <?= $program_activities_one->pr_price_9_10 ?> @person</td>
                                                                      <td><input type="radio" id="person" name="person" value="9" class="trigger" data-rel="total_person" /></td>
                                                            </tr>

                                                            <tr>
                                                                      <td>11-20 people</td>
                                                                      <td>$<?= $program_activities_one->pr_price_11_20 ?> @person</td>
                                                                      <td><input type="radio" id="person" name="person" value="11" class="trigger" data-rel="total_person" /></td>
                                                            </tr>


                                                  </table>
                                                  <?php if ($this->session->flashdata('Errorbooking')): ?>
                                                                      <div class="col-md-12">
                                                                                <div class="alert alert-block alert-danger fade in">
                                                                                          <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                                                          <?= $this->session->flashdata('Errorbooking') ?>
                                                                                </div>
                                                                      </div>
                                                            <?php endif ?>
                                                  <div class="total_person konten"><input type="number" class="pesan" name="total_person"> total guest (s) to come</div>

                                                  <hr class="hr-booking">
                                                  <div class="row">
                                                            <div class="col-md-12 margin-bottom-10">
                                                                      <input type="checkbox" name="bnb" value="yes"> Bed and Breakfast (B&B)
                                                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right"
                                                                            title="B&B is an accomodation service, like rest room, lunch and other">
                                                                      </span>
                                                            </div>
                                                            <div class="col-md-12">
                                                                      <input type="checkbox" name="rent_a_car" value="yes"> Rent a car
                                                                      <span class="glyphicon glyphicon-question-sign" data-toggle="tooltip" data-placement="right"
                                                                            title="If you need a transportation to explore the lombok island, you can check this">
                                                                      </span>
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <button type="submit" name="add_basket"class="btn btn-primary btn-block">
                                                                                <span class="glyphicon glyphicon-hand-right" aria-hidden="true"></span> Booking
                                                                      </button>
                                                            </div>
                                                  </div>

                                        </div>
                                        <?= form_close() ?>
                              </div>
                              <hr>
                              <h4 class="h4-style-1">Description</h4>
                              <?= $program_activities_one->pr_description ?>

                    </div>

                    <div class="col-md-4">
                              <h3 class="h3-style-1"><i class="glyphicon glyphicon-tree-conifer"></i> Other programs</h3>
                              <?php foreach ($program_activities_side as $value): ?>
                                                  <div class="col-md-12 box-side">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->pr_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></h3>

                                                                                <p><a class="btn btn-default btn-xs" href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" role="button">More Details &raquo;</a></p>

                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>

                              <h3 class="h3-style-1 margin-top-15"><i class="glyphicon glyphicon-heart"></i> Recent Info Lombok</h3>
                              <?php foreach ($posts_side as $value): ?>
                                                  <div class="col-md-12 box-side">
                                                            <div class="row">
                                                                      <div class="col-md-4  img-read-side col-sm-4 col-xs-3">
                                                                                <img class="img-responsive" src="<?= $value->posts_image ?>">
                                                                      </div>
                                                                      <div class="col-md-8 col-sm-8 col-xs-9">
                                                                                <h3 class="sub-title-side"><a href="<?= base_url('posts/view/' . $value->posts_url) ?>"><?= $value->posts_title ?></a></h3>
                                                                                <h6 class="atention"><i class="glyphicon glyphicon-calendar"></i> Post date: <?= date('D j M Y', strtotime($value->created_at)) ?></h6>
                                                                      </div>
                                                            </div>
                                                  </div><!-- /.col-md-4 -->
                                        <?php endforeach; ?>
                    </div>


          </div>
</div>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>

<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(200, function () {
                                        $(this).remove();
                              });
                    }, 1000);

          });
//-->
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });

          $('input#person').on('ifChanged', function () {
                    $('.konten').hide();
                    $('.' + $(this).data('rel')).show();
          });
          $('input#no-person').on('ifChanged', function () {
                    $('.konten').hide();
          });
</script>

<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
</body>
</html>