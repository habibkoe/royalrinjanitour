<?php
echo "<script>
          tinymce.init({
                    selector: 'textarea#tinyEditor',
                    height: 350,
                    theme: 'modern',
                    relative_urls:false,
                    remove_script_host:false,
                    plugins: [
                              'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                              'searchreplace wordcount visualblocks visualchars code fullscreen',
                              'insertdatetime media nonbreaking save table contextmenu directionality',
                              'emoticons template paste textcolor colorpicker textpattern responsivefilemanager imagetools'
                    ],
                    toolbar1: 'insertfile undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                    toolbar2: '| responsivefilemanager | print preview media | forecolor backcolor emoticons',
                    image_advtab: true,
                    templates: [
                              {title: 'Test template 1', content: 'Test 1'},
                              {title: 'Test template 2', content: 'Test 2'}
                    ],
                    
                    external_filemanager_path: '".base_url()."/filemanager/',
                    filemanager_title: 'Responsive Filemanager',
                    external_plugins: {'filemanager': '".base_url()."/filemanager/plugin.min.js'}
          });
</script>";

?>