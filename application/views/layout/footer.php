<footer>
          <div class="container footer-main">
                    <div class="row">
                              <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4 class="h4-footer"><i class="glyphicon glyphicon-globe"></i> Follow Us</h4>
                                        <a href="<?= $web_setting->web_link_3 ?>" target="_blank"><img src="<?= base_url('assets/img/weebly.png') ?>"></a>
                                        <a href="<?= $web_setting->web_link_1 ?>" target="_blank"><img src="<?= base_url('assets/img/fb.png') ?>"></a>
                                        <a href="https://plus.google.com/112456124827476661105"  target="_blank"><img src="<?= base_url('assets/img/g+.png') ?>"></a>
                                        <a href="<?= $web_setting->web_link_2 ?>" target="_blank"><img src="<?= base_url('assets/img/youtube.png') ?>"</a>
                                        <a href="<?= $web_setting->web_maps ?>"><img src="<?= base_url('assets/img/logo-google-2.png') ?>"></a>
                                        
                                        <h4  class="h4-footer"><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Payment Method</h4>
                                        <img src="<?= base_url('assets/img/bri.jpg') ?>" height="30"> <img src="<?= base_url('assets/img/wu.jpg') ?>" height="30"> 
                                        <br>
                                        <br>
                                        <!-- Histats.com  (div with counter) --><div id="histats_counter"></div>
                                        <!-- Histats.com  START  (aync)-->
                                        <script type="text/javascript">var _Hasync= _Hasync|| [];
                                        _Hasync.push(['Histats.start', '1,3542839,4,1044,200,30,00011111']);
                                        _Hasync.push(['Histats.fasi', '1']);
                                        _Hasync.push(['Histats.track_hits', '']);
                                        (function() {
                                        var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
                                        hs.src = ('//s10.histats.com/js15_as.js');
                                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
                                        })();</script>
                                        <noscript><a href="/" target="_blank"><img  src="//sstatic1.histats.com/0.gif?3542839&101" alt="cool hit counter" border="0"></a></noscript>
                                        <!-- Histats.com  END  -->
                              </div>
                              <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4  class="h4-footer"><i class="glyphicon glyphicon-info-sign"></i> Information</h4>
                                        <?php foreach ($help_footer as $value): ?>
                                        <h5><a href="<?= base_url('help/'.$value->posts_url) ?>"><?= $value->posts_title ?></a></h5>
                                        <?php endforeach; ?>
                                        <h5><a href="<?= base_url('question-and-answer') ?>">Question and Answer</a></h5>
                                        <?php foreach ($other_footer as $value): ?>
                                        <h5><a href="<?= base_url('other/'.$value->posts_url) ?>"><?= $value->posts_title ?></a></h5>
                                        <?php endforeach; ?>
                                        <h5><a href="<?= base_url('about-us') ?>">About Us</a></h5>
                                        <h5><a href="<?= base_url('advertise') ?>">Advertise</a></h5>
                              </div>
                              
                              <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4  class="h4-footer"><i class="glyphicon glyphicon-plus-sign"></i> Contact Us</h4>
                                        <h5>24/7 Hours online</h5>
                                        <h4><i class="glyphicon glyphicon-earphone"></i> <?= $web_setting->web_call_center ?></h4>
                                        <h4><a href="royalrinjanitour@gmail.com" title="royalrinjanitour@gmail.com"><img src="<?= base_url('assets/img/Gmail.png') ?>"> royalrinjanitour@gmail.com</a></h4>
                                        <h4><img src="<?= base_url('assets/img/Gmail.png') ?>"> admin@royalrinjanitour.com</h4>
                                        
                              </div>
                              <div class="col-md-3 col-sm-6 col-xs-12">
                                        <h4  class="h4-footer"><i class="glyphicon glyphicon-star-empty"></i> Company Description</h4>
                                        <p><?= $web_setting->web_description ?></p>
                              </div>
                              
                    </div>
          </div>
          <div class="footer-bottom">
                    &copy; <?= date('Y') ?> Royal Rinjani Tour<br>
                    Powered by <a href="http://lombokinnovation.com">Lombok Innovation</a>
          </div>
</footer>