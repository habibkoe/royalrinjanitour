
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
          <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                    <title> <?= $app_name ?></title>
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/bootstrap.min.css" type="text/css">
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/css/login.css" type= "text/css">
                    <link rel="stylesheet" href="<?= base_url(); ?>assets/plugin/font-awesome/css/font-awesome.min.css" type= "text/css">
                    <link rel="icon" href="<?= base_url(); ?>assets/img/favicon-2.png">

          </head>