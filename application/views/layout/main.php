<!DOCTYPE html>
<html lang="en" class="no-js">
          <head>
                    <link rel="icon" href="<?= base_url('assets/img/favicon-2.png') ?>">
                    <title><?php if(isset($posts_one)){ echo $posts_one->posts_title;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_title;}else if(isset($bnb_one)){ echo $bnb_one->bnb_name;}else{echo $app_name;} ?></title>
                    
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    
                    <!-- Meta -->
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="description" content="<?php if(isset($posts_one)){ echo $posts_one->posts_short_description;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_short_description;}else if(isset($bnb_one)){ echo $bnb_one->bnb_short_description;}else{echo $web_setting->web_description;} ?>" itemprop="description">
                    <meta name="keywords" content="<?= $web_setting->web_meta ?>" itemprop="keywords">
                    <meta name="author" content="<?= $web_setting->web_author ?>">
                    <meta name="copyright" content="royalrinjanitour" itemprop="dateline" />
                    <meta name="thumbnailUrl" content="<?php if(isset($posts_one)){ echo $posts_one->posts_image;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_image;}else if(isset($bnb_one)){ echo $bnb_one->bnb_image;}else{echo 'http://royalrinjanitour.com/source/P%26A-M.%20Rinjani-en.wikipedia.org.jpg';} ?>" itemprop="thumbnailUrl" />
                    <meta content="<?php if(isset($posts_one)){ echo base_url('posts/view/'.$posts_one->posts_url);}else if(isset($program_activities_one)){ echo base_url('programactivities/view/'.$program_activities_one->pr_url);}else if(isset($bnb_one)){ echo base_url('bnb/view/'.$bnb_one->bnb_url);}else{echo 'http://royalrinjanitour.com/';} ?>" itemprop="url" />
                    
                    <!-- Properti Facebook -->
                    <meta property="og:title" content="<?php if(isset($posts_one)){ echo $posts_one->posts_title;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_title;}else if(isset($bnb_one)){ echo $bnb_one->bnb_name;}else{echo $web_setting->web_title;} ?>"/>
                    <meta property="og:site_name" content="<?= $web_setting->web_author ?>"/>
                    <meta property="og:image" content="<?php if(isset($posts_one)){ echo $posts_one->posts_image;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_image;}else if(isset($bnb_one)){ echo $bnb_one->bnb_image;}else{echo 'http://royalrinjanitour.com/source/P%26A-M.%20Rinjani-en.wikipedia.org.jpg';} ?>" />
                    <meta property="og:url" content="<?php if(isset($posts_one)){ echo base_url('posts/view/'.$posts_one->posts_url);}else if(isset($program_activities_one)){ echo base_url('programactivities/view/'.$program_activities_one->pr_url);}else if(isset($bnb_one)){ echo base_url('bnb/view/'.$bnb_one->bnb_url);}else{echo 'http://royalrinjanitour.com/';} ?>" />
                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:width" content="500" />
                    <meta property="og:image:height" content="300" />
                    <meta property="og:description" content="<?php if(isset($posts_one)){ echo $posts_one->posts_short_description;}else if(isset($program_activities_one)){ echo $program_activities_one->pr_short_description;}else if(isset($bnb_one)){ echo $bnb_one->bnb_short_description;}else{echo $web_setting->web_description;} ?>"/>
                    
                    <!-- CSS -->
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/main.css'); ?>">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/icheck/square/_all.css') ?>">
                    <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/sweet-allert/sweetalert2.min.css') ?>">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/select2/select2.min.css') ?>">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/plugin/font-awesome/css/font-awesome.min.css'); ?>">
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/parsley.css')?>">
                    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/7.3/styles/github.min.css">
                    
                    <!--Javascript Head-->
                    <script type="text/javscript" src="<?= base_url('assets/js/modernizr-custom.js'); ?>"></script>
                    <script type="text/javascript">
                              var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                              (function () {
                                        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                                        s1.async = true;
                                        s1.src = 'https://embed.tawk.to/575e268addb549146fc89e61/default';
                                        s1.charset = 'UTF-8';
                                        s1.setAttribute('crossorigin', '*');
                                        s0.parentNode.insertBefore(s1, s0);
                              })();
                    </script>
                    <!--End of Tawk.to Script-->
          </head>
          <body>
                    <nav class="navbar navbar-default  navbar-static-top nav-top">
                              <div class="container">
                                        <ul class="nav navbar-nav nav-custom-top navbar-right">
                                                  <li><a><i class="glyphicon glyphicon-earphone"></i> +62 818 0579 1762</a></li>
                                                  <li><a href="https://gmail.com" title="royalrinjanitour@gmail.com"><img src="<?= base_url('assets/img/Gmail.png') ?>"></a></li>
                                                  <li><a href="<?= $web_setting->web_link_3 ?>" target="_blank"><img src="<?= base_url('assets/img/weebly.png') ?>"></a></li>
                                                  <li><a href="<?= $web_setting->web_link_1 ?>" target="_blank"><img src="<?= base_url('assets/img/fb.png') ?>"></a></li>
                                                  <li><a href="https://plus.google.com/112456124827476661105"  target="_blank"><img src="<?= base_url('assets/img/g+.png') ?>"></a></li>
                                                  <li><a href="<?= $web_setting->web_link_2 ?>"><img src="<?= base_url('assets/img/youtube.png') ?>"></a></li>
                                                  
                                        </ul>
                              </div>
                    </nav>


                    <nav class="navbar navbar-default navbar-static-top nav-custom">
                              <div class="container">
                                        <div class="navbar-header">
                                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                  </button>
                                                  <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url('assets/img/logo-6.png') ?>"></a>
                                        </div>
                                        <div id="navbar" class="navbar-collapse collapse">
                                                  <ul class="nav navbar-nav">
                                                            <li><a href="<?= base_url('programactivities') ?>" title="Booking Your Trekking Package">Programs</a></li>
                                                            <li><a href="<?= base_url('bed-and-breakfast') ?>" title="Booking Your Rest Place or Rent Car">BnB</a></li>
                                                            <li><a href="<?= base_url('info') ?>" title="Info About Lombok">Info Lombok</a></li>
                                                            <?php if($this->session->userdata('customerin')== FALSE): ?>
                                                            <li><a href="#" data-toggle="modal" data-target="#myModal" title="See and Confirm Your Booking">Confirm</a></li>
                                                            <?php else: ?>
                                                            <li><a href="<?= base_url('transaction') ?>" title="<?= $this->session->userdata('or_full_name') ?>"><?= $this->session->userdata('or_full_name') ?></a></li>
                                                            <?php endif; ?>
                                                            <li><a href="<?= base_url('detail_booking') ?>"><i class="glyphicon glyphicon-shopping-cart"></i> Item(s) <?= $this->cart->total_items() ?></a></li>
                                                  </ul>
                                                  
                                        </div><!--/.nav-collapse -->
                              </div>
                    </nav>

                    <!-- Modal Login -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                              <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                                  <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            <h4 class="modal-title" id="myModalLabel">Customer Login</h4>
                                                  </div>
                                                  <?= form_open('login_customer') ?>
                                                  <div class="modal-body">
                                                            <div class="form-group has-feedback">
                                                                      <label for="invoice">Email</label>
                                                                      <input type="email" name="or_email" class="form-control" id="invoice" placeholder="Email" required="">
                                                                      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                                            </div>
                                                            <div class="form-group has-feedback">
                                                                      <label for="nope">Booking Number</label>
                                                                      <input type="number" name="or_booking_number" class="form-control" id="nope" placeholder="Booking Number" required="">
                                                                      <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                                            </div>

                                                  </div>
                                                  <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger">Come in</button>
                                                  </div>
                                                  <?= form_close() ?>
                                        </div>
                              </div>
                    </div>