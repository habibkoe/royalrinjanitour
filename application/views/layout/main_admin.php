<!DOCTYPE html>
<html lang="en"  class="no-js">
          <head>
                    <link rel="icon" href="<?= base_url('assets/img/favicon-2.png') ?>">
                    <title>Admin Royal Rinjani Tour</title>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    
                    <!-- Meta -->
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="description" content="">
                    <meta name="author" content="Lombok Innovation | Habib Koe Tkj">
                    
                    <!--  CSS -->
                    <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>">
                    <link rel="stylesheet" href="<?= base_url('assets/plugin/fancybox/source/jquery.fancybox.css'); ?>">
                    <link rel="stylesheet" href="<?= base_url('assets/css/dashboard.css'); ?>">
                    <link rel="stylesheet" href="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.css') ?>">
                    <link rel="stylesheet" href="<?= base_url('assets/plugin/font-awesome/css/font-awesome.min.css'); ?>">
                    
                    <!--JS-->
                    <script type="text/javscript" src="<?= base_url('assets/js/modernizr-custom.js'); ?>"></script>
          </head>

          <body>
                    <nav class="navbar navbar-inverse navbar-fixed-top">
                              <div class="container-fluid">
                                        <div class="navbar-header">
                                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                  </button>
                                                  <a class="navbar-brand" href="#">Royal Rinjani Tour Admin</a>
                                        </div>
                                        <div id="navbar" class="navbar-collapse collapse">
                                                  <ul class="nav navbar-nav navbar-right">
                                                            
                                                            <li><a href="<?= base_url('backend/dashboard/setting/1') ?>"><i class="glyphicon glyphicon-cog"></i>  Settings</a></li>
                                                            <li><a href="#"><?= $this->session->userdata('admin_name'); ?></a></li>
                                                            <li><a href="#"><i class="glyphicon glyphicon-info-sign"></i> Help</a></li>
                                                            <li><a href="<?= base_url('logout'); ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
                                                  </ul>
                                                  <form class="navbar-form navbar-right">
                                                            <div class="input-group">
                                                                      <span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>
                                                                      <input type="search" class="form-control" placeholder="Find data here.........">
                                                            </div>
                                                  </form>
                                        </div>
                              </div>
                    </nav>