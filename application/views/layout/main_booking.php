<!DOCTYPE html>
<html lang="en">
          <head>
                    <meta charset="utf-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="description" content="<?= $web_setting->web_description ?>">
                    <meta name="keywords" content="<?= $web_setting->web_meta ?>">
                    <meta name="author" content="<?= $web_setting->web_author ?>">
                    <meta property="og:title" content="<?= $web_setting->web_title ?>"/>
                    <meta property="og:site_name" content="<?= $web_setting->web_author ?>"/>
                    <meta property="og:description" content="<?= $web_setting->web_description ?>"/>
                    <title><?= $app_name ?></title>
                    <!-- Bootstrap core CSS -->
                    <link href="<?= base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
                    <!-- Custom styles for this template -->
                    <link href="<?= base_url('assets/css/main.css'); ?>" rel="stylesheet">
                    <link rel="icon" href="<?= base_url('assets/img/favicon-2.png') ?>">
                    <link href="<?= base_url('assets/plugin/icheck/square/_all.css') ?>" rel="stylesheet">
          </head>
          <body>
                    <nav class="navbar navbar-default navbar-static-top nav-booking">
                              <div class="container">
                                        <div class="navbar-header">
                                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                                            <span class="sr-only">Toggle navigation</span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                            <span class="icon-bar"></span>
                                                  </button>
                                                  <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url('assets/img/logo-5.png') ?>"></a>
                                        </div>
                                        <div id="navbar" class="navbar-collapse collapse">
                                                  <ul class="nav navbar-nav">
                                                            <li><a href="<?= base_url('programactivities') ?>" title="Programs and Activities">Programs</a></li>
                                                            <li><a href="<?= base_url('bb_rent_a_car') ?>" title="Bed and Breakfast-Rent a car">B&B-Rent a Car</a></li>
                                                            <li><a href="<?= base_url('about_us') ?>" title="About Royal Rinjani Tour">About Us</a></li>
                                                            <li><a href="<?= base_url('info') ?>" title="Info About Lombok">Info Lombok</a></li>
                                                            <li><a href="<?= base_url('confirm') ?>" title="Confirm your booking">Confirm booking</a></li>
                                                            <li><a href="<?= base_url('booking_include_all') ?>"><i class="glyphicon glyphicon-shopping-cart"></i> <?= $this->cart->total_items() ?></a></li>
                                                  </ul>
                                        </div><!--/.nav-collapse -->
                              </div>
                    </nav>