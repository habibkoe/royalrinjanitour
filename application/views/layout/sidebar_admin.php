<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-3 col-md-2 sidebar">
                              <ul class="nav nav-sidebar">
                                        <li <?php if($page_base=='Dashboard'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/dashboard'); ?>">
                                                            <i class="glyphicon glyphicon-dashboard"></i>  Dashboard
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Attribut'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/attribut'); ?>">
                                                            <i class="glyphicon glyphicon-bookmark"></i>  Attribut
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Booking'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/booking'); ?>">
                                                            <i  class="glyphicon glyphicon-book"></i>  Booking
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Programs'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/programactivities'); ?>">
                                                            <i class="glyphicon glyphicon-list"></i>  Programs
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Bed and Breakfast'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/bnb') ?>">
                                                            <i class="glyphicon glyphicon-cutlery"></i>  Bed and Breakfast
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Rent a Car'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/rac') ?>">
                                                            <i class="fa fa-car" aria-hidden="true"></i>  Rent a car
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Post'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/posts'); ?>">
                                                            <i class="glyphicon glyphicon-text-background"></i>  Posts
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Slider'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/sliders'); ?>">
                                                            <i  class="glyphicon glyphicon-upload"></i>  Sliders
                                                  </a>
                                        </li>
                                        <li <?php if($page_base=='Visitor'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/users'); ?>">
                                                            <i class="fa fa-users" aria-hidden="true"></i>  Tracking Visitor
                                                  </a>
                                        </li>
                                        <?php if($this->session->userdata('admin_level')=='admin'): ?>
                                        <li <?php if($page_base=='User'){ echo "class='active'"; } ?>>
                                                  <a href="<?= base_url('backend/users'); ?>">
                                                            <i  class="glyphicon glyphicon-user"></i>  User
                                                  </a>
                                        </li>
                                        <?php endif; ?>
                                        
                              </ul>
                              <ul class="nav nav-sidebar">
                    <li>
                              <a href="<?=base_url()?>" target="_blank"><i class="glyphicon glyphicon-blackboard"></i> View Home Page</a>
                    </li>
          </ul>
                    </div>
