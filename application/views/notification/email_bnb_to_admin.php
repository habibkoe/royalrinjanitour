<html>
          <head>
                    <meta charset="utf-8" />
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <title>Your Details Booking At Royal Rinjani Tour</title>
                    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
          </head>
          
          <body style="background: #D0D0D0;font-family: 'Oswald', sans-serif;padding-top: 50px;padding-bottom: 50px; font-weight: normal;">
                    <div style="min-height: 400px;width: 80%;float: left;margin-left: 5%;margin-right: 5%;background: #fff;padding: 5%;border-top: #CB0011 3px dashed;border-radius: 10px;">
                              
                              <div style="float: left; width: 100%;">
                                        <h2 style="color: #CB0011;margin-bottom: 10px;font-size: 20px;margin-top: 10px; text-align: center;">Royal Rinjani Tour And B&B Sembalun Lodge. </h2>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;">Sembalun Lawang Village, East Lombok 83666, Nusa Tenggara Barat, Indonesia.</h5>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;"> Mobile Phone Number: +6281805791762.</h5>
                              </div>
                              <hr style="width: 100%;float: left;">
                              <div style="width: 100%;float: left;">
                               
                               <div style="width: 100%">
                                         <h1 style="text-align: center;font-weight: bold;">TOTAL  AMOUNT  TO BE PAID US$<?= number_format($or_price, 2, ".", ","); ?></h1>
                                         
                                         
                                         
                                         <h3 style="color: #CB0011;">DETAILS:</h3>
                                         <table style="width: 100%">
                                                   <tr><td colspan="3" style="background: #008000;color: #fff;padding: 10px;text-align: center;">Customer’s Booking Number: <?= $id ?></td></tr>
                                                  
                                                  <tr>
                                                            <td>Name</td><td>:</td><td><?= $or_full_name ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Mobile Phone Number</td><td>:</td><td><?= $or_phone ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Land Line Phone Number</td><td>:</td><td><?= $or_home_phone ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Email</td><td>:</td><td><?= $or_email ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Address</td><td>:</td><td><?= $or_address ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Check in date</td><td>:</td><td><?= $or_checkin_date ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Check out date</td><td>:</td><td><?= $or_checkout_date ?></td>
                                                            
                                                  </tr>
                                                  
                                                  <tr>
                                                            <td>Guest(s) name(s)</td><td>:</td><td><?= $or_guest_name ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Allergies, Special Diets Or Vegetarian</td><td>:</td><td><?= $or_special_diets ?></td>
                                                            
                                                  </tr>
                                                  
                                                  <tr>
                                                            <td>Special request</td><td>:</td><td><?= $or_message?></td>
                                                            
                                                  </tr>
                                         
                                         </table>
                                         <hr>
                                         <h4>Detail Bed and Breakfast</h4>
                                         <table style="width: 100%;">
                                                   <tr style="background: #008000;color: #fff;">
                                                             <th style="padding: 10px;">B&B Name </th>
                                                             <th style="padding: 10px;"><span style="float: right;">US$</span></th>
                                                   </tr>
                                                   <?php $this->load->library('cart'); ?>
                                                   <?php foreach ($this->cart->contents() as $items): ?>
                                                                                <?php
                                                                                foreach ($bnb as $sp) :
                                                                                          if ($sp->id == $items['id']) {
                                                                                                    ?>
                                                                                                    <tr>
                                                                                                              <td>
                                                                                                                        <strong><?= $sp->bnb_name ?></strong>
                                                                                                                        <p><?= $items['qty']; ?> <?= $sp->bnb_type ?>.<br>
                                                                                                                                  Capacity: <?= $sp->bnb_capacity ?> people<br>
                                                                                                                                  US$ <?= number_format($items['price'], 2, ".", ","); ?>
                                                                                                                        </p>

                                                                                                              </td>

                                                                                                              <td>
                                                                                                                        <span class="pull-right"><?= number_format($items['price'], 2, ".", ","); ?></span>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                              <td colspan="2">
                                                                                                                        <p class="price-includes">
                                                                                                                                  <strong>Price includes:</strong><br>
                                                                                                                                  <?= $sp->bnb_short_description ?>
                                                                                                                        </p>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                          <?php } endforeach; ?>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                                                                          <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                                    <tr>
                                                                                                              <td><?= $option_name; ?></td><td><?= $option_value ?></td><td></td>
                                                                                                    </tr>          

                                                                                          <?php endforeach; ?>

                                                                                <?php endif; ?>
                                                                                
                                                                      <?php endforeach; ?>
                                                  <tr>
                                                            <td style="color: red;padding: 10px;">TOTAL  AMOUNT  TO BE PAID</td><td style="background: red;padding: 10px;color: #fff;">US$ <?= number_format($or_price, 2, ".", ","); ?></td>
                                                  </tr>
                                        </table>
                                         <p>
                                                   I acknowledge that I have read, understand, and agree to Royal Rinjani Tour and Bed and Breakfast Sembalun Lodge Terms and Conditions
                                         </p>
                                         
                               </div>
                              </div>
                    </div>
                    <div class="footer">
                              <h5 style="color: #000;text-align: center;"><a href="http://royalrinjanitour.com">royalrinjanitour.com</a> Powered by <a href="http://lombokinnovation.com">Lombok Innovation</a></h5>
                    </div>
                    
                    
          </body>
</html>