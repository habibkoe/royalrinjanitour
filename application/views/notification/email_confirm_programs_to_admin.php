<html>
          <head>
                    <meta charset="utf-8" />
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <title>Royal Rinjani Tour And B&B Sembalun Lodge.</title>
                    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
          </head>
          
          <body style="background: #D0D0D0;font-family: 'Oswald', sans-serif;padding-top: 50px;padding-bottom: 50px;font-weight: normal;">
                    <div style="min-height: 400px;width: 80%;float: left;margin-left: 5%;margin-right: 5%;background: #fff;padding: 5%;border-top: #CB0011 3px dashed;border-radius: 10px;">
                              
                              <div style="float: left; width: 100%;">
                                        <h2 style="color: #CB0011;margin-bottom: 10px;font-size: 20px;margin-top: 10px; text-align: center;">Royal Rinjani Tour And B&B Sembalun Lodge. </h2>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;">Sembalun Lawang Village, East Lombok 83666, Nusa Tenggara Barat, Indonesia.</h5>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;"> Mobile Phone Number: +6281805791762.</h5>
                              </div>
                              <hr style="width: 100%;float: left;">
                              <div style="width: 100%;float: left;">
                               
                               <div style="width: 100%">
                                         <h1 style="text-align: center;font-weight: bold;">PAID  US$<?= number_format($bc_total_paid, 2, ".", ","); ?></h1>
                                         <h3 style="color: #CB0011;">DETAILS:</h3>
                                         <table style="width: 100%">
                                                   <tr><td colspan="3" style="background: #008000;color: #fff;padding: 10px;text-align: center;">Customer’s Booking Number: <?= $id ?></td></tr>
                                                  <tr>
                                                            <td>Bank Account</td><td>:</td><td><?= $bc_bank_account ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Bank Name</td><td>:</td><td><?= $bc_bank ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Name</td><td>:</td><td><?= $or_full_name ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Mobile Phone Number</td><td>:</td><td><?= $or_phone ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Email</td><td>:</td><td><?= $or_email ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Arrival date</td><td>:</td><td><?= $or_arrival_date ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Drop off to</td><td>:</td><td><?= $or_drop_off_to ?></td>
                                                            
                                                  </tr>
                                         
                                         </table>
                                         <p>
                                                   I acknowledge that I have read, understand, and agree to Royal Rinjani Tour and Bed and Breakfast Sembalun Lodge Terms and Conditions
                                         </p>
                                         
                               </div>
                              </div>
                    </div>
                    <div class="footer">
                              <h5 style="color: #000;text-align: center;"><a href="http://royalrinjanitour.com">royalrinjanitour.com</a> Powered by <a href="http://lombokinnovation.com">Lombok Innovation</a></h5>
                    </div>
                    
                    
          </body>
</html>