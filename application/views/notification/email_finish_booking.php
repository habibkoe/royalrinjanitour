<html>
          <head>
                    <meta charset="utf-8" />
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <title>Your Details Booking At Royal Rinjani Tour</title>
                    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
          </head>
          
          <body style="background: #ccc;font-family: 'Oswald', sans-serif;padding: 50px;">
                    <div style="min-height: 400px;width: 90%;margin-left: auto;margin-right: auto;background: #fff;padding: 40px;border-top: #2AA19C 3px dashed;border-radius: 10px;">
                              <h1 style="color: #2AA19C;margin-bottom: 10px;font-size: 40px;margin-top: 10px;">Royal Rinjani Tour</h1>
                              <h4 style="margin-top: 10px;">Booking Number.  #<?= $id ?></h4>
                              <hr>
                              <p>
                                        Dear <?= $or_full_name ?>,<br>
                                        Thank you for your order, your detail payment will show at bottom:
                              
                               </p>
                               <h3 style="text-align: center;padding: 15px;border: 1px solid #2AA19C;">Senin Tanggal 18 April 2016</h3>
                               
                               <div style="width: 100%">
                                         <h5 style="text-align: center;">Please transfer:</h5>
                                         <h1 style="text-align: center;font-weight: bold;">US$ <?= number_format($or_price, 2, ".", ","); ?></h1>
                                         <p style="text-align: center;">
                                                   TEPAT hingga 2 digit terakhir<br>
                                                   Perbedaan nilai transfer akan menghambat proses verifikasi<br>
                                                   
                                         </p>
                                         <h4 style="text-align: center;">Pembayaran dapat dilakukan ke salah satu nomor rekening a/n Royal Sembahulun:</h4>
                                         <p>
                                                   Transaksi dianggap batal jika sampai dengan Senin Tanggal <?= $or_due_date ?> (1 × 12 jam) pembayaran belum dilunasi. 
                                                   Kami akan menghubungi anda 3 jam sebelum waktu pembayaran habis, untuk melakukan konfirmasi apakah anda akan melanjutkan pembayaran atau tidak, terimakasih telah berbelanja di auratanilombok.com ^_^
                                         </p>
                                         <h3 style="color: dodgerblue;">Berikut adalah rincian tagihan pembayaran:</h3>
                                         <h4>Detail Transaksi</h4>
                                         <table style="width: 100%">
                                                   <tr><td colspan="3" style="background: #2AA19C;color: #fff;padding: 10px;text-align: center;">Your Booking Number. #<?= $id ?></td></tr>
                                                  <tr>
                                                            <td>Date Expired</td><td>:</td><td><?= $or_due_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Name</td><td>:</td><td><?= $or_full_name ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Phone</td><td>:</td><td><?= $or_phone ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Home phone</td><td>:</td><td><?= $or_home_phone ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Email</td><td>:</td><td><?= $or_email ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Address</td><td>:</td><td><?= $or_address ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Departure date</td><td>:</td><td><?= $or_departure_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Arrival date</td><td>:</td><td><?= $or_arrival_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Pick-up from</td><td>:</td><td><?= $or_pick_up_from ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Drop off to</td><td>:</td><td><?= $or_drop_off_to ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Checkin date</td><td>:</td><td><?= $or_checkin_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Checkout date</td><td>:</td><td><?= $or_checkout_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Night(s)</td><td>:</td><td><?= $or_long_night ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Type</td><td>:</td><td><?= $or_room_type ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Rent a car</td><td>:</td><td><?= $or_rent_a_car ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Star date</td><td>:</td><td><?= $or_rent_start_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>End date</td><td>:</td><td><?= $or_rent_end_date ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Preffered car</td><td>:</td><td><?= $or_prefered_car ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Payment method</td><td>:</td><td><?= $or_payment_method ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Total member</td><td>:</td><td><?= $or_total_member ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Relevant medical conditions</td><td>:</td><td><?= $or_medical_condition ?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Allergies, Special Diets Or Vegetarian.?</td><td>:</td><td><?= $or_special_diets ?></td>
                                                  </tr>
                                                  
                                                  <tr>
                                                            <td>Special request</td><td>:</td><td><?= $or_message?></td>
                                                  </tr>
                                                  <tr>
                                                            <td>Total price</td><td>:</td><td><?= number_format($or_price, 2, ".", ","); ?></td>
                                                  </tr>
                                         
                                         </table>
                                         <hr>
                                         <h4>Detail Programs</h4>
                                         <table style="width: 100%;">
                                                   <tr style="background: #2AA19C;color: #fff;"><td style="padding: 10px;">Program name</td><td style="padding: 10px;">Guest(s)</td><td style="padding: 10px;">Price</td></tr>
                                                   
                                                   <?php foreach ($this->cart->contents() as $items): ?>
                                                            <tr>
                                                                      <td><?= $items['name'] ?></td><td><?= $items['qty'] ?></td><td>US$ <?= number_format($items['price'], 2, ".", ","); ?></td>
                                                            </tr>
                                                   <?php  endforeach;?>
                                                  <tr>
                                                            <td colspan="2" style="color: orange;padding: 10px;">Total payment</td><td style="background: orange;padding: 10px;color: #fff;">US$ <?= number_format($or_price, 2, ".", ","); ?></td>
                                                  </tr>
                                        </table>
                                         <p style="border-top: 1px solid orange;border-bottom: 1px solid orange;padding: 10px;">
                                                   Mohon bayar TEPAT hingga 2 digit terakhir.
                                         </p>
                                         <p style="padding: 10px;">
                                                   Alamat tujuan pengiriman<br>
                                                   muhammad habiburrahman<br>
                                                   jalan jagir sidomukti 1 buntu no 14 surabaya<br>
                                                   Wonokromo Surabaya<br>
                                                   Jawa Timur – 60244<br>
                                                   No. Telp: 081703712575<br>
                                         </p>
                               </div>
                               
                    </div>
                    <div class="footer">
                              <h5 style="color: #000;text-align: center;"><a href="http://royalrinjanitour.com">royalrinjanitour.com</a></h5>
                    </div>
                    
                    
          </body>
</html>