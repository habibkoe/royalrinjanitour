<html>
          <head>
                    <meta charset="utf-8" />
                    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                    <title>Royal Rinjani Tour And B&B Sembalun Lodge.</title>
                    <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
          </head>
          
          <body style="background: #D0D0D0;font-family: 'Oswald', sans-serif;padding: 50px; font-weight: normal;" onload="window.print();">
                    <div style="min-height: 400px;width: 94%;float: left;margin-left: auto;margin-right: auto;background: #fff;padding: 40px;border-top: #CB0011 3px dashed;border-radius: 10px;">
                              <div style="float: left; width: 20%;">
                                        <img src="<?= base_url('assets/img/logo1.PNG')?>" style="float: left;margin-right: 10px;"> 
                              </div>
                              <div style="float: left; width: 60%;">
                                        <h2 style="color: #CB0011;margin-bottom: 10px;font-size: 30px;margin-top: 10px; text-align: center;">Royal Rinjani Tour And B&B Sembalun Lodge. </h2>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;">Sembalun Lawang Village, East Lombok 83666, Nusa Tenggara Barat, Indonesia.</h5>
                                        <h5 style="text-align: center; margin-top: 5px; margin-bottom: 5px;"> Mobile Phone Number: +6281805791762.</h5>
                              </div>
                              <div style="float: left; width: 20%;">
                                        <img src="<?= base_url('assets/img/logo_sembalun_lodge.JPG')?>" style="float: right;margin-right: 10px;">
                              </div>
                              <hr style="width: 100%;float: left;">
                              <div style="width: 100%;float: left;">
                              <h4 style="margin-top: 10px;">Customer’s Booking Number: <?= $id ?></h4>
                              
                              <p>
                                        Dear <?= $or_full_name ?>,<br><br>
                                        
                                        Thank you for your booking(s) on <?= $or_date ?> with Royal Rinjani Tour and B&B Sembalun Lodge.
                                        
                               </p>
                               
                               <div style="width: 100%">
                                         <h1 style="text-align: center;font-weight: bold;">TOTAL  AMOUNT  TO BE PAID US$<?= number_format($or_price, 2, ".", ","); ?></h1>
                                         
                                         <p>
                                                   PAYMENT DETAILS:<br>
                                                   <a href="http://royalrinjanitour.com/other/terms-and-conditions">Terms & Conditions</a>: There are 2 types of booking systems, and we make no service charge for your booking:<br>
                                                   • Booking online or by email, before 1 week or more, prior to your departure date.<br>
                                                   • Late booking online or by email, less than 3 days prior to your departure date.<br>

                                                   To proceed with your booking: <br>
                                                   1. If your departure date is 1 week or more, first non-refundable payment of 50% of the total should reach us in 1 week or 7 working days from now.<br>
                                                   2. If your departure date is 3 days or less, we will require full non-refundable payment when you make your late booking.<br>
                                                   3. We will send you an email to remind you. <br>
                                                   4. If the payment is not received on the 7th or 3rd working day, your booking process will be deleted automatically. <br>
                                         </p>
                                         
                                         <h3 style="color: #CB0011;">DETAILS:</h3>
                                         <table style="width: 100%">
                                                   <tr><td colspan="3" style="background: #008000;color: #fff;padding: 10px;text-align: center;">Customer’s Booking Number: <?= $id ?></td></tr>
                                                  
                                                  <tr>
                                                            <td>Name</td><td>:</td><td><?= $or_full_name ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Mobile Phone Number</td><td>:</td><td><?= $or_phone ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Land Line Phone Number</td><td>:</td><td><?= $or_home_phone ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Email</td><td>:</td><td><?= $or_email ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Address</td><td>:</td><td><?= $or_address ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Check in date</td><td>:</td><td><?= $or_checkin_date ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Check out date</td><td>:</td><td><?= $or_checkout_date ?></td>
                                                            
                                                  </tr>
                                                  
                                                  <tr>
                                                            <td>Guest(s) name(s)</td><td>:</td><td><?= $or_guest_name ?></td>
                                                            
                                                  </tr>
                                                  <tr>
                                                            <td>Allergies, Special Diets Or Vegetarian</td><td>:</td><td><?= $or_special_diets ?></td>
                                                            
                                                  </tr>
                                                  
                                                  <tr>
                                                            <td>Special request</td><td>:</td><td><?= $or_message?></td>
                                                            
                                                  </tr>
                                         
                                         </table>
                                         <hr>
                                         <h4>Detail Bed and Breakfast</h4>
                                         <table style="width: 100%;">
                                                   <tr style="background: #008000;color: #fff;">
                                                             <th colspan="2" style="padding: 10px;">B&B Name </th>
                                                             <th style="padding: 10px;"><span style="float: right;">US$</span></th>
                                                   </tr>
                                                   <?php $this->load->library('cart'); ?>
                                                   <?php foreach ($this->cart->contents() as $items): ?>
                                                                                <?php
                                                                                foreach ($bnb as $sp) :
                                                                                          if ($sp->prs_id == $items['id']) {
                                                                                                    ?>
                                                                                                    <tr>

                                                                                                              <td><img src="<?= $sp->posts_image ?>" width="70"></td>
                                                                                                              <td>
                                                                                                                        <?= $sp->posts_title ?>
                                                                                                                        <p><?= $items['qty']; ?> <?= $sp->prs_type ?>.<br>
                                                                                                                                  Capacity: <?= $sp->prs_capacity ?> people<br>
                                                                                                                                  US$ <?= number_format($items['price'], 2, ".", ","); ?>
                                                                                                                        </p>

                                                                                                              </td>

                                                                                                              <td>
                                                                                                                        <span class="pull-right"><?= number_format($items['price'], 2, ".", ","); ?></span>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                              <td colspan="3">
                                                                                                                        <p class="price-includes">
                                                                                                                                  <strong>Price includes:</strong><br>
                                                                                                                                  <?= $sp->posts_short_description ?>
                                                                                                                        </p>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                          <?php } endforeach; ?>
                                                                                <?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

                                                                                          <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>
                                                                                                    <tr>
                                                                                                              <td><?= $option_name; ?></td><td><?= $option_value ?></td><td></td>
                                                                                                    </tr>          

                                                                                          <?php endforeach; ?>

                                                                                <?php endif; ?>
                                                                                
                                                                      <?php endforeach; ?>
                                                  <tr>
                                                            <td colspan="2" style="color: red;padding: 10px;">TOTAL  AMOUNT  TO BE PAID</td><td style="background: red;padding: 10px;color: #fff;">US$ <?= number_format($or_price, 2, ".", ","); ?></td>
                                                  </tr>
                                        </table>
                                         <p>
                                                   I acknowledge that I have read, understand, and agree to Royal Rinjani Tour and Bed and Breakfast Sembalun Lodge Terms and Conditions
                                         </p>
                                         <p style="padding: 10px;">
                                                   PAYMENT METHODS:<br>
                                                   1. Bank Transfer: PT. Bank Rakyat Indonesia.<br>
                                                   Branch Address: Jl. Pejagik No.16, Mataram, Nusa Tenggara Barat, Indonesia.<br>
                                                   Office: +62212510244.<br>
                                                   Branch: Mataram. Account number 759801003465538. Name: Royal Sebahulun.<br>
                                                   Swift code (BIC): BRINIJA. National code bank: 0020307.<br>
                                                   2. Western Union. Recipient: Royal Sembahulun.<br>
                                                   3. PayPal.<br>
                                         </p>
                               </div>
                             
                              <p style="text-align: center;">
                                        We look forward to welcoming you, and THANK YOU for choosing Royal Rinjani Tour and B&B Sembalun Lodge.
                              </p>
                              </div>
                    </div>
                    <div class="footer">
                              <h5 style="color: #000;text-align: center;"><a href="http://royalrinjanitour.com">royalrinjanitour.com</a> Powered by <a href="http://lombokinnovation.com">Lombok Innovation</a></h5>
                    </div>
                    
                    
          </body>
</html>