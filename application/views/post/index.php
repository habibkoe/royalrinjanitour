<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/posts/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New Post</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-text-background"></i> <?= $page_name ?></span>
                              </h2>

                              <table class="table table-striped table-bordered table-hover" id="example1">
                                        <thead>
                                                  <tr>
                                                            <th>No</th>
                                                            <th>Gambar</th>
                                                            <th>Title</th>
                                                            <th>Category</th>
                                                            <th>Status</th>
                                                            <th>Author</th>
                                                            <th>Action</th>
                                                  </tr>
                                        </thead>
                                        <tbody>
                                                  <?php $no=1; foreach ($all_post as $value): ?>
                                                                      <tr>
                                                                                <td><?= $no ?></td>
                                                                                <td>
                                                                                          <?php if ($value->posts_image != 'No image'): ?>
                                                                                                    <img src="<?= $value->posts_image ?>" width="100">
                                                                                          <?php else: ?>
                                                                                                    No Image
                                                                                          <?php endif; ?>
                                                                                </td>
                                                                                <td><?= $value->posts_title ?></td>
                                                                                <td><?= $value->at_name ?></td>
                                                                                <td>
                                                                                          <?php if($value->posts_status=='publish'): ?>
                                                                                          <a class="btn btn-xs btn-success"><?= $value->posts_status ?></a>
                                                                                          <?php else: ?>
                                                                                          <a class="btn btn-xs btn-danger"><?= $value->posts_status ?></a>
                                                                                          <?php endif;?>
                                                                                </td>
                                                                                <td><?= $value->admin_name ?></td>
                                                                                <td>
                                                                                          <div class="btn-group" role="group" aria-label="...">
                                                                                                    <a href="<?= base_url('posts/view/' . $value->posts_url) ?>" class="btn btn-success btn-xs" title="View posting" target="_blank"><span class="glyphicon glyphicon-zoom-in"></span></a>
                                                                                                    <a href="<?= base_url('backend/posts/put/' . $value->id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                    <a href="<?= base_url('backend/posts/delete/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                          </div>
                                                                                </td>
                                                                      </tr>
                                                            <?php $no++; endforeach; ?>
                                        </tbody>
                              </table>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/plugin/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script>
                                                                                          $(function () {
                                                                                                    $("#example1").DataTable();
                                                                                          });
</script>
</body>
</html>