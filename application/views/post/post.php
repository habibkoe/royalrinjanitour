<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/posts') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Post</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-text-background"></i> <?= $page_name ?></span>
                              </h2> 
                              <div class="row">
                                        <?php if (validation_errors()): ?>
                                                                      <div class="col-md-12">
                                                                                <div class="alert alert-block alert-danger fade in">
                                                                                          <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                                                          <?= validation_errors(); ?>
                                                                                </div>
                                                                      </div>
                                                            <?php endif; ?>
                                        <?= form_open_multipart('backend/posts/post') ?>
                                        <div class="col-md-8">
                                                  <input type="text" name="posts_title" placeholder="judul" class="form-control">
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-5">
                                                                      <label>Posts Type</label><br>
                                                                      <div class="btn-group" data-toggle="buttons">
                                                                                <label class="btn btn-primary">
                                                                                          <input type="radio" name="posts_type" id="option2" autocomplete="off" value="page"> Page
                                                                                </label>
                                                                                <label class="btn btn-primary">
                                                                                          <input type="radio" name="posts_type" id="option2" autocomplete="off" value="info"> Info
                                                                                </label>
                                                                                
                                                                      </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                      <label>Category</label>
                                                                      <select name="posts_category" class="form-control">
                                                                                <option value="">Pilih Kategori</option>
                                                                                <?php foreach ($category as $value): ?>
                                                                                                    <option value="<?= $value->at_id ?>"><?= $value->at_name ?></option>
                                                                                          <?php endforeach; ?>
                                                                      </select>
                                                            </div>

                                                  </div>
                                                  <br>
                                                  <textarea name="posts_description" placeholder="isi" id="tinyEditor"></textarea>

                                        </div>
                                        <div class="col-md-4">
                                                  <label>Publish</label>
                                                  <br>
                                                  <input type="submit" name="posts_status" value="publish"  class="btn btn-primary">
                                                  <hr>
                                                  <label>Short Description</label>
                                                  <br>
                                                  <textarea name="posts_short_description" class="form-control" rows="5" maxlength="230" placeholder="deskripsi ini akan muncul di halaman home"></textarea>
                                                  <span class="atention">Kata terbatas 230 karakter</span>
                                                  <hr>
                                                  <label>Featured Image</label>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <div class="row">
                                                                                <div class="col-md-12">
                                                                                          <a href="<?= base_url('filemanager/dialog.php?type=1&field_id=img_name') ?>" data-fancybox-type="iframe"  class="btn btn-info btn-xs fancy">Insert Image</a>
                                                                                          <button class="btn btn-danger btn-xs" id="delete" onclick="clear_img()">Remove</button>
                                                                                </div>
                                                                      </div>
                                                                      <img src="<?= base_url('assets/plugin/fancybox/image_upload.jpg') ?>" id="img_prev" class="slider-img-prev">
                                                                      <input type="text" name="posts_image_credit" class="form-control input-sm" placeholder="image copyright"> 
                                                                      <span class="atention">resolution: 500px x 300px or 700px x 420px or 1000px x 600px, image allowed: jpg, png, jpeg, size: < 200kb</span>
                                                                      <input type="text" name="posts_image" class="form-control" id="img_name" readonly="">
                                                            </div>
                                                  </div>
                                        </div>
                                        <?= form_close() ?>
                              </div>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/tinymce/tinymce.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?= base_url(); ?>assets/js/custom-js.js"></script>
<?php $this->load->view('layout/editor'); ?>        

</body>
</html>
