<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/programactivities/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New program</a>
                                        <a href="<?= base_url('backend/bnb/post') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-cog"></i>  BnB</a>
                                        <a href="<?= base_url('backend/rac/post') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-cog"></i>  Rent a Car</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-list"></i> <?= $page_name ?></span>
                              </h2>

                              <table class="table table-striped table-bordered table-hover" id="example1">
                                        <thead>
                                                  <tr>
                                                            <th>No</th>
                                                            <th>Gambar</th>
                                                            <th>Title</th>
                                                            <th>1 Person</th>
                                                            <th>2 People</th>
                                                            <th>3-4 People</th>
                                                            <th>5-6 People</th>
                                                            <th>7-8 People</th>
                                                            <th>9-10 People</th>
                                                            <th>11-20 People</th>
                                                            <th>Category</th>
                                                            <th width="8%">Action</th>
                                                  </tr>
                                        </thead>
                                        <tbody>
                                                  <?php $no = 1;foreach ($all_programactivities as $value): ?>
                                                                      <tr>
                                                                                <td><?= $no ?></td>
                                                                                <td><img src="<?= $value->pr_image ?>" width="60"></td>
                                                                                <td><a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>"><?= $value->pr_title ?></a></td>
                                                                                <td>US$ <?= $value->pr_price_1 ?></td>
                                                                                <td>US$ <?= $value->pr_price_2 ?></td>
                                                                                <td>US$ <?= $value->pr_price_3_4 ?></td>
                                                                                <td>US$ <?= $value->pr_price_5_6 ?></td>
                                                                                <td>US$ <?= $value->pr_price_7_8 ?></td>
                                                                                <td>US$ <?= $value->pr_price_9_10 ?></td>
                                                                                <td>US$ <?= $value->pr_price_11_20 ?></td>
                                                                                <td><?= $value->at_name ?></td>
                                                                                <td>
                                                                                          <div class="btn-group" role="group" aria-label="...">
                                                                                                    <a href="<?= base_url('programactivities/view/' . $value->pr_url) ?>" class="btn btn-success btn-xs" title="View program activities" target="_blank"><span class="glyphicon glyphicon-zoom-in"></span></a>
                                                                                                    <a href="<?= base_url('backend/programactivities/put/' . $value->id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                    <a href="<?= base_url('backend/programactivities/delete/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                          </div>
                                                                                </td>
                                                                      </tr>
                                                  <?php $no++;endforeach; ?>
                                        </tbody>
                              </table>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/plugin/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script>
          $(function () {
                    $("#example1").DataTable();
          });
</script>
</body>
</html>