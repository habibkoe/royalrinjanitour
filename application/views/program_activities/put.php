<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/posts') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All programs</a>
                                        <a href="<?= base_url('backend/programactivities/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New program</a>
                                        <a href="<?= base_url('backend/bnb/post') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-cog"></i>  BnB</a>
                                        <a href="<?= base_url('backend/rac/post') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-cog"></i>  Rent a Car</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-list"></i> <?= $page_name ?></span>
                              </h2> 
                              <div class="row">
                                        <?= form_open_multipart('backend/programactivities/put/' . $programactivities_one->id) ?>
                                        <div class="col-md-8">
                                                  <input type="text" name="pr_title" placeholder="judul" class="form-control" value="<?= $programactivities_one->pr_title ?>">
                                                  <hr>

                                                  <label><i class="fa fa-star" aria-hidden="true"></i> Price Include</label>
                                                  <br>
                                                  <textarea name="pr_short_description" class="form-control" rows="5" placeholder="deskripsi ini akan muncul di halaman  booking form"><?= $programactivities_one->pr_short_description ?></textarea>
                                                  
                                                  <hr>
                                                  <label>Program activities description</label>
                                                  <textarea name="pr_description" placeholder="isi" id="tinyEditor"><?= $programactivities_one->pr_description ?></textarea>

                                        </div>
                                        <div class="col-md-4">
                                                  <label><i class="fa fa-paper-plane" aria-hidden="true"></i> Publish</label>
                                                  <br>
                                                  <input type="submit" name="pr_status" value="publish"  class="btn btn-warning">
                                                  <hr>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <label><i class="fa fa-bookmark" aria-hidden="true"></i> Category</label>
                                                                      <select name="category" class="form-control">
                                                                                <option>Pilih Kategori</option>
                                                                                <?php foreach ($category as $value): ?>
                                                                                                    <?php if ($value->at_id == $programactivities_one->category): ?>
                                                                                                              <option value="<?= $value->at_id ?>" selected=""><?= $value->at_name ?></option>
                                                                                                    <?php else: ?>
                                                                                                              <option value="<?= $value->at_id ?>"><?= $value->at_name ?></option>
                                                                                                    <?php endif; ?>
                                                                                          <?php endforeach; ?>
                                                                      </select>
                                                            </div>


                                                  </div>
                                                  <label><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Price</label>
                                                  <div class="row">
                                                            <div class="col-md-6">
                                                                      <label>00-01 Person</label>
                                                                      US$ <input type="text" name="pr_price_1" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_1 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>00-02 People</label>
                                                                      US$ <input type="text" name="pr_price_2" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_2 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>03-04 People</label>
                                                                      US$ <input type="text" name="pr_price_3_4" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_3_4 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>05-06 People</label>
                                                                      US$ <input type="text" name="pr_price_5_6" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_5_6 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>07-08 People</label>
                                                                      US$ <input type="text" name="pr_price_7_8" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_7_8 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>09-10 People</label>
                                                                      US$ <input type="text" name="pr_price_9_10" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_9_10 ?>">
                                                            </div>
                                                            <div class="col-md-6">
                                                                      <label>11-20 People</label>
                                                                      US$ <input type="text" name="pr_price_11_20" class="form-control input-sm" placeholder="US$" value="<?= $programactivities_one->pr_price_11_20 ?>">
                                                            </div>
                                                  </div>
                                                  <hr>
                                                  <label><i class="fa fa-camera" aria-hidden="true"></i> Featured Image</label>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <div class="row">
                                                                                <div class="col-md-12">
                                                                                          <a href="<?= base_url('filemanager/dialog.php?type=1&field_id=img_name') ?>" data-fancybox-type="iframe"  class="btn btn-info btn-xs fancy">Insert Image</a>
                                                                                          <button class="btn btn-danger btn-xs" id="delete" onclick="clear_img()">Remove</button>
                                                                                </div>
                                                                      </div>
                                                                      <img src="<?= base_url('assets/plugin/fancybox/image_upload.jpg') ?>" id="img_prev" class="slider-img-prev">
                                                                      <input type="text" name="pr_image_credit" class="form-control input-sm" value="<?= $programactivities_one->pr_image_credit ?>" placeholder="image copyright"> 
                                                                      <span class="atention">resolution: 500px x 300px or 700px x 420px or 1000px x 600px, image allowed: jpg, png, jpeg, size: < 200kb</span>
                                                                      <input type="text" name="pr_image" class="form-control" id="img_name" readonly="">
                                                                      <br>
                                                                      <div class="row">
                                                                                <div class="col-md-6">
                                                                                          <input type="hidden" name="pr_image_old" value="<?= $programactivities_one->pr_image ?>">
                                                                                          <img src="<?= $programactivities_one->pr_image ?>" class="img-responsive">
                                                                                </div>
                                                                      </div>
                                                            </div>
                                                  </div>
                                        </div>
                                        <?= form_close() ?>
                              </div>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/tinymce/tinymce.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/fancybox/source/jquery.fancybox.pack.js'); ?>"></script>
<script src="<?= base_url('assets/js/custom-js.js'); ?>"></script>
<?php $this->load->view('layout/editor'); ?>        

</body>
</html>