<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/programsetting/post') ?>" class="btn btn-warning"><i class="glyphicon glyphicon-cog"></i>  New</a>
                                        <span class="pull-right"><i class="glyphicon glyphicon-cog"></i> <?= $page_name ?></span>
                              </h2>
                              <div class="row">
                                        <?= form_open('backend/programsetting/put/'.$program_setting_one->prs_id) ?>
                                        <div class="col-md-3">
                                                  <label>B&B or Rent a car name </label>
                                                  <select name="prs_name" class="form-control">
                                                            <?php foreach ($bnb as $value): ?>
                                                                                <?php if ($value->id == $program_setting_one->prs_name): ?>
                                                                                          <option value="<?= $value->id ?>" selected=""><?= $value->posts_title ?></option>
                                                                                <?php else: ?>
                                                                                          <option value="<?= $value->id ?>"><?= $value->posts_title ?></option>
                                                                                <?php endif; ?>
                                                                      <?php endforeach; ?>
                                                  </select>
                                                  <span class="atention">this data is selected from post where category is b&b-rent a car</span>
                                        </div>
                                        <div class="col-md-1">
                                                  <label>Capacity</label>
                                                  <input type="text" name="prs_capacity" class="form-control" value="<?= $program_setting_one->prs_capacity ?>">
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Type</label>
                                                  <input type="text" name="prs_type" class="form-control" placeholder="example: house, room" value="<?= $program_setting_one->prs_type ?>">
                                                  <span class="atention">example:house, room, xenia, fortuner</span>
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Category</label>
                                                  <select name="prs_category" class="form-control">
                                                            <option <?php if ($program_setting_one->prs_category == 'bnb') {
                                                                      echo 'selected';
                                                            } ?> value="bnb">Bed and Breakfast</option>
                                                            <option <?php if ($program_setting_one->prs_category == 'rent_a_car') {
                                                                      echo 'selected';
                                                            } ?>  value="rent_a_car">Rent a car</option>
                                                  </select>
                                                  <span class="atention">Only two category, B&B or Rent a car</span>
                                        </div>
                                        <div class="col-md-1">
                                                  <label>Price</label>
                                                  <input type="text" name="prs_price" class="form-control" placeholder="US$" value="<?= $program_setting_one->prs_price ?>">
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Status</label>
                                                  <select name="prs_status" class="form-control">
                                                            <option <?php if ($program_setting_one->prs_status == 'available') {
                                                                      echo 'selected';
                                                            } ?> value="available">Available</option>
                                                            <option <?php if ($program_setting_one->prs_status == 'full') {
                                                                      echo 'selected';
                                                            } ?> value="full">Full</option>
                                                  </select>
                                                  <span class="atention">the default status is available. if this room choose by customer, it will be change</span>
                                        </div>
                                        <div class="col-md-12">
                                                  <hr>
                                                  <input type="submit" class="btn btn-primary btn-success" value="Update">
                                                  <hr>
                                        </div>
<?= form_close() ?>
                              </div>
                    </div>
          </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</body>
</html>
