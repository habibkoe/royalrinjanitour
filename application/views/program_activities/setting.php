<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header"><i class="glyphicon glyphicon-cog"></i>  <?= $page_name ?></h2>
                              <div class="row">
                                        <?= form_open('backend/programsetting/post') ?>
                                        <div class="col-md-3">
                                                  <label>B&B or Rent a car name </label>
                                                  <select name="prs_name" class="form-control">
                                                            <?php foreach($bnb as $value): ?>
                                                            <option value="<?= $value->id ?>"><?= $value->posts_title ?></option>
                                                            <?php endforeach; ?>
                                                  </select>
                                                  <span class="atention">this data is selected from post where category is b&b-rent a car</span>
                                        </div>
                                        <div class="col-md-1">
                                                  <label>Capacity</label>
                                                  <input type="text" name="prs_capacity" class="form-control">
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Type</label>
                                                  <input type="text" name="prs_type" class="form-control" placeholder="example: house, room">
                                                  <span class="atention">example:house, room, xenia, fortuner</span>
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Category</label>
                                                  <select name="prs_category" class="form-control">
                                                            <option value="bnb">Bed and Breakfast</option>
                                                            <option value="rent_a_car">Rent a car</option>
                                                  </select>
                                                  <span class="atention">Only two category, B&B or Rent a car</span>
                                        </div>
                                        <div class="col-md-1">
                                                  <label>Price</label>
                                                  <input type="text" name="prs_price" class="form-control" placeholder="US$">
                                        </div>
                                        <div class="col-md-2">
                                                  <label>Status</label>
                                                  <select name="prs_status" class="form-control">
                                                            <option value="available">Available</option>
                                                            <option value="full">Full</option>
                                                  </select>
                                                  <span class="atention">the default status is available. if this room choose by customer, it will be change</span>
                                        </div>
                                        <div class="col-md-12">
                                                  <hr>
                                                  <input type="submit" class="btn btn-primary btn-success" value="Save">
                                                  <hr> 
                                        </div>
                                        <?= form_close() ?>
                              </div>
                              
                              <h4>Monitoring B&B Rent a car</h4>
                                        <table class="table table-striped table-bordered table-hover" id="example1">
                                        <thead>
                                                  <tr>
                                                            <th>No</th>
                                                            <th>Name</th>
                                                            <th>Capacity</th>
                                                            <th>Type</th>
                                                            <th>Category</th>
                                                            <th>Price</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                            
                                                  </tr>
                                        </thead>
                                        <tbody>
                                                  <?php $no = 1;foreach ($all_program_setting as $value): ?>
                                                                      <tr>
                                                                                <td><?= $no ?></td>
                                                                                <td><?= $value->posts_title ?></td>
                                                                                <td><?= $value->prs_capacity ?></td>
                                                                                <td><?= $value->prs_type ?></td>
                                                                                <td><?= $value->prs_category ?></td>
                                                                                <td>US$ <?= $value->prs_price ?></td>
                                                                                <td><?= $value->prs_status ?></td>
                                                                                <td>
                                                                                          <div class="btn-group" role="group" aria-label="...">
                                                                                                    <a href="<?= base_url('backend/programsetting/available/' . $value->prs_id) ?>" class="btn btn-success btn-xs" title="make it available"><span class="glyphicon glyphicon-ok"></span></a>
                                                                                                    <a href="<?= base_url('backend/programsetting/put/' . $value->prs_id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                    <a href="<?= base_url('backend/programsetting/delete/' . $value->prs_id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                          </div>
                                                                                </td>
                                                                      </tr>
                                                  <?php $no++;endforeach; ?>
                                        </tbody>
                              </table>
                             
                    </div>
          </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</body>
</html>
