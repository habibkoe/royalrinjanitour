<div class="container-fluid">
          <div class="row">
                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/rac/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New Rent Car</a>
                                        <span class="pull-right"><i class="fa fa-car" aria-hidden="true"></i> <?= $page_name ?></span>
                              </h2>

                              <table class="table table-striped table-bordered table-hover" id="example1">
                                        <thead>
                                                  <tr>
                                                            <th>No</th>
                                                            <th>Gambar</th>
                                                            <th>Title</th>
                                                            <th>Status</th>
                                                            <th>Author</th>
                                                            <th>Action</th>
                                                  </tr>
                                        </thead>
                                        <tbody>
                                                  <?php $no=1; foreach ($all_rac as $value): ?>
                                                                      <tr>
                                                                                <td><?= $no ?></td>
                                                                                <td>
                                                                                          <?php if ($value->rac_image != 'No image'): ?>
                                                                                                    <img src="<?= $value->rac_image ?>" width="100">
                                                                                          <?php else: ?>
                                                                                                    No Image
                                                                                          <?php endif; ?>
                                                                                </td>
                                                                                <td><?= $value->rac_name ?></td>
                                                                                <td>
                                                                                          <?php if($value->rac_status=='publish'): ?>
                                                                                          <a class="btn btn-xs btn-success"><?= $value->rac_status ?></a>
                                                                                          <?php else: ?>
                                                                                          <a class="btn btn-xs btn-danger"><?= $value->rac_status ?></a>
                                                                                          <?php endif;?>
                                                                                </td>
                                                                                <td><?= $value->admin_name ?></td>
                                                                                <td>
                                                                                          <div class="btn-group" role="group" aria-label="...">
                                                                                                    <a href="<?= base_url('rac/view/' . $value->rac_url) ?>" class="btn btn-success btn-xs" title="View posting" target="_blank"><span class="glyphicon glyphicon-zoom-in"></span></a>
                                                                                                    <a href="<?= base_url('backend/rac/put/' . $value->id) ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>
                                                                                                    <a href="<?= base_url('backend/rac/delete/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                          </div>
                                                                                </td>
                                                                      </tr>
                                                            <?php $no++; endforeach; ?>
                                        </tbody>
                              </table>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/plugin/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/plugin/datatables/dataTables.bootstrap.min.js') ?>"></script>
<script>
           $(function () {
                       $("#example1").DataTable();
            });
</script>
</body>
</html>