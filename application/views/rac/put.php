<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/rac') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Rent Car</a>
                                        <a href="<?= base_url('backend/rac/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New Car</a>
                                        <span class="pull-right"><i class="fa fa-car" aria-hidden="true"></i> <?= $page_name ?></span>
                              </h2> 
                              <div class="row">
                                        <?php if (validation_errors()): ?>
                                                                      <div class="col-md-12">
                                                                                <div class="alert alert-block alert-danger fade in">
                                                                                          <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                                                          <?= validation_errors(); ?>
                                                                                </div>
                                                                      </div>
                                                            <?php endif; ?>
                                        <?= form_open_multipart('backend/rac/put/' . $rac_one->id) ?>
                                        <div class="col-md-8">
                                                  <input type="text" name="rac_name" placeholder="Car name" class="form-control" value="<?= $rac_one->rac_name ?>">
                                                  <br>
                                                  <div class="row">
                                                            <div class="col-md-4">
                                                                      <label><i class="fa fa-building-o" aria-hidden="true"></i> Type</label>
                                                                      <input type="text" name="rac_type" class="form-control" value="<?= $rac_one->rac_type ?>">
                                                                      <span class="atention">example: mini bus, van, suv, sedan.</span>
                                                            </div>
                                                            <div class="col-md-2">
                                                                      <label><i class="fa fa-user" aria-hidden="true"></i> Capacity</label>
                                                                      <input type="text" name="rac_capacity" class="form-control" value="<?= $rac_one->rac_capacity ?>">
                                                                      <span class="atention">example: 4</span>
                                                            </div>
                                                            <div class="col-md-2">
                                                                      <label><i class="fa fa-credit-card-alt" aria-hidden="true"></i> Price</label>
                                                                      <input type="text" name="rac_price" class="form-control" value="<?= $rac_one->rac_price ?>">
                                                                      <span class="atention">example: 17</span>
                                                            </div>
                                                            <div class="col-md-3">
                                                                      <label><i class="fa fa-check" aria-hidden="true"></i> Status</label>
                                                                      <select name="rac_stock" class="form-control">
                                                                                <option value="available">Available</option>
                                                                                <option value="full">Full</option>
                                                                      </select>
                                                                      <span class="atention">the default status is available.</span>
                                                            </div>
                                                  </div>
                                                  <br>
                                                  <textarea name="rac_description" placeholder="isi" id="tinyEditor"><?= $rac_one->rac_description ?></textarea>

                                        </div>
                                        <div class="col-md-4">
                                                  <label><i class="fa fa-paper-plane" aria-hidden="true"></i> Publish</label>
                                                  <br>
                                                  <input type="submit" name="rac_status" value="publish"  class="btn btn-warning">
                                                  <hr>
                                                  <label><i class="fa fa-comment-o" aria-hidden="true"></i> Short Description</label>
                                                  <br>
                                                  <textarea name="rac_short_description" class="form-control" rows="5" maxlength="230" placeholder="deskripsi ini akan muncul di halaman home"><?= $rac_one->rac_short_description ?></textarea>
                                                  <span class="atention">Kata terbatas 230 karakter</span>
                                                  <hr>
                                                  <label><i class="fa fa-camera" aria-hidden="true"></i> Featured Image</label>
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <div class="row">
                                                                                <div class="col-md-12">
                                                                                          <a href="<?= base_url('filemanager/dialog.php?type=1&field_id=img_name') ?>" data-fancybox-type="iframe"  class="btn btn-info btn-xs fancy">Insert Image</a>
                                                                                          <button class="btn btn-danger btn-xs" id="delete" onclick="clear_img()">Remove</button>
                                                                                </div>
                                                                      </div>
                                                                      <img src="<?= base_url('assets/plugin/fancybox/image_upload.jpg') ?>" id="img_prev" class="slider-img-prev">
                                                                      <input type="text" name="rac_image_credit" class="form-control input-xs" value="<?= $rac_one->rac_image_credit ?>"  placeholder="image copyright">
                                                                      <span class="atention">resolution: 500px x 300px or 700px x 420px or 1000px x 600px, image allowed: jpg, png, jpeg, size: < 200kb</span>
                                                                      <input type="text" name="rac_image" class="form-control" id="img_name" readonly="">
                                                                      <br>
                                                                      <div class="row">
                                                                                <div class="col-md-6">
                                                                                          <input type="hidden" name="post_image_old" value="<?= $rac_one->rac_image ?>">
                                                                                          <img src="<?= $rac_one->rac_image ?>" class="img-responsive">
                                                                                </div>
                                                                      </div>
                                                            </div>
                                                  </div>
                                        </div>
                                        <?= form_close() ?>
                              </div>

                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/tinymce/tinymce.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?= base_url(); ?>assets/js/custom-js.js"></script>
<?php $this->load->view('layout/editor'); ?>        

</body>
</html>
