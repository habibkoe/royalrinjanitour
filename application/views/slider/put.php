<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/sliders') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Sliders</a>
                                        <a href="<?= base_url('backend/sliders/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New Slider</a>
                                        <span class="pull-right"><i  class="glyphicon glyphicon-upload"></i>  <?= $page_name ?></span>
                              </h2>
                              <?= form_open_multipart('backend/sliders/put/'.$slider_one->id); ?>
                              <input type="text" name="slider_title" class="form-control" value="<?= $slider_one->slider_title ?>">
                              <br>
                              <div class="row">
                                        <div class="col-md-9">
                                                  <div class="row">
                                                            <div class="col-md-12">
                                                                      <a href="<?= base_url('filemanager/dialog.php?type=1&field_id=img_name') ?>" data-fancybox-type="iframe"  class="btn btn-info btn-xs fancy">Insert Image</a>
                                                                      <button class="btn btn-danger btn-xs" id="delete" onclick="clear_img()">Remove</button>
                                                            </div>
                                                  </div>
                                                  <img src="<?= base_url('assets/plugin/fancybox/image_upload.jpg') ?>" id="img_prev" class="slider-img-prev">
                                                  <input type="text" value="" name="slider_image" class="form-control read-only" id="img_name" readonly="">
                                                  <span>best resolution : 1400px X 500px :: image allowed : < 500 KB and extention jpg, png</span>
                                        </div>
                                        <div class="col-md-3">
                                                  <label>Recent image</label>
                                                  <img src="<?= $slider_one->slider_image ?>" class="img-responsive">
                                        </div>
                              </div>
                              <label>Description</label>
                              <textarea name="slider_description" class="form-control" rows="5"></textarea>
                              <br>
                              <input type="submit" name="slider_status" value="publish" class="btn btn-primary">
                              <?= form_close(); ?>

                    </div>
          </div>
</div>


<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>assets/plugin/fancybox/source/jquery.fancybox.pack.js"></script>
<script src="<?= base_url(); ?>assets/js/custom-js.js"></script>

</body>
</html>
