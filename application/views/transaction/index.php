<?php if($this->session->userdata('customerin')== TRUE): ?>
<div class="container box-home">
          <div class="breadcrumb">
                    <a href="<?= base_url() ?>"><i class="glyphicon glyphicon-home"></i> Home > </a>
                    <a href="<?= base_url('programactivities') ?>">Transaction > </a>
          </div>
          <?php if (validation_errors()): ?>
                              <div class="col-md-12">
                                        <div class="alert alert-block alert-danger fade in">
                                                  <button data-dismiss="alert" class="close close-sm" type="button">x</button>
                                                  <?= validation_errors(); ?>
                                        </div>
                              </div>
                    <?php endif; ?>
          <?php if($booking_one->or_status=='disable'): ?>
          <div class="row" style="text-align: center;">
                    <div class="col-md-12">
                              <h3 style="text-align: center;"><strong>Your Transaction Expired.</strong></h3>
                              <br>
                              <img src="<?= base_url('assets/img/empty_cart.png') ?>" width="250">
                    </div>
          </div>
          <?php else: ?>
          <div class="row">
                    <div class="col-md-5 col-sm-5">
                              <h3>MY PROFILE</h3>

                    </div>
                    <div class="col-md-7 col-sm-7">
                              <span style="font-size: 24px;float: left; margin-right: 5px; margin-top: 20px; margin-bottom: 10px;">YOUR TRANSACTION</span>
                              <span style="font-size: 14px;float: left; margin-right: 10px; margin-top: 24px; margin-bottom: 10px;"><?= $booking_one->id ?></span>
                    </div>

          </div>
          <div class="row">
                    <div class="col-md-5 col-sm-5">
                              <table style="width: 100%">

                                        <tr>
                                                  <td>Name</td><td>:</td><td><?= $booking_one->or_full_name ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Mobile Phone Number</td><td>:</td><td><?= $booking_one->or_phone ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Land Line Phone Number</td><td>:</td><td><?= $booking_one->or_home_phone ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Email</td><td>:</td><td><?= $booking_one->or_email ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Address</td><td>:</td><td><?= $booking_one->or_address ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Departure date</td><td>:</td><td><?= $booking_one->or_departure_date ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Arrival date</td><td>:</td><td><?= $booking_one->or_arrival_date ?></td>

                                        </tr>
                                        <?php if($booking_one->or_type!='bnb'): ?>
                                        <tr>
                                                  <td>Pick-up from</td><td>:</td><td><?= $booking_one->or_pick_up_from ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Drop off to</td><td>:</td><td><?= $booking_one->or_drop_off_to ?></td>

                                        </tr>
                                        <tr>
                                                  <td>Relevant medical conditions</td><td>:</td><td><?= $booking_one->or_medical_condition ?></td>

                                        </tr>
                                        <?php endif; ?>
                                        <tr>
                                                  <td>Allergies, Special Diets Or Vegetarian</td><td>:</td><td><?= $booking_one->or_special_diets ?></td>

                                        </tr>

                                        <tr>
                                                  <td>Special request</td><td>:</td><td><?= $booking_one->or_message ?></td>

                                        </tr>

                              </table>
                    </div>
                    <div class="col-md-7 col-sm-7">
                              <p>

                                        Booking(s) on <strong><?= date('d-m-Y H:i:s', strtotime($booking_one->created_at)) ?></strong>

                              </p>
                              <?php if($booking_one->or_status=='unpaid'): ?>
                                        <h3 style="text-align: left;font-weight: bold;text-transform: capitalize; color: #F00;"><?= $booking_one->or_status; ?></h3>
                              <?php else: ?>
                                        <h3 style="text-align: left;font-weight: bold;text-transform: capitalize; color: #008000;"><?= $booking_one->or_status; ?></h3>
                              <?php endif; ?>

                              <div style="width: 100%">
                                        <?php if($booking_one->or_status=='unpaid'): ?>
                                                  <h3 style="text-align: left;font-weight: bold;">Total  Amount  To Be Paid  <span style="color: #F00">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></h3>
                                        <?php else: ?>
                                                  <h3 style="text-align: left;font-weight: bold;">Thank You For Paid  <span style="color: #008000">US$<?= number_format($booking_one->or_price, 2, ".", ","); ?></span></h3>
                                        <?php endif; ?>
                                        <hr>
                                        <h4>Detail Programs</h4>
                                        <?php if ($booking_list_one > 0): ?>
                                                            <table style="width: 100%;">
                                                                      <thead>
                                                                                <tr style="background: #008000;color: #fff;" style="padding: 10px;">
                                                                                          <th colspan="2" style="padding: 10px;">Program (s)</th>
                                                                                          <th style="padding: 10px;"><span style="float: right;">US$</span></th>
                                                                                </tr>
                                                                      </thead>
                                                                      <tbody>
                                                                                <?php if($booking_one->or_type == 'programs'): ?>
                                                                                <?php  foreach ($booking_list_one as $sp) : ?>
                                                                                         <?php  if ($sp->booking_id == $booking_one->id): ?>
                                                                                                    <tr>
                                                                                                              <td style="padding-top: 5px;"><img src="<?= $sp->pr_image ?>" width="100"></td>
                                                                                                              <td>
                                                                                                                        <?= $sp->pr_title ?>
                                                                                                              </td>  
                                                                                                              <td>
                                                                                                                        <?= number_format($sp->price, 2, ".", ","); ?>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                              <td colspan="3">
                                                                                                                        <p class="price-includes">
                                                                                                                                  <strong>Price includes:</strong><br>
                                                                                                                                  <?= $sp->pr_short_description ?>
                                                                                                                        </p>
                                                                                                              </td>
                                                                                                    </tr>
                                                                                                    <?php endif; ?>
                                                                                          <?php endforeach; ?>
                                                                                          <?php else: ?>
                                                                                                    <?php  foreach ($booking_list_one as $sp) : ?>
                                                                                                              <?php  if ($sp->booking_id == $booking_one->id): ?>
                                                                                                               <tr>
                                                                                                                         <td style="padding-top: 5px;"><img src="<?= $sp->bnb_image ?>" width="100"></td>
                                                                                                                         <td>
                                                                                                                                   <?= $sp->bnb_name ?>
                                                                                                                         </td>  
                                                                                                                         <td>
                                                                                                                                   <?= number_format($sp->price, 2, ".", ","); ?>
                                                                                                                         </td>
                                                                                                               </tr>
                                                                                                               <tr>
                                                                                                                         <td colspan="3">
                                                                                                                                   <p class="price-includes">
                                                                                                                                             <strong>Price includes:</strong><br>
                                                                                                                                             <?= $sp->bnb_short_description ?>
                                                                                                                                   </p>
                                                                                                                         </td>
                                                                                                               </tr>
                                                                                                               <?php endif; ?>
                                                                                                     <?php endforeach; ?>
                                                                                          <?php endif; ?>
                                                                      </tbody>

                                                            </table>
                                                  <?php endif; ?>
                                        <?php if($booking_one->or_status=='unpaid'): ?>
                                        <a href="#" data-toggle="modal" data-target="#myModalConfirm"  class="btn btn-lg btn-primary">Confirm</a>
                                        <?php endif;?>
                              </div>
                    </div>

          </div>
          <?php endif; ?>
</div>
<div class="modal fade" id="myModalConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
                    <div class="modal-content">
                              <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Paid Confirm</h4>
                              </div>
                              <?= form_open_multipart('transaction/confirm/' . $booking_one->id) ?>
                              <div class="modal-body">
                                        <div class="form-group has-feedback">
                                                  <label>Your Bank Account</label>
                                                  <input type="text" name="bc_bank_account" class="form-control" id="invoice" placeholder="Account Name" required="">
                                                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                                        </div>
                                        <h6>example: Jack Miller</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Bank</label>
                                                  <input type="text" name="bc_bank" class="form-control" id="nope" placeholder="Bank" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>example: Western Union, Capital One Financial</h6>
                                        <div class="form-group has-feedback">
                                                  <label>Total Paid</label>
                                                  <input type="text" name="bc_total_paid" class="form-control" id="nope" placeholder="You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>" required="">
                                                  <span class="glyphicon glyphicon-text-background form-control-feedback"></span>
                                        </div>
                                        <h6>You must paid US$<?= number_format($booking_one->or_price, 2, ".", ","); ?>, just enter the bill without "US$"</h6>
                                        <label>Image Transfer</label>
                                        <div id="image-preview">
                                                  <label for="image-upload" id="image-label">image</label>
                                                  <input type="file" name="bc_image_paid" id="image-upload" required="" />
                                        </div>
                                        <h6>image extention: jpg, png | size: 100kb</h6>
                              </div>
                              <div class="modal-footer">
                                        <button type="submit" class="btn btn-danger pull-left">Paid</button>
                              </div>
                              <?= form_close() ?>
                    </div>
          </div>
</div>
<?php else: ?>
<div class="container box-home" style="min-height: 400px;">
          <h2 style="text-align: center;">Sorry You Must Login To Show Your Data</h2>
</div>
<?php endif; ?>
<?php $this->load->view('layout/footer'); ?>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url('assets/js/jquery.min.js'); ?>"></script>
<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
<script src="<?= base_url('assets/plugin/icheck/icheck.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('assets/js/jquery.uploadPreview.js'); ?>"></script>
<script type="text/javascript">
          $(document).ready(function () {
                    $.uploadPreview({
                              input_field: "#image-upload",
                              preview_box: "#image-preview",
                              label_field: "#image-label"
                    });
          });

</script>

<script type="text/javascript">
<!--
          $(document).ready(function () {

                    window.setTimeout(function () {
                              $(".alert").fadeTo(1500, 0).slideUp(500, function () {
                                        $(this).remove();
                              });
                    }, 5000);

          });
//-->
</script>
<script>
          $(document).ready(function () {
                    $('input').iCheck({
                              checkboxClass: 'icheckbox_square-yellow',
                              radioClass: 'iradio_square-yellow',
                              increaseArea: '20%' // optional
                    });
          });

          $('input#person').on('ifChanged', function () {
                    $('.konten').hide();
                    $('.' + $(this).data('rel')).show();
          });
          $('input#no-person').on('ifChanged', function () {
                    $('.konten').hide();
          });
</script>

<script>
          $(function () {
                    $('[data-toggle="tooltip"]').tooltip()
          })
</script>
<script>
                  (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                      (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

          ga('create', 'UA-78897314-1', 'auto');
          ga('send', 'pageview');

</script>
<div id="fb-root"></div>
<script>(function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id))
                              return;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6&appId=178073548932787";
                    fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>