<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/users') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All user</a>
                                        <span class="pull-right"><i  class="glyphicon glyphicon-user"></i> <?= $page_name ?></span>
                              </h2>
                              <?= form_open('backend/users/post') ?>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Name</label>
                                                  <input type="text" name="admin_name" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Email</label>
                                                  <input type="text" name="admin_email" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Phone</label>
                                                  <input type="text" name="admin_phone" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Username</label>
                                                  <input type="text" name="admin_username" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Password</label>
                                                  <input type="password" name="admin_password" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Status</label>
                                                  <select name="admin_status" class="form-control">
                                                            <option value="active">Active</option>
                                                            <option value="non active">Non active</option>
                                                  </select>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Level </label>
                                                  <select name="admin_level" class="form-control">
                                                            <option value="employee">Employee</option>
                                                            <option value="admin">Admin</option>
                                                  </select>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Position </label>
                                                  <select name="admin_position" class="form-control">
                                                            <option value="editor">Editor</option>
                                                            <option value="author">Author</option>
                                                  </select>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <input type="submit" name="ok" value="Save" class="btn btn-success">
                                        </div>
                              </div>
                              <?= form_close() ?>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>