<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/users') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-th-list"></i> All Users</a>
                                        <a href="<?= base_url('backend/users/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New User</a>
                                        <span class="pull-right"><i  class="glyphicon glyphicon-user"></i> <?= $page_name ?></span>
                              </h2>
                              <?= form_open('backend/users/put/'.$user_one->admin_id) ?>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Name</label>
                                                  <input type="text" name="admin_name" class="form-control" value="<?= $user_one->admin_name ?>">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Email</label>
                                                  <input type="text" name="admin_email" class="form-control" value="<?= $user_one->admin_email ?>">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Phone</label>
                                                  <input type="text" name="admin_phone" class="form-control" value="<?= $user_one->admin_phone ?>">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Username</label>
                                                  <input type="text" name="admin_username" class="form-control" value="<?= $user_one->admin_username ?>">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Password</label>
                                                  <input type="hidden" name="admin_password_old" value="<?= $user_one->admin_password ?>">
                                                  <input type="password" name="admin_password" class="form-control">
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Status</label>
                                                  <select name="admin_status" class="form-control">
                                                            <option value="active" <?php if($user_one->admin_status=='active'){ echo 'selected'; } ?>>Active</option>
                                                            <option value="non active" <?php if($user_one->admin_status=='non active'){ echo 'selected'; } ?>>Non active</option>
                                                  </select>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Level </label>
                                                  <select name="admin_level" class="form-control">
                                                            <option value="employee" <?php if($user_one->admin_level=='employee'){ echo 'selected'; } ?>>Employee</option>
                                                            <option value="admin" <?php if($user_one->admin_level=='admin'){ echo 'selected'; } ?>>Admin</option>
                                                  </select>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <label>Position </label>
                                                  <select name="admin_position" class="form-control">
                                                            <option value="editor" <?php if($user_one->admin_position=='editor'){ echo 'selected'; } ?>>Editor</option>
                                                            <option value="author" <?php if($user_one->admin_position=='author'){ echo 'selected'; } ?>>Author</option>
                                                  </select>
                                        </div>
                              </div>
                              <br>
                              <div class="row">
                                        <div class="col-md-12">
                                                  <input type="submit" name="ok" value="Save" class="btn btn-success">
                                        </div>
                              </div>
                              <?= form_close() ?>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>