<div class="container-fluid">
          <div class="row">

                    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                              <h2 class="page-header">
                                        <a href="<?= base_url('backend/users/post') ?>" class="btn btn-ungu"><i class="glyphicon glyphicon-plus"></i> New User</a>
                                        <span class="pull-right"><i  class="glyphicon glyphicon-user"></i> <?= $page_name ?></span>
                              </h2>

                              <div class="table-responsive">
                                        <table class="table table-bordered">
                                                  <thead>
                                                            <tr>
                                                                      <th>No</th>
                                                                      <th>IP Address</th>
                                                                      <th>Time</th>
                                                            </tr>
                                                  </thead>
                                                  <tbody>
                                                            <?php $no = 1; foreach ($all_visitor as $value): ?>
                                                                                <tr>
                                                                                          <td><?= $no ?></td>
                                                                                          <td><?= $value->ip_address ?></td>
                                                                                          <td><?= $value->timestamp ?></td>
                                                                                          <td>
                                                                                                    <div class="btn-group" role="group" aria-label="...">
                                                                                                              <a href="<?= base_url('backend/users/deleteVisitor/' . $value->id) ?>" class="btn btn-danger btn-xs" onclick="return confirm('are you sure to delete this data.?')"><span class="glyphicon glyphicon-trash"></span></a>
                                                                                                    </div>
                                                                                          </td>
                                                                                </tr>
                                                                      <?php $no++; endforeach; ?>
                                                  </tbody>
                                        </table>
                              </div>
                    </div>
          </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
</body>
</html>